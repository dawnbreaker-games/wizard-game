public interface ISpawnable
{
	string ScenePath { get; }
}
