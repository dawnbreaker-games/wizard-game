public interface IDestructable : Interface
{
	float Hp { get; set; }
	uint MaxHp { get; set; }
	
	void TakeDamage (float amount);
	void Death ();
}
