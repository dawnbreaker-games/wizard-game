using Godot;

namespace FightRoom
{
	public interface ICollisionEnterHandler2D
	{
		CollisionObject2D Collider { get; }
		void OnCollisionEnter2D (Node2D node);
	}
}
