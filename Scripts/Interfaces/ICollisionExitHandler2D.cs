using Godot;

namespace FightRoom
{
	public interface ICollisionExitHandler2D
	{
		CollisionObject2D Collider { get; }
		void OnCollisionExit2D (Node2D node);
	}
}
