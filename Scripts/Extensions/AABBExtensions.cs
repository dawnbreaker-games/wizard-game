using Godot;
using System.Collections;
using System.Collections.Generic;

namespace Extensions
{
	public static class AABBExtensions
	{
		public static AABB INFINITE = new AABB(VectorExtensions.INFINITE3, VectorExtensions.INFINITE3);

		public static bool IsEncapsulating (this AABB b1, AABB b2, bool equalAABBRetunsTrue)
		{
			if (equalAABBRetunsTrue)
			{
				bool minIsOk = b1.min.X <= b2.min.X && b1.min.Y <= b2.min.Y && b1.min.Z <= b2.min.Z;
				bool maxIsOk = b1.min.X >= b2.min.X && b1.min.Y >= b2.min.Y && b1.min.Z >= b2.min.Z;
				return minIsOk && maxIsOk;
			}
			else
			{
				bool minIsOk = b1.min.X < b2.min.X && b1.min.Y < b2.min.Y && b1.min.Z < b2.min.Z;
				bool maxIsOk = b1.max.X > b2.max.X && b1.max.Y > b2.max.Y && b1.max.Z > b2.max.Z;
				return minIsOk && maxIsOk;
			}
		}
		
		public static AABB Combine (this AABB[] aabbArray)
		{
			AABB output = aabbArray[0];
			for (int i = 1; i < aabbArray.Length; i ++)
			{
				AABB AABB = aabbArray[i];
				if (AABB.min.X < output.min.X)
					output.min = new Vector3(AABB.min.X, output.min.Y, output.min.Z);
				if (AABB.min.Y < output.min.Y)
					output.min = new Vector3(output.min.X, AABB.min.Y, output.min.Z);
				if (AABB.min.Z < output.min.Z)
					output.min = new Vector3(output.min.X, output.min.Y, AABB.min.Z);
				if (AABB.max.X > output.max.X)
					output.max = new Vector3(AABB.max.X, output.max.Y, output.max.Z);
				if (AABB.max.Y > output.max.Y)
					output.max = new Vector3(output.max.X, AABB.max.Y, output.max.Z);
				if (AABB.max.Z > output.max.Z)
					output.max = new Vector3(output.max.X, output.max.Y, AABB.max.Z);
			}
			return output;
		}
		
		public static bool Intersects (AABB b1, AABB b2, Vector3 expandB1 = new Vector3(), Vector3 expandB2 = new Vector3())
		{
			b1.Expand(expandB1);
			b2.Expand(expandB2);
			return b1.Intersects(b2);
		}
		
		public static float GetVolume (this AABB b)
		{
			return b.size.X * b.size.Y * b.size.Z;
		}
		
//		public static int GetVolume (this AABBInt b)
//		{
//			return b.size.X * b.size.Y * b.size.Z;
//		}
		
		public static Vector3 FromNormalizedPoint (this AABB b, Vector3 normalizedPoint)
		{
			return b.min + b.size.Multiply(normalizedPoint);
		}
		
		public static Vector3 ToNormalizedPoint (this AABB b, Vector3 point)
		{
			return point.Divide(b.size) - b.min;
		}
		
		public static Vector3 FlipPoint (this AABB b, Vector3 point)
		{
			return b.FromNormalizedPoint(Vector3.one - b.ToNormalizedPoint(point));
		}
		
//		public static AABBInt ToAABBInt (this AABB b, MathfExtensions.RoundingMethod minRoundingMethod = MathfExtensions.RoundingMethod.HalfOrLessRoundsUp, MathfExtensions.RoundingMethod maxRoundingMethod = MathfExtensions.RoundingMethod.HalfOrMoreRoundsDown)
//		{
//			AABBInt output = new AABBInt();
//			output.SetMinMax(b.min.ToVec3Int(minRoundingMethod), b.max.ToVec3Int(maxRoundingMethod));
//			return output;
//		}
		
		public static Vector3[] GetCorners (this AABB b)
		{
			Vector3[] corners = new Vector3[8];
			corners[0] = b.min;
			corners[1] = new Vector3(b.max.X, b.min.Y, b.min.Z);
			corners[2] = new Vector3(b.max.X, b.max.Y, b.min.Z);
			corners[3] = b.max;
			corners[4] = new Vector3(b.min.X, b.max.Y, b.min.Z);
			corners[5] = new Vector3(b.min.X, b.max.Y, b.max.Z);
			corners[6] = new Vector3(b.min.X, b.min.Y, b.max.Z);
			corners[7] = new Vector3(b.max.X, b.min.Y, b.max.Z);
			return corners;
		}
		
		public static LineSegment3D[] GetSides (this AABB b)
		{
			LineSegment3D[] sides = new LineSegment3D[12];
			Vector3[] corners = b.GetCorners();
			Vector3 corner0 = corners[0];
			Vector3 corner1 = corners[1];
			Vector3 corner2 = corners[2];
			Vector3 corner3 = corners[3];
			Vector3 corner4 = corners[4];
			Vector3 corner5 = corners[5];
			Vector3 corner6 = corners[6];
			Vector3 corner7 = corners[7];
			sides[0] = new LineSegment3D(corner0, corner1);
			sides[1] = new LineSegment3D(corner2, corner3);
			sides[2] = new LineSegment3D(corner0, corner6);
			sides[3] = new LineSegment3D(corner6, corner7);
			sides[4] = new LineSegment3D(corner5, corner6);
			sides[5] = new LineSegment3D(corner3, corner5);
			sides[6] = new LineSegment3D(corner3, corner7);
			sides[7] = new LineSegment3D(corner0, corner4);
			sides[8] = new LineSegment3D(corner4, corner5);
			sides[9] = new LineSegment3D(corner2, corner4);
			sides[10] = new LineSegment3D(corner1, corner7);
			sides[11] = new LineSegment3D(corner1, corner2);
			return sides;
		}

		public static Plane[] GetOutsideFacePlanes (this AABB b)
		{
			Plane[] facePlanes = new Plane[6];
			Vector3[] corners = b.GetCorners();
			Vector3 corner0 = corners[0];
			Vector3 corner1 = corners[1];
			Vector3 corner2 = corners[2];
			Vector3 corner3 = corners[3];
			Vector3 corner4 = corners[4];
			Vector3 corner5 = corners[5];
			Vector3 corner6 = corners[6];
			Vector3 corner7 = corners[7];
			facePlanes[0] = new Plane(corner0, corner5, corner4); // -x
			facePlanes[1] = new Plane(corner1, corner2, corner3); // +x
			facePlanes[2] = new Plane(corner0, corner1, corner6); // -y
			facePlanes[3] = new Plane(corner2, corner4, corner3); // +y
			facePlanes[4] = new Plane(corner0, corner2, corner1); // -z
			facePlanes[5] = new Plane(corner3, corner5, corner6); // +z
			return facePlanes;
		}
		
		public static Vector3[] GetPointsInside (this AABB b, Vector3 checkInterval)
		{
			List<Vector3> output = new List<Vector3>();
			for (float x = b.min.X; x <= b.max.X; x += checkInterval.X)
			{
				for (float y = b.min.Y; y <= b.max.Y; y += checkInterval.Y)
				{
					for (float z = b.min.Z; z <= b.max.Z; z += checkInterval.Z)
						output.Add(new Vector3(x, y, z));
				}
			}
			return output.ToArray();
		}
		
		public static Vector3[] GetPointsInside (this AABB b, Vector3 checkInterval, Vector3 offsetMin, Vector3 offsetMax)
		{
			List<Vector3> output = new List<Vector3>();
			for (float x = b.min.X + offsetMin.X; x <= b.max.X + offsetMax.X; x += checkInterval.X)
			{
				for (float y = b.min.Y + offsetMin.Y; y <= b.max.Y + offsetMax.Y; y += checkInterval.Y)
				{
					for (float z = b.min.Z + offsetMin.Z; z <= b.max.Z + offsetMax.Z; z += checkInterval.Z)
						output.Add(new Vector3(x, y, z));
				}
			}
			return output.ToArray();
		}
		
		public static AABB GetAABBFromNormalizedMinMax (this AABB b, Vector3 normalizedStart, Vector3 normalizedEnd)
		{
			AABB output = new AABB();
			output.SetMinMax(b.FromNormalizedPoint(normalizedStart), b.FromNormalizedPoint(normalizedEnd));
			return output;
		}

		public static AABB GetOffset (this AABB aabb, Vector3 offsetMin, Vector3 offsetMax)
		{
			AABB output = new AABB(aabb);
			output.min += offsetMin;
			output.max += offsetMax;
			return output;
		}

//		public static AABB ToAABB (this AABBInt b)
//		{
//			return new AABB(b.min, b.size);
//		}
		
		public static AABB MakePositiveSize (this AABB b)
		{
			return new AABB(b.min, b.size.MakePositive());
		}
		
		public static bool Raycast (this AABB b, Ray ray, out Vector3 hit)
		{
			hit = VectorExtensions.INFINITE3;
			Plane[] facePlanes = b.GetOutsideFacePlanes();
			Plane facePlane0 = facePlanes[0];
			Plane facePlane1 = facePlanes[1];
			Plane facePlane2 = facePlanes[2];
			Plane facePlane3 = facePlanes[3];
			Plane facePlane4 = facePlanes[4];
			Plane facePlane5 = facePlanes[5];
			float distance0 = 0;
			float distance1 = 0;
			float distance2 = 0;
			float distance3 = 0;
			float distance4 = 0;
			float distance5 = 0;
			Vector3 hit0 = new Vector3();
			Vector3 hit1 = new Vector3();
			Vector3 hit2 = new Vector3();
			Vector3 hit3 = new Vector3();
			Vector3 hit4 = new Vector3();
			Vector3 hit5 = new Vector3();
			if (facePlane0.Raycast(ray, out distance0))
			{
				hit0 = ray.GetPoint(distance0);
				if (hit0.Y < b.min.Y || hit0.Y > b.max.Y || hit0.Z < b.min.Z || hit0.Z > b.max.Z)
					distance0 = float.MaxValue;
			}
			else
				distance0 = float.MaxValue;
			if (facePlane1.Raycast(ray, out distance1))
			{
				hit1 = ray.GetPoint(distance1);
				if (hit1.Y < b.min.Y || hit1.Y > b.max.Y || hit1.Z < b.min.Z || hit1.Z > b.max.Z)
					distance1 = float.MaxValue;
			}
			else
				distance1 = float.MaxValue;
			if (facePlane2.Raycast(ray, out distance2))
			{
				hit2 = ray.GetPoint(distance2);
				if (hit2.X < b.min.X || hit2.X > b.max.X || hit2.Z < b.min.Z || hit2.Z > b.max.Z)
					distance2 = float.MaxValue;
			}
			else
				distance2 = float.MaxValue;
			if (facePlane3.Raycast(ray, out distance3))
			{
				hit3 = ray.GetPoint(distance3);
				if (hit3.X < b.min.X || hit3.X > b.max.X || hit3.Z < b.min.Z || hit3.Z > b.max.Z)
					distance3 = float.MaxValue;
			}
			else
				distance3 = float.MaxValue;
			if (facePlane4.Raycast(ray, out distance4))
			{
				hit4 = ray.GetPoint(distance4);
				if (hit4.X < b.min.X || hit4.X > b.max.X || hit4.Y < b.min.Y || hit4.Y > b.max.Y)
					distance4 = float.MaxValue;
			}
			else
				distance4 = float.MaxValue;
			if (facePlane5.Raycast(ray, out distance5))
			{
				hit5 = ray.GetPoint(distance5);
				if (hit5.X < b.min.X || hit5.X > b.max.X || hit5.Y < b.min.Y || hit5.Y > b.max.Y)
					distance5 = float.MaxValue;
			}
			else
				distance5 = float.MaxValue;
			float distance = Mathf.min(distance0, distance1, distance2, distance3, distance4, distance5);
			if (distance == float.MaxValue)
				return false;
			else if (distance == distance0)
				hit = hit0;
			else if (distance == distance1)
				hit = hit1;
			else if (distance == distance2)
				hit = hit2;
			else if (distance == distance3)
				hit = hit3;
			else if (distance == distance4)
				hit = hit4;
			else
				hit = hit5;
			return true;
		}
		
		public static Vector3 RandomPoint (this AABB b)
		{
			return new Vector3(Random.Range(b.min.X, b.max.X), Random.Range(b.min.Y, b.max.Y), Random.Range(b.min.Z, b.max.Z));
		}
	}
}
