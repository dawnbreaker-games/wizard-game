using System.Collections;
using System.Collections.Generic;

namespace Extensions
{
	public static class MathfExtensions
	{
		public const float PI = 3.141592653589793238462643f;
		public const float DEG_2_RAD = 0.01745329f;
		public const float RAD_2_DEG = 57.2957795f;
		public const float INCHES_TO_CENTIMETERS = 2.54f;

		public static float RoundToInterval (float f, float interval, RoundingMethod roundingMethod = RoundingMethod.HalfOrLessRoundsDown)
		{
			if (interval == 0)
				return f;
			else
				return Round(f / interval, roundingMethod) * interval;
		}
		
		public static int Sign (float f)
		{
			if (f == 0)
				return 0;
			else
				return (int) Mathf.Sign(f);
		}
		
		public static bool AreOppositeSigns (float f1, float f2)
		{
			return Mathf.Abs(Sign(f1) - Sign(f2)) == 2;
		}

		public static float GetClosestNumber (float f, params float[] numbers)
		{
			float closestNumber = numbers[0];
			float closestDistance = Mathf.Abs(f - closestNumber);
			for (int i = 1; i < numbers.Length; i ++)
			{
				float number = numbers[i];
				float distance = Mathf.Abs(f - number);
				if (distance < closestDistance)
				{
					closestDistance = distance;
					closestNumber = number;
				}
			}
			return closestNumber;
		}

		public static int GetIndexOfClosestNumber (float f, params float[] numbers)
		{
			int output = 0;
			float closestNumber = numbers[0];
			float closestDistance = Mathf.Abs(f - closestNumber);
			for (int i = 1; i < numbers.Length; i ++)
			{
				float number = numbers[i];
				float distance = Mathf.Abs(f - number);
				if (distance < closestDistance)
				{
					closestDistance = distance;
					closestNumber = number;
					output = i;
				}
			}
			return output;
		}

		public static (float, int) GetClosestNumberAndIndex (float f, params float[] numbers)
		{
			int indexOfClosestNumer = 0;
			float closestNumber = numbers[0];
			float closestDistance = Mathf.Abs(f - closestNumber);
			for (int i = 1; i < numbers.Length; i ++)
			{
				float number = numbers[i];
				float distance = Mathf.Abs(f - number);
				if (distance < closestDistance)
				{
					closestDistance = distance;
					closestNumber = number;
					indexOfClosestNumer = i;
				}
			}
			return (closestNumber, indexOfClosestNumer);
		}

		public static float RegularizeAngle (float angle)
		{
			while (angle >= 360 || angle < 0)
				angle += Mathf.Sign(360 - angle) * 360;
			return angle;
		}

		public static float ClampAngle (float ang, float min, float max)
		{
			ang = RegularizeAngle(ang);
			min = RegularizeAngle(min);
			max = RegularizeAngle(max);
			float minDist = Mathf.Min(Mathf.DeltaAngle(ang, min), Mathf.DeltaAngle(ang, max));
			float _ang = WrapAngle(ang + Mathf.DeltaAngle(ang, minDist));
			if (_ang == min)
				return min;
			else if (_ang == max)
				return max;
			else
				return ang;
		}

		public static float WrapAngle (float ang)
		{
			if (ang < 0)
				ang += 360;
			else if (ang > 360)
				ang = 360 - ang;
			return ang;
		}

		public static float Round (float f, RoundingMethod roundingMethod)
		{
			if (roundingMethod == RoundingMethod.HalfOrMoreRoundsUp)
			{
				if (f % 1 >= 0.5f)
					return Mathf.Ceil(f);
			}
			else if (roundingMethod == RoundingMethod.HalfOrLessRoundsDown)
			{
				if (f % 1 <= 0.5f)
					return Mathf.Floor(f);
			}
			else if (roundingMethod == RoundingMethod.HalfOrMoreRoundsDown)
			{
				if (f % 1 >= 0.5f)
					return Mathf.Floor(f);
			}
			else if (roundingMethod == RoundingMethod.HalfOrLessRoundsUp)
			{
				if (f % 1 <= 0.5f)
					return Mathf.Ceil(f);
			}
			else if (roundingMethod == RoundingMethod.RoundUpIfNotInteger)
			{
				if (f % 1 != 0)
					return Mathf.Ceil(f);
			}
			else// if (roundingMethod == RoundingMethod.RoundDownIfNotInteger)
			{
				if (f % 1 != 0)
					return Mathf.Floor(f);
			}
			return f;
		}

		public static int RoundToInt (float f, RoundingMethod roundingMethod)
		{
			return (int) Round(f, roundingMethod);
		}

		public static float GetHypotenuse (float length, float length2)
		{
			return Mathf.Sqrt(length * length + length2 * length2);
		}

		public static float Clamp (float value, float min, float max)
		{
			if (value <= min)
				return min;
			else if (value >= max)
				return max;
			else
				return value;
		}

		public static int Clamp (int value, int min, int max)
		{
			if (value <= min)
				return min;
			else if (value >= max)
				return max;
			else
				return value;
		}

		public static uint Clamp (uint value, uint min, uint max)
		{
			if (value <= min)
				return min;
			else if (value >= max)
				return max;
			else
				return value;
		}

		public static float Min (params float[] values)
		{
			float smallestValue = values[0];
			for (int i = 1; i < values.Length; i ++)
			{
				float value = values[i];
				if (value < smallestValue)
					smallestValue = value;
			}
			return smallestValue;
		}

		public static int Min (params int[] values)
		{
			int smallestValue = values[0];
			for (int i = 1; i < values.Length; i ++)
			{
				int value = values[i];
				if (value < smallestValue)
					smallestValue = value;
			}
			return smallestValue;
		}

		public static uint Min (params uint[] values)
		{
			uint smallestValue = values[0];
			for (int i = 1; i < values.Length; i ++)
			{
				uint value = values[i];
				if (value < smallestValue)
					smallestValue = value;
			}
			return smallestValue;
		}

		public static float Max (params float[] values)
		{
			float biggestValue = values[0];
			for (int i = 1; i < values.Length; i ++)
			{
				float value = values[i];
				if (value > biggestValue)
					biggestValue = value;
			}
			return biggestValue;
		}

		public static int Max (params int[] values)
		{
			int biggestValue = values[0];
			for (int i = 1; i < values.Length; i ++)
			{
				int value = values[i];
				if (value > biggestValue)
					biggestValue = value;
			}
			return biggestValue;
		}

		public static uint Max (params uint[] values)
		{
			uint biggestValue = values[0];
			for (int i = 1; i < values.Length; i ++)
			{
				uint value = values[i];
				if (value > biggestValue)
					biggestValue = value;
			}
			return biggestValue;
		}
		
		public enum RoundingMethod
		{
			HalfOrMoreRoundsUp,
			HalfOrLessRoundsDown,
			HalfOrMoreRoundsDown,
			HalfOrLessRoundsUp,
			RoundUpIfNotInteger,
			RoundDownIfNotInteger
		}
	}
}
