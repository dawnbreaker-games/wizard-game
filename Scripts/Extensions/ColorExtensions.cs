using Godot;
using System;

namespace Extensions
{
	public static class ColorExtensions
	{
		public static Color SetAlpha (this Color c, float a)
		{
			return new Color(c.r, c.g, c.b, a);
		}

		public static Color AddAlpha (this Color c, float a)
		{
			return c.SetAlpha(c.a + a);
		}

		public static Color MultiplyAlpha (this Color c, float a)
		{
			return c.SetAlpha(c.a * a);
		}

		public static Color DivideAlpha (this Color c, float a)
		{
			return c.SetAlpha(c.a / a);
		}

		public static Color AddRGB (this Color c, Color add)
		{
			return new Color(c.r + add.r, c.g + add.g, c.b + add.b);
		}

		public static Color MultiplyRGB (this Color c, Color multiply)
		{
			return new Color(c.r * multiply.r, c.g * multiply.g, c.b * multiply.b);
		}

		public static Color DivideRGB (this Color c, Color divide)
		{
			return new Color(c.r / divide.r, c.g / divide.g, c.b / divide.b);
		}

		public static Color AddRGB (this Color c, float add)
		{
			return new Color(c.r + add, c.g + add, c.b + add);
		}

		public static Color MultiplyRGB (this Color c, float multiply)
		{
			return new Color(c.r * multiply, c.g * multiply, c.b * multiply);
		}

		public static Color DivideRGB (this Color c, float divide)
		{
			return new Color(c.r / divide, c.g / divide, c.b / divide);
		}

		public static byte[] ToBytes (this Color color)
		{
			byte[] bytes = new byte[4];
			bytes[0] = (byte) (255 * color.r - 128);
			return bytes;
		}

		public static Color GetRandom ()
		{
			return new Color(Random.value, Random.value, Random.value);
		}

		public static Color GetAverage (params Color[] colors)
		{
			Color output = CLEAR;
			for (int i = 1; i < colors.Length; i ++)
			{
				Color color = colors[i];
				output += color;
			}
			return output / colors.Length;
		}

		public static float GetComponentAverage (this Color c)
		{
			return (c.r + c.g + c.b) / 3;
		}

		public static Color SetGrayscale (this Color c, float brightness)
		{
			throw new NotImplementedException();
		}

		public static float GetSimilarity (this Color color, Color otherColor, bool testAlpha = false)
		{
			float rDifference = Mathf.Abs(color.r - otherColor.r);
			float gDifference = Mathf.Abs(color.g - otherColor.g);
			float bDifference = Mathf.Abs(color.b - otherColor.b);
			if (testAlpha)
			{
				float aDifference = Mathf.Abs(color.a - otherColor.a);
				return 1f - (rDifference + gDifference + bDifference + aDifference) / 4;
			}
			else
				return 1f - (rDifference + gDifference + bDifference) / 3;
		}

		public static Color GetOpposite (this Color color)
		{
			Color output = color;
			color.r = 1f - color.r;
			color.g = 1f - color.g;
			color.b = 1f - color.b;
			color.a = 1f - color.a;
			return output;
		}
	}
}
