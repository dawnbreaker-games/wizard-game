#if !IS_BUILD
using Godot;
using System.Collections.Generic;

namespace Extensions
{
	public partial class SelectionExtensions
	{
		public static T[] GetSelected<T> () where T : Node
		{
			Node[] selectedNodes = EditorPlugin.GetInterface().GetSelection().GetSelectedNodes();
			List<T> output = new List<T>();
			for (int i = 0; i < selectedNodes.Length; i ++)
			{
				Node node = selectedNodes[i];
				T _node = node as T;
				if (_node != null)
					output.Add(_node);
			}
			return output.ToArray();
		}
	}
}
#endif
