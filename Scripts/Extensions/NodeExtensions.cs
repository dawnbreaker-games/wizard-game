using Godot;
using System;
using System.Collections.Generic;

namespace Extensions
{
	public static class NodeExtensions
	{
		public static void ApplyMetadata<T> (this Node node, ref T value)
		{
			Type valueType = value.GetType();
			if (valueType.IsArray)
			{
				uint index = 0;
				Array array = default(Array);
				string valueNamePrefix = nameof(value) + "_";
				while (node.HasMeta(valueNamePrefix + index))
				{
					Type? elementType = valueType.GetElementType();
					if (elementType.IsAssignableTo(typeof(Node)))
					{
						array.SetValue(node.GetTree().CurrentScene.GetNode<Node>((string) node.GetMeta(valueNamePrefix + index)), index);
						index ++;
					}
				}
				value = (T) (object) array;
			}
			else
				value = (T) (object) node.GetMeta(nameof(value), (Variant) (object) value);
		}
		
		public static T GetParentNode<T> (this Node node, bool scanAllParents = true) where T : Node
		{
			T output = node as T;
			if (output != null)
				return output;
			else if (scanAllParents)
			{
				Node parent = node.GetParent();
				while (parent != null)
				{
					output = parent as T;
					if (output != null)
						return output;
					parent = parent.GetParent();
				}
			}
			else
				output = node.GetParent() as T;
			return output;
		}
		
		public static T GetParentInterface<T> (this Node node, bool scanAllParents = true) where T : Interface
		{
			T output = default(T);
			if (node is T)
				return (T) (Interface) node;
			else if (scanAllParents)
			{
				Node parent = node.GetParent();
				while (parent != null)
				{
					if (parent is T)
						return (T) (Interface) parent;
					parent = parent.GetParent();
				}
			}
			else
			{
				Node parent = node.GetParent();
				if (parent is T)
					return (T) (Interface) parent;
			}
			return output;
		}
		
		public static T GetChildNode<T> (this Node node, bool scanAllChildren = true) where T : Node
		{
			T output = node as T;
			if (output != null)
				return output;
			else if (scanAllChildren)
			{
				List<Node> childrenRemaining = new List<Node>(node.GetChildren());
				while (childrenRemaining.Count > 0)
				{
					Node child = childrenRemaining[0];
					childrenRemaining.RemoveAt(0);
					output = child as T;
					if (output != null)
						return output;
					childrenRemaining.AddRange(child.GetChildren());
				}
			}
			else
			{
				for (int i = 0; i < node.GetChildCount(); i ++)
				{
					Node child = node.GetChild(i);
					output = child as T;
					if (output != null)
						return output;
				}
			}
			return output;
		}
		
		public static T[] GetChildNodes<T> (this Node node, bool scanAllChildren = true) where T : Node
		{
			List<T> output = new List<T>();
			T _node = node as T;
			if (_node != null)
				output.Add(_node);
			if (scanAllChildren)
			{
				List<Node> childrenRemaining = new List<Node>(node.GetChildren());
				while (childrenRemaining.Count > 0)
				{
					Node child = childrenRemaining[0];
					childrenRemaining.RemoveAt(0);
					_node = child as T;
					if (_node != null)
						output.Add(_node);
					childrenRemaining.AddRange(child.GetChildren());
				}
			}
			else
			{
				for (int i = 0; i < node.GetChildCount(); i ++)
				{
					Node child = node.GetChild(i);
					_node = child as T;
					if (_node != null)
						output.Add(_node);
				}
			}
			return output.ToArray();
		}
	}
}
