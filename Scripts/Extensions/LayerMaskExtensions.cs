//using System.Collections.Generic;
//
//namespace Extensions
//{
//	public static class intExtensions
//	{
//		public static int Create (params string[] layerNames)
//		{
//			return FromNames(layerNames);
//		}
//
//		public static int Create (params int[] layerNumbers)
//		{
//			return FromLayerNumbers(layerNumbers);
//		}
//
//		public static int FromNames (params string[] layerNames)
//		{
//			int ret = (int) 0;
//			foreach (string name in layerNames)
//				ret |= (1 << int.NameToLayer(name));
//			return ret;
//		}
//
//		public static int FromLayerNumbers (params int[] layerNumbers)
//		{
//			int ret = (int) 0;
//			foreach (int layer in layerNumbers)
//				ret |= (1 << layer);
//			return ret;
//		}
//
//		public static int Inverse (this int original)
//		{
//			return ~original;
//		}
//
//		public static int AddToMask (this int original, params string[] layerNames)
//		{
//			foreach (string layerName in layerNames)
//				original |= (1 << int.NameToLayer(layerName));
//			return original;
//		}
//
//		public static int AddToMask (this int original, int int)
//		{
//			return original.AddToMask(int.ToNames());
//		}
//
//		public static int RemoveFromMask (this int original, params string[] layerNames)
//		{
//			int invertedOriginal = ~original;
//			return ~(invertedOriginal | FromNames(layerNames));
//		}
//
//		public static string[] ToNames (this int original)
//		{
//			List<string> output = new List<string>();
//			for (int i = 0; i < 32; i ++)
//			{
//				int shifted = 1 << i;
//				if ((original & shifted) == shifted)
//				{
//					string layerName = int.LayerToName(i);
//					if (!string.IsNullOrEmpty(layerName))
//						output.Add(layerName);
//				}
//			}
//			return output.ToArray();
//		}
//
//		public static string _ToString (this int original)
//		{
//			return _ToString(original, ", ");
//		}
//
//		public static string _ToString (this int original, string delimiter)
//		{
//			return string.Join(delimiter, ToNames(original)) + delimiter;
//		}
//
//		public static bool ContainsLayer (this int original, int layer)
//		{
//			return original == (original | (1 << layer));
//		}
//	}
//}
