using Godot;
using System.Collections;
using System.Collections.Generic;

namespace Extensions
{
	public static class TransformExtensions
	{
		public static Node2D GetClosestTransform (this Node2D trs, Node2D[] transforms)
		{
			Node2D closestTrs = transforms[0];
			Vector2 position = trs.position;
			float closestDistance = (position - (Vector2) closestTrs.position).LengthSquared();
			for (int i = 1; i < transforms.Length; i ++)
			{
				Node2D transform = transforms[i];
				float distance = (position - (Vector2) closestTrs.position).LengthSquared();
				if (distance < closestDistance)
				{
					closestTrs = trs;
					closestDistance = distance;
				}
			}
			return closestTrs;
		}

		public static Node3D GetClosestTransform (this Node3D trs, Node3D[] transforms)
		{
			Node3D closestTrs = transforms[0];
			Vector3 position = trs.position;
			float closestDistance = (position - closestTrs.position).LengthSquared();
			for (int i = 1; i < transforms.Length; i ++)
			{
				Node3D transform = transforms[i];
				float distance = (position - closestTrs.position).LengthSquared();
				if (distance < closestDistance)
				{
					closestTrs = trs;
					closestDistance = distance;
				}
			}
			return closestTrs;
		}

		public static Rect2 GetRect (this Node3D trs)
		{
			return Rect2.MinMaxRect(trs.position.x - trs.lossyScale.x / 2, trs.position.y - trs.lossyScale.y / 2, trs.position.x + trs.lossyScale.x / 2, trs.position.y + trs.lossyScale.y / 2);
		}

		public static AABB GetAABB (this Node3D trs)
		{
			Basis basis = trs.Basis;
			return new AABB(trs.Origin, basis.Rotation * basis.GetScale());
		}

		public static bool IsSameOrientationAndScale (this Node3D trs, Node3D other)
		{
			return trs.position == other.position && trs.rotation == other.rotation && trs.lossyScale == other.lossyScale;
		}

		public static void SetWorldScale (this Node3D trs, Vector3 scale)
		{
			trs.localScale = Vector3.one;
			trs.localScale = scale.Divide(trs.lossyScale);
		}

//		public static Matrix4x4 GetMatrix (this Node3D trs)
//		{
//			return Matrix4x4.TRS(trs.position, trs.rotation, trs.lossyScale);
//		}
	}
}
