using Godot;
using FightRoom;
using System.Collections;
using System.Collections.Generic;

namespace Extensions
{
	public static class VectorExtensions
	{
		public static Vector2 INFINITE2 = new Vector2(float.MaxValue, float.MaxValue);
		public static Vector3 INFINITE3 = new Vector3(float.MaxValue, float.MaxValue, float.MaxValue);
		
		public static string _ToString (this Vector3 v)
		{
			return "(" + v.X + ", " + v.Y + ", " + v.Z + ")";
		}
		
		public static string _ToString (this Vector2 v)
		{
			return "(" + v.X + ", " + v.Y + ")";
		}
		
		public static Vector3 Snap (this Vector3 v, Vector3 snap)
		{
			return new Vector3(MathfExtensions.RoundToInterval(v.X, snap.X), MathfExtensions.RoundToInterval(v.Y, snap.Y), MathfExtensions.RoundToInterval(v.Z, snap.Z));
		}

		public static Vector2 Snap (this Vector2 v, Vector2 snap)
		{
			return new Vector2(MathfExtensions.RoundToInterval(v.X, snap.X), MathfExtensions.RoundToInterval(v.Y, snap.Y));
		}
		
		public static Vector3 Multiply (this Vector3 v1, Vector3 v2)
		{
			return new Vector3(v1.X * v2.X, v1.Y * v2.Y, v1.Z * v2.Z);
		}

		public static Vector2 Multiply (this Vector2 v1, Vector2 v2)
		{
			return new Vector2(v1.X * v2.X, v1.Y * v2.Y);
		}

		public static Vector2I Multiply (this Vector2I v1, Vector2 v2)
		{
			return v1.ToVec2().Multiply(v2).ToVec2Int();
		}

		public static float Cross (this Vector2 v1, Vector2 v2)
		{
			return v1.X * v2.Y - v1.Y * v2.X;
		}

		public static float Multiply_float (this Vector2 v1, Vector2 v2)
		{
			return v1.X * v2.X + v1.Y * v2.Y;
		}
		
		public static Vector3 Divide (this Vector3 v1, Vector3 v2)
		{
			return new Vector3(v1.X / v2.X, v1.Y / v2.Y, v1.Z / v2.Z);
		}

		public static Vector2 Divide (this Vector2 v1, Vector2 v2)
		{
			return new Vector2(v1.X / v2.X, v1.Y / v2.Y);
		}
		
		public static Vector3 Rotate (this Vector3 v, float degrees)
		{
			return ((Vector2) v).Rotate(degrees);
		}
		
		public static Vector2 Rotate (this Vector2 v, float degrees)
		{
			float ang = GetFacingAngle(v) + degrees;
			ang *= Mathf.Deg2Rad;
			// ang = MathfExtensions.RegularizeAngle(ang);
			return new Vector2(Mathf.Cos(ang), Mathf.Sin(ang)).Normalized() * v.Length();
		}
		
		public static Vector2 Rotate (this Vector2 v, Vector2 pivotPoint, float degrees)
		{
			float ang = GetFacingAngle(v - pivotPoint) + degrees;
			ang *= Mathf.Deg2Rad;
			return pivotPoint + (new Vector2(Mathf.Cos(ang), Mathf.Sin(ang)).Normalized() * Vector2.Distance(v, pivotPoint));
		}
		
		public static Vector3 Rotate (this Vector3 v, Quaternion rotation)
		{
			return rotation * v;
		}

		public static Vector3 Rotate (this Vector3 v, Vector3 pivotPoint, Quaternion rotation)
		{
			Vector3 direction = (rotation * (v - pivotPoint)).Normalized();
			return pivotPoint + (direction * Vector3.Distance(v, pivotPoint));
		}
		
		public static float GetFacingAngle (this Vector2 v)
		{
			v = v.Normalized();
			return Mathf.Atan2(v.Y, v.X) * Mathf.Rad2Deg;
		}
		
		public static float GetFacingAngle (this Vector3 v)
		{
			v = v.Normalized();
			return Mathf.Atan2(v.Y, v.X) * Mathf.Rad2Deg;
		}
		
		public static Vector2 RotateTo (this Vector2 from, Vector2 to, float maxDegrees)
		{
			float ang = from.GetFacingAngle();
			ang += MathfExtensions.Clamp(Vector2.SignedAngle(from, to), -maxDegrees, maxDegrees);
			ang *= Mathf.Deg2Rad;
			return new Vector2(Mathf.Cos(ang), Mathf.Sin(ang)).Normalized() * from.Length();
		}
		
		public static Vector2 RotateTo (this Vector3 from, Vector3 to, float maxDegrees)
		{
			float ang = from.GetFacingAngle();
			ang += MathfExtensions.Clamp(Vector2.SignedAngle(from, to), -maxDegrees, maxDegrees);
			ang *= Mathf.Deg2Rad;
			return new Vector2(Mathf.Cos(ang), Mathf.Sin(ang)).Normalized() * from.Length();
		}
		
		public static Vector3 ClampComponents (this Vector3 v, Vector3 min, Vector3 max)
		{
			return new Vector3(MathfExtensions.Clamp(v.X, min.X, max.X), MathfExtensions.Clamp(v.Y, min.Y, max.Y), MathfExtensions.Clamp(v.Z, min.Z, max.Z));
		}
		
		public static Vector2 ClampComponents (this Vector2 v, Vector2 min, Vector2 max)
		{
			return new Vector2(MathfExtensions.Clamp(v.X, min.X, max.X), MathfExtensions.Clamp(v.Y, min.Y, max.Y));
		}
		
		public static Vector3I ToVec3Int (this Vector3 v, MathfExtensions.RoundingMethod roundingMethod = MathfExtensions.RoundingMethod.HalfOrLessRoundsDown)
		{
			return new Vector3I(MathfExtensions.RoundToInt(v.X, roundingMethod), MathfExtensions.RoundToInt(v.Y, roundingMethod), MathfExtensions.RoundToInt(v.Z, roundingMethod));
		}
		
		public static Vector3I ToVec3Int (this Vector2 v, MathfExtensions.RoundingMethod roundingMethod = MathfExtensions.RoundingMethod.HalfOrLessRoundsDown)
		{
			return new Vector3I(MathfExtensions.RoundToInt(v.X, roundingMethod), MathfExtensions.RoundToInt(v.Y, roundingMethod), 0);
		}

		public static Vector2I ToVec2Int (this Vector2 v, MathfExtensions.RoundingMethod roundingMethod = MathfExtensions.RoundingMethod.HalfOrLessRoundsDown)
		{
			return new Vector2I(MathfExtensions.RoundToInt(v.X, roundingMethod), MathfExtensions.RoundToInt(v.Y, roundingMethod));
		}

		public static Vector2I ToVec2Int (this Vector3 v, MathfExtensions.RoundingMethod roundingMethod = MathfExtensions.RoundingMethod.HalfOrLessRoundsDown)
		{
			return new Vector2I(MathfExtensions.RoundToInt(v.X, roundingMethod), MathfExtensions.RoundToInt(v.Y, roundingMethod));
		}

		public static Vector3 ToVec3 (this Vector4 v)
		{
			return new Vector3(v.X, v.Y, v.Z);
		}

		public static Vector3I ToVec3Int (this Vector4 v)
		{
			return new Vector3I((int) v.X, (int) v.Y, (int) v.Z);
		}

		public static Vector2 ToVec2 (this Vector2I v)
		{
			return new Vector2(v.X, v.Y);
		}

		public static Vector2 ToVec2 (this Vector3I v)
		{
			return new Vector2(v.X, v.Y);
		}

		public static Vector3 ToVec3 (this Vector2I v)
		{
			return new Vector3(v.X, v.Y);
		}

		public static Vector2I ToVec2Int (this Vector3I v)
		{
			return new Vector2I(v.X, v.Y);
		}

		public static Vector3I ToVec3Int (this Vector2I v)
		{
			return new Vector3I(v.X, v.Y, 0);
		}
		
		public static Vector3 SetX (this Vector3 v, float x)
		{
			return new Vector3(x, v.Y, v.Z);
		}
		
		public static Vector3 SetY (this Vector3 v, float y)
		{
			return new Vector3(v.X, y, v.Z);
		}
		
		public static Vector3 SetZ (this Vector3 v, float z)
		{
			return new Vector3(v.X, v.Y, z);
		}
		
		public static Vector2 SetX (this Vector2 v, float x)
		{
			return new Vector2(x, v.Y);
		}
		
		public static Vector2 SetY (this Vector2 v, float y)
		{
			return new Vector2(v.X, y);
		}

		public static Vector3 SetZ (this Vector2 v, float z)
		{
			return new Vector3(v.X, v.Y, z);
		}

		public static Vector3I SetZ (this Vector3I v, int z)
		{
			return new Vector3I(v.X, v.Y, z);
		}

		public static Vector2 GetXZ (this Vector3 v)
		{
			return new Vector2(v.X, v.Z);
		}

		public static Vector2I GetXZ (this Vector3I v)
		{
			return new Vector2I(v.X, v.Z);
		}

		public static Vector2 GetYZ (this Vector3 v)
		{
			return new Vector2(v.Y, v.Z);
		}

		public static Vector2I GetYZ (this Vector3I v)
		{
			return new Vector2I(v.Y, v.Z);
		}
		
		public static Vector3 XYToXZ (this Vector2 v)
		{
			return new Vector3(v.X, 0, v.Y);
		}
		
		public static Vector3 XYToXZ (this Vector3 v)
		{
			return new Vector3(v.X, 0, v.Y);
		}
		
		public static Vector2 XZToXY (this Vector3 v)
		{
			return new Vector2(v.X, v.Z);
		}
		
		public static Vector2 ClampLength (this Vector2 v, float maxLength)
		{
			return v.Normalized() * Clamp(v.Length(), 0, maxLength);
		}

		public static Vector2 SetToMinComponents (this Vector2 v, Vector2 v2)
		{
			return new Vector2(Mathf.Min(v.X, v2.X), Mathf.Min(v.Y, v2.Y));
		}

		public static Vector2 SetToMaxComponents (this Vector2 v, Vector2 v2)
		{
			return new Vector2(Mathf.Max(v.X, v2.X), Mathf.Max(v.Y, v2.Y));
		}

		public static Vector2I SetToMinComponents (this Vector2I v, Vector2I v2)
		{
			return new Vector2I(Mathf.Min(v.X, v2.X), Mathf.Min(v.Y, v2.Y));
		}

		public static Vector2I SetToMaxComponents (this Vector2I v, Vector2I v2)
		{
			return new Vector2I(Mathf.Max(v.X, v2.X), Mathf.Max(v.Y, v2.Y));
		}
		
		public static Vector2 FromFacingAngle (float angle)
		{
			angle *= Mathf.Deg2Rad;
			return new Vector2(Mathf.Cos(angle), Mathf.Sin(angle)).Normalized();
		}
		
		public static Vector2I FromFacingAngle (float angle, float maxLength)
		{
			angle *= Mathf.Deg2Rad;
			Vector2 actualResult = new Vector2(Mathf.Cos(angle), Mathf.Sin(angle)).Normalized() * maxLength;
			// Find what actualResult is closest to
			return actualResult.ToVec2Int();
		}

		public static Vector3 GetClosestPoint (Vector3 v, params Vector3[] points)
		{
			Vector3 closestPoint = points[0];
			float closestDistanceSqr = (v - closestPoint).LengthSquared();
			for (int i = 1; i < points.Length; i ++)
			{
				Vector3 point = points[i];
				float distanceSqr = (v - point).LengthSquared();
				if (distanceSqr < closestDistanceSqr)
				{
					closestPoint = point;
					closestDistanceSqr = distanceSqr;
				}
			}
			return closestPoint;
		}

		public static int GetIndexOfClosestPoint (Vector3 v, params Vector3[] points)
		{
			int indexOfClosestPoint = 0;
			Vector3 closestPoint = points[0];
			float closestDistanceSqr = (v - closestPoint).LengthSquared();
			for (int i = 1; i < points.Length; i ++)
			{
				Vector3 point = points[i];
				float distanceSqr = (v - point).LengthSquared();
				if (distanceSqr < closestDistanceSqr)
				{
					closestPoint = point;
					closestDistanceSqr = distanceSqr;
					indexOfClosestPoint = i;
				}
			}
			return indexOfClosestPoint;
		}

		public static Vector2 GetClosestPoint (Vector2 v, params Vector2[] points)
		{
			Vector2 closestPoint = points[0];
			float closestDistanceSqr = (v - closestPoint).LengthSquared();
			for (int i = 1; i < points.Length; i ++)
			{
				Vector2 point = points[i];
				float distanceSqr = (v - point).LengthSquared();
				if (distanceSqr < closestDistanceSqr)
				{
					closestPoint = point;
					closestDistanceSqr = distanceSqr;
				}
			}
			return closestPoint;
		}

		public static int GetIndexOfClosestPoint (Vector2 v, params Vector2[] points)
		{
			int indexOfClosestPoint = 0;
			Vector2 closestPoint = points[0];
			float closestDistanceSqr = (v - closestPoint).LengthSquared();
			for (int i = 1; i < points.Length; i ++)
			{
				Vector2 point = points[i];
				float distanceSqr = (v - point).LengthSquared();
				if (distanceSqr < closestDistanceSqr)
				{
					closestPoint = point;
					closestDistanceSqr = distanceSqr;
					indexOfClosestPoint = i;
				}
			}
			return indexOfClosestPoint;
		}

		public static float Sign (Vector2 p1, Vector2 p2, Vector2 p3)
		{
			return (p1.X - p3.X) * (p2.Y - p3.Y) - (p2.X - p3.X) * (p1.Y - p3.Y);
		}

		public static bool IsInTriangle (Vector2 pt, Vector2 v1, Vector2 v2, Vector2 v3)
		{
			float d1, d2, d3;
			bool has_neg, has_pos;
			d1 = Sign(pt, v1, v2);
			d2 = Sign(pt, v2, v3);
			d3 = Sign(pt, v3, v1);
			has_neg = (d1 < 0) || (d2 < 0) || (d3 < 0);
			has_pos = (d1 > 0) || (d2 > 0) || (d3 > 0);
			return !(has_neg && has_pos);
		}

		public static bool IsFacingAngleWithinAngleRange (Vector2I v, AngleRange angleRange, bool equalAnglesCountsAsIn = false)
		{
			return new Angle(v.ToVec2().GetFacingAngle()).IsWithinAngleRange(angleRange, equalAnglesCountsAsIn);
		}

		public static Vector2I Snap (Vector2I v, Angle snap, bool searchClockwise = false)
		{
			Vector2I output = (Vector2.right * v.Length()).ToVec2Int();
			float minDegreesBetween = float.MaxValue;
			float degreesBetween;
			Angle currentAngle = new Angle();
			do
			{
				degreesBetween = Mathf.Abs(currentAngle.degrees - snap.degrees);
				if (minDegreesBetween > degreesBetween)
				{
					minDegreesBetween = degreesBetween;
					output = VectorExtensions.FromFacingAngle(currentAngle.degrees, v.Length());
				}
				currentAngle.degrees += snap.degrees;
			} while (currentAngle.degrees < 360);
			return output;
		}

		public static Vector2 FlipY (this Vector2 v)
		{
			v.Y *= -1;
			return v;
		}

		public static Vector3 MakePositive (this Vector3 v)
		{
			v.X = Mathf.Abs(v.X);
			v.Y = Mathf.Abs(v.Y);
			v.Z = Mathf.Abs(v.Z);
			return v;
		}

		public static Vector2 MakePositive (this Vector2 v)
		{
			v.X = Mathf.Abs(v.X);
			v.Y = Mathf.Abs(v.Y);
			return v;
		}

		public static float[] ToArray (this Vector2 v)
		{
			return new float[2] { v.X, v.Y };
		}

		public static float[] ToArray (this Vector3 v)
		{
			return new float[3] { v.X, v.Y, v.Z };
		}

		public static int[] ToArray (this Vector2I v)
		{
			return new int[2] { v.X, v.Y };
		}

		public static int[] ToArray (this Vector3I v)
		{
			return new int[3] { v.X, v.Y, v.Z };
		}

		public static Vector3 GetAverage (params Vector3[] points)
		{
			Vector3 output = points[0];
			for (int i = 1; i < points.Length; i ++)
			{
				Vector3 point = points[i];
				output += point;
			}
			output /= points.Length;
			return output;
		}

		public static Vector2 GetAverage (params Vector2[] points)
		{
			Vector2 output = points[0];
			for (int i = 1; i < points.Length; i ++)
			{
				Vector2 point = points[i];
				output += point;
			}
			output /= points.Length;
			return output;
		}

		public static Vector2 Rotate90 (this Vector2 v)
		{
			return new Vector2(-v.Y, v.X);
		}

		public static Vector2 Rotate270 (this Vector2 v)
		{
			return new Vector2(v.Y, -v.X);
		}

		public static uint GetManhattenDistance (Vector2I position, Vector2I position2)
		{
			return (uint) GetManhattenDistance(position, position2);
		}

		public static float GetManhattenDistance (Vector2 position, Vector2 position2)
		{
			return Mathf.Abs(position2.X - position.X) + Mathf.Abs(position2.Y - position.Y);
		}

		public static float GetManhattenLength (this Vector2 position)
		{
			return Mathf.Abs(position.X) + Mathf.Abs(position.Y);
		}

		public static Vector2 RandomDirection ()
		{
			return FromFacingAngle(GameManager.random.Randf(360));
		}

		public static Vector2 RandomVector ()
		{
			return RandomDirection() * GameManager.random.Randf();
		}
	}
}
