using Godot;
using System.Collections;
using System.Collections.Generic;

namespace Extensions
{
	public static class Rect2Extensions
	{
		public static Rect2 Move (this Rect2 rect, Vector2 movement)
		{
			rect.position += movement;
			return rect;
		}
		
		public static Rect2I Move (this Rect2I rect, Vector2I movement)
		{
			rect.position += movement;
			return rect;
		}

		public static Rect2 SwapXAndY (this Rect2 rect)
		{
			return Rect2.MinMaxRect(rect.Position.Y, rect.Position.X, rect.End.Y, rect.End.X);
		}
		
		public static bool IsEncapsulating (this Rect2 r1, Rect2 r2, bool equalRectsRetunsTrue)
		{
			if (equalRectsRetunsTrue)
			{
				bool minIsOk = r1.Position.X <= r2.Position.X && r1.Position.Y <= r2.Position.Y;
				bool maxIsOk = r1.End.X >= r2.End.X && r1.End.Y >= r2.End.Y;
				return minIsOk && maxIsOk;
			}
			else
			{
				bool minIsOk = r1.Position.X < r2.Position.X && r1.Position.Y < r2.Position.Y;
				bool maxIsOk = r1.End.X > r2.End.X && r1.End.Y > r2.End.Y;
				return minIsOk && maxIsOk;
			}
		}
		
		public static bool IsIntersecting (this Rect2 r1, Rect2 r2, bool equalRectsRetunsTrue = true)
		{
			if (equalRectsRetunsTrue)
				return r1.XMin <= r2.XMax && r1.XMax >= r2.XMin && r1.YMin <= r2.YMax && r1.YMax >= r2.YMin;
			else
				return r1.XMin < r2.XMax && r1.XMax > r2.XMin && r1.YMin < r2.YMax && r1.YMax > r2.YMin;
		}
		
		public static bool IsEncapsulating (this Rect2I r1, Rect2I r2, bool equalRectsRetunsTrue)
		{
			if (equalRectsRetunsTrue)
			{
				bool minIsOk = r1.Position.X <= r2.Position.X && r1.Position.Y <= r2.Position.Y;
				bool maxIsOk = r1.End.X >= r2.End.X && r1.End.Y >= r2.End.Y;
				return minIsOk && maxIsOk;
			}
			else
			{
				bool minIsOk = r1.Position.X < r2.Position.X && r1.Position.Y < r2.Position.Y;
				bool maxIsOk = r1.End.X > r2.End.X && r1.End.Y > r2.End.Y;
				return minIsOk && maxIsOk;
			}
		}
		
		public static bool IsIntersecting (this Rect2I r1, Rect2I r2, bool equalRectsRetunsTrue = true)
		{
			if (equalRectsRetunsTrue)
				return r1.XMin <= r2.XMax && r1.XMax >= r2.XMin && r1.YMin <= r2.YMax && r1.YMax >= r2.YMin;
			else
				return r1.XMin < r2.XMax && r1.XMax > r2.XMin && r1.YMin < r2.YMax && r1.YMax > r2.YMin;
		}

		public static Vector2[] GetCorners (this Rect2 rect)
		{
			Vector2[] output = new Vector2[4];
			output[0] = rect.Position;
			output[1] = new Vector2(rect.XMax, rect.YMin);
			output[2] = new Vector2(rect.XMin, rect.YMax);
			output[3] = rect.End;
			return output;
		}
		
		public static bool IsExtendingOutside (this Rect2 r1, Rect2 r2, bool equalRectsRetunsTrue)
		{
			if (equalRectsRetunsTrue)
			{
				bool minIsOk = r1.Position.X <= r2.Position.X || r1.Position.Y <= r2.Position.Y;
				bool maxIsOk = r1.End.X >= r2.End.X || r1.End.Y >= r2.End.Y;
				return minIsOk || maxIsOk;
			}
			else
			{
				bool minIsOk = r1.Position.X < r2.Position.X || r1.Position.Y < r2.Position.Y;
				bool maxIsOk = r1.End.X > r2.End.X || r1.End.Y > r2.End.Y;
				return minIsOk || maxIsOk;
			}
		}
		
		public static Rect2 ToRect (this AABB aabb)
		{
			return Rect2.MinMaxRect(aabb.Position.X, aabb.Position.Y, aabb.End.X, aabb.End.Y);
		}
		
		public static Rect2I ToRect2I (this AABB aabb, MathfExtensions.RoundingMethod minRoundingMethod = MathfExtensions.RoundingMethod.RoundDownIfNotInteger, MathfExtensions.RoundingMethod maxRoundingMethod = MathfExtensions.RoundingMethod.RoundUpIfNotInteger)
		{
			return FromMinAndMax(aabb.Position.ToVec2Int(minRoundingMethod), aabb.End.ToVec2Int(maxRoundingMethod));
		}

		public static Rect2 Combine (this Rect2[] rectsArray)
		{
			Rect2 output = rectsArray[0];
			for (int i = 1; i < rectsArray.Length; i ++)
			{
				Rect2 rect = rectsArray[i];
				if (rect.XMin < output.XMin)
					output.XMin = rect.XMin;
				if (rect.YMin < output.YMin)
					output.YMin = rect.YMin;
				if (rect.XMax > output.XMax)
					output.XMax = rect.XMax;
				if (rect.YMax > output.YMax)
					output.YMax = rect.YMax;
			}
			return output;
		}

		public static Rect2I Combine (this Rect2I[] rectsArray)
		{
			Rect2I output = rectsArray[0];
			for (int i = 1; i < rectsArray.Length; i ++)
			{
				Rect2I rect = rectsArray[i];
				if (rect.XMin < output.XMin)
					output.XMin = rect.XMin;
				if (rect.YMin < output.YMin)
					output.YMin = rect.YMin;
				if (rect.XMax > output.XMax)
					output.XMax = rect.XMax;
				if (rect.YMax > output.YMax)
					output.YMax = rect.YMax;
			}
			return output;
		}

		public static Rect2 Expand (this Rect2 rect, Vector2 amount)
		{
			Vector2 center = rect.center;
			rect.size += amount;
			rect.center = center;
			return rect;
		}

		public static Rect2I Expand (this Rect2I rect, Vector2I amount)
		{
			rect.Position -= amount / 2;
			rect.End += amount / 2;
			return rect;
		}
		
		public static Rect2 Set (this Rect2 rect, Rect2I Rect2I)
		{
			rect.center = Rect2I.center;
			rect.size = Rect2I.size;
			return rect;
		}
		
		public static Rect2I FromMinAndSize (Vector2I min, Vector2I size)
		{
			Rect2I rect = new Rect2I();
			rect.position = min;
			rect.size = size;
			return rect;
		}
		
		public static Rect2I FromMinAndMax (Vector2I min, Vector2I max)
		{
			Rect2I rect = new Rect2I();
			rect.SetMinMax(min, max);
			return rect;
		}

		public static Rect2 ToRect (this Rect2I Rect2I)
		{
			return Rect2.MinMaxRect(Rect2I.XMin, Rect2I.YMin, Rect2I.XMax, Rect2I.YMax);
		}

		public static Rect2I ToRect2I (this Rect2 rect, MathfExtensions.RoundingMethod minRoundingMethod = MathfExtensions.RoundingMethod.RoundDownIfNotInteger, MathfExtensions.RoundingMethod maxRoundingMethod = MathfExtensions.RoundingMethod.RoundUpIfNotInteger)
		{
			return FromMinAndMax(rect.Position.ToVec2Int(minRoundingMethod), rect.End.ToVec2Int(maxRoundingMethod));
		}

		public static Vector2 ClosestPoint (this Rect2 rect, Vector2 point)
		{
			return point.ClampComponents(rect.Position, rect.End);
		}

		public static Vector2 ToNormalizedPosition (this Rect2 rect, Vector2 point)
		{
			return Rect2.PointToNormalized(rect, point);
			// return Vector2.one.Divide(rect.size) * (point - rect.Position);
		}

		public static Vector2 ToNormalizedPosition (this Rect2I rect, Vector2I point)
		{
			return Vector2.one.Divide(rect.size.ToVec2()).Multiply(point.ToVec2() - rect.Position.ToVec2());
		}

		public static Rect2 SetToPositiveSize (this Rect2 rect)
		{
			Rect2 output = rect;
			output.size = new Vector2(Mathf.Abs(output.size.X), Mathf.Abs(output.size.Y));
			output.center = rect.center;
			return output;
		}

		public static Circle2D GetSmallestCircleAround (this Rect2 rect)
		{
			return new Circle2D(rect.center, rect.size.Length() / 2);
		}

		// public static Rect2 GetExactFitRectForCircle (Circle2D circle)
		// {
		// }

		public static Rect2 AnchorToPoint (this Rect2 rect, Vector2 point, Vector2 anchorPoint)
		{
			Rect2 output = rect;
			output.position = point - (output.size * anchorPoint);
			return output;
		}

		public static LineSegment2D[] GetEdges (this Rect2 rect)
		{
			return new LineSegment2D[4] { new LineSegment2D(rect.Position, new Vector2(rect.XMin, rect.YMax)), new LineSegment2D(rect.Position, new Vector2(rect.XMax, rect.YMin)), new LineSegment2D(rect.End, new Vector2(rect.XMin, rect.YMax)), new LineSegment2D(rect.End, new Vector2(rect.XMax, rect.YMin)) };
		}

		public static AABB ToBounds (this Rect2 rect)
		{
			return new AABB(rect.center, rect.size);
		}

		public static AABB ToBounds (this Rect2I rect)
		{
			return new AABB(rect.center, rect.size.ToVec2());
		}

//		public static BoundsInt ToBoundsInt (this Rect2I rect, MathfExtensions.RoundingMethod minRoundingMethod = MathfExtensions.RoundingMethod.RoundUpIfNotInteger, MathfExtensions.RoundingMethod maxRoundingMethod = MathfExtensions.RoundingMethod.RoundUpIfNotInteger)
//		{
//			return rect.ToBounds().ToBoundsInt(minRoundingMethod, maxRoundingMethod);
//		}

		public static Rect2 GrowToPoint (this Rect2 rect, Vector2 point)
		{
			rect.Position = rect.Position.SetToMinComponents(point);
			rect.End = rect.End.SetToMaxComponents(point);
			return rect;
		}

		public static float GetPerimeter (this Rect2 rect)
		{
			return rect.size.X * 2 + rect.size.Y * 2;
		}

		public static Vector2 GetPointOnEdges (this Rect2 rect, float distance)
		{
			if (distance <= rect.size.Y)
				return new Vector2(rect.XMin, rect.YMin + distance);
			else if (distance <= rect.size.Y + rect.size.X)
				return new Vector2(rect.XMin + distance - rect.size.Y, rect.YMax);
			else if (distance <= rect.size.Y * 2 + rect.size.X)
				return new Vector2(rect.XMax, rect.YMax - (distance - rect.size.X - rect.size.Y));
			else
				return new Vector2(rect.XMax - (distance - rect.size.X - rect.size.Y * 2), rect.YMin);
		}
		
		public static Vector3 RandomPoint (this Rect2 b)
		{
			return new Vector2(Random.Range(b.XMin, b.XMax), Random.Range(b.YMin, b.YMax));
		}
	}
}
