using Godot;
using FightRoom;
using Extensions;

public partial class _Slider : Slider
{
	public _Text displayValueText;
	public float[] snapValues = new float[0];
	string initDisplayValueTextString;
	
	public override void _Ready ()
	{
		if (displayValueText != null)
		{
			initDisplayValueTextString = displayValueText.Text;
			SetDisplayValue ();
		}
		OnValueChanged ();
	}

	public void OnValueChanged ()
	{
		if (snapValues.Length > 0)
			Value = MathfExtensions.GetClosestNumber((float) Value, snapValues);
	}
	
	public void SetDisplayValue ()
	{
		if (displayValueText != null)
			displayValueText.Text = initDisplayValueTextString + Value;
	}
}
