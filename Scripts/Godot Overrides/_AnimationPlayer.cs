using Godot;
using System;
using Extensions;
using System.Collections;
using System.Collections.Generic;

namespace FightRoom
{
	public partial class _AnimationPlayer : AnimationPlayer
	{
		// public static List<_AnimationPlayer> instances = new List<_AnimationPlayer>();
		public static _AnimationPlayer[] instances = new _AnimationPlayer[0];

#if !IS_BUILD
		void OnValidate ()
		{
			if (!Application.isPlaying)
			{
				if (animator == null)
					animator = GetComponent<Animator>();
				return;
			}
		}
#endif

		// void _Ready ()
		// {
		// 	instances.Add(this);
		// }

		// void OnDisable ()
		// {
		// 	instances.Remove(this);
		// }

		public void PauseAnimator (Func<bool> whenToUnpause)
		{
			animator.enabled = false;
			GameManager.updatables = GameManager.updatables.Add(new UnpauseUpdater(this, whenToUnpause));
		}

		public partial class UnpauseUpdater : IUpdatable
		{
			public _AnimationPlayer animator;
			public Func<bool> shouldUnpause;

			public UnpauseUpdater (_AnimationPlayer animator, Func<bool> shouldUnpause)
			{
				this.animator = animator;
				this.shouldUnpause = shouldUnpause;
			}
			
			public void DoUpdate ()
			{
				if (shouldUnpause())
				{
					animator.animator.enabled = true;
					GameManager.updatables = GameManager.updatables.Remove(this);
				}
			}
		}
	}
}
