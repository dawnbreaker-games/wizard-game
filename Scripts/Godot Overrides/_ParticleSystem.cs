//using Godot;
//using FightRoom;
//using System.Collections;
//using System.Collections.Generic;
//
//public partial class _ParticleSystem : Spawnable
//{
//	public ParticleSystem particleSystem;
//	ParticleSystem duplicateParticleSystem;
//
//	public override void Start ()
//	{
//#if !IS_BUILD
//		base.Start ();
//		if (!Application.isPlaying)
//		{
//			if (particleSystem == null)
//				particleSystem = GetComponent<ParticleSystem>();
//			return;
//		}
//#endif
//		if (Level.instance.type == Level.Type.Playback && duplicateParticleSystem == null)
//		{
//			gameObject.SetActive(false);
//			duplicateParticleSystem = Instantiate(particleSystem);
//			Destroy(duplicateParticleSystem.GetComponent<_ParticleSystem>());
//			gameObject.SetActive(true);
//			EventManager.AddEvent((object obj) => { duplicateParticleSystem.gameObject.SetActive(true); }, Level.instance.playbackTimeOffset);
//		}
//	}
//}
