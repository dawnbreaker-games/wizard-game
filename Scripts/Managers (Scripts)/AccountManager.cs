using System;

namespace FightRoom
{
	public partial class AccountManager : SingletonNode<AccountManager>, ISaveableAndLoadable
	{
		public static Account CurrentAccount
		{
			get
			{
				return instance.accounts[currentAccountIndex];
			}
			set
			{
				instance.accounts[currentAccountIndex] = value;
			}
		}
		public static int currentAccountIndex;
		[SaveAndLoadValue]
		public Account[] accounts = new Account[0];

		[Serializable]
		public partial class Account
		{
			public string name;
			public string password;
		}
	}
}
