using System;
using FightRoom;
using System.Collections.Generic;

public partial class PhysicsManager2D : SingletonNode2D<PhysicsManager2D>
{
	public LayerCollision[] layerCollisions = new LayerCollision[0];
	public static Dictionary<string, string[]> layerCollisionsDict = new Dictionary<string, string[]>();

	public override void _Ready ()
	{
		base._Ready ();
		layerCollisionsDict.Clear();
		for (int i = 0; i < layerCollisions.Length; i ++)
		{
			LayerCollision layerCollision = layerCollisions[i];
			layerCollisionsDict.Add(layerCollision.layerName, layerCollision.collidingLayers);
		}
	}

	[Serializable]
	public struct LayerCollision
	{
		public string layerName;
		public string[] collidingLayers;
	}
}
