using Godot;
using System;
using System.IO;
using Extensions;
using System.Collections;
using System.Collections.Generic;
using System.Runtime.Serialization.Formatters.Binary;

namespace FightRoom
{
	public partial class SaveAndLoadManager : SingletonNode<SaveAndLoadManager>
	{
		public static SaveData saveData = new SaveData();
		public static string MostRecentSaveFileName
		{
			get
			{
				return PlayerPrefs.GetString("Most recent save file name", null);
			}
			set
			{
				PlayerPrefs.SetString("Most recent save file name", value);
			}
		}
		public static string filePath;
		
		void Start ()
		{
			filePath = Application.persistentDataPath + Path.DirectorySeparatorChar + "Auto-Save";
			GD.Print(filePath);
		}

		static void OnAboutToSave ()
		{
			Achievement.instances = FindObjectsOfType<Achievement>();
			List<string> completeAchievementsNames = new List<string>();
			for (int i = 0; i < Achievement.instances.Length; i ++)
			{
				Achievement achievement = Achievement.instances[i];
				if (achievement.complete)
					completeAchievementsNames.Add(achievement.name);
			}
			if (saveData.completeAchievementsNames == null)
				saveData.completeAchievementsNames = new string[0];
			if (saveData.bestLevelTimesDict == null)
				saveData.bestLevelTimesDict = new Dictionary<string, float>();
			if (saveData.triedPlayers == null)
				saveData.triedPlayers = new string[0];
			saveData.completeAchievementsNames = completeAchievementsNames.ToArray();
			if (Level.instance != null)
				saveData.levelName = Level.instance.name;
			if (Player.instance != null)
				saveData.playerName = Player.instance.name;
			saveData.inputRebindings = InputManager.instance.inputActionAsset.SaveBindingOverridesAsJson();
		}
		
		public static void Save (string fileName)
		{
			OnAboutToSave ();
			FileStream fileStream = new FileStream(fileName, FileMode.Create);
			BinaryFormatter binaryFormatter = new BinaryFormatter();
			binaryFormatter.Serialize(fileStream, saveData);
			fileStream.Close();
			MostRecentSaveFileName = fileName;
		}
		
		public void Load (string fileName)
		{
			GD.Print(fileName);
			FileStream fileStream = new FileStream(fileName, FileMode.Open);
			BinaryFormatter binaryFormatter = new BinaryFormatter();
			saveData = (SaveData) binaryFormatter.Deserialize(fileStream);
			fileStream.Close();
			OnLoad (fileName);
		}

		void OnLoad (string fileName)
		{
			MostRecentSaveFileName = fileName;
			OnLoaded ();
		}

		void OnLoaded ()
		{
//			Achievement.instances = FindObjectsOfType<Achievement>();
//			for (int i = 0; i < Achievement.instances.Length; i ++)
//			{
//				Achievement achievement = Achievement.instances[i];
//				achievement.Init ();
//			}
//			Level.instances = FindObjectsOfType<Level>();
//			for (int i = 0; i < Level.instances.Length; i ++)
//			{
//				Level level = Level.instances[i];
//				if (level.name == saveData.levelName)
//				{
//					Level.instance = level;
//					break;
//				}
//			}
//			InputManager.instance.inputActionAsset.LoadBindingOverridesFromJson(saveData.inputRebindings);
//			if (PlayerSelectMenu.instance == null || string.IsNullOrEmpty(saveData.playerName))
//				return;
//			Player player = PlayerSelectMenu.instance.playersParent.Find(saveData.playerName).GetComponent<Player>();
//			PlayerSelectMenu.players = PlayerSelectMenu.instance.playersParent.GetChildNodes<Player>();
//			PlayerSelectMenu.currentPlayerIndex = (uint) PlayerSelectMenu.players.IndexOf(player);
//			PlayerSelectMenu.instance.SetCurrentPlayer (player);
		}
		
		public void LoadMostRecent ()
		{
			Load (MostRecentSaveFileName);
		}
		
		[Serializable]
		public struct SaveData
		{
			public string[] completeAchievementsNames;
			public string levelName;
			public string playerName;
			public Dictionary<string, float> bestLevelTimesDict;
			public string[] triedPlayers;
			public string inputRebindings;
		}
	}
}
