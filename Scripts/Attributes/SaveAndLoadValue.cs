using System;
using System.Collections;
using System.Collections.Generic;

[AttributeUsage(AttributeTargets.Property | AttributeTargets.Field)]
public partial class SaveAndLoadValueAttribute : Attribute
{
}
