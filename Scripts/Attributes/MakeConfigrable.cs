﻿using System;

[AttributeUsage(AttributeTargets.Property | AttributeTargets.Field)]
public partial class MakeConfigurable : Attribute
{
}
