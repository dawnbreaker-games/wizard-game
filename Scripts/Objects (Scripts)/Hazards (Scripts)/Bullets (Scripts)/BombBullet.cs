using Godot;
using System;
using Extensions;
using System.Collections;

namespace FightRoom
{
	public partial class BombBullet : Bullet, IUpdatable
	{
		public bool playExplodeDelayAnimEntryOnHit;
		public AnimationEntry explodeDelayAnimEntry;
		public Explosion explosionPrefab;
		public bool explodeOnHit;
		public bool explodeOnDisable;
		public float durationRemaining;
		bool exploded;

		public override void _Ready ()
		{
			base._Ready ();
			durationRemaining = duration;
			GameManager.updatables = GameManager.updatables.Add(this);
		}

		public void DoUpdate ()
		{
			if (GameManager.paused)
				return;
			durationRemaining -= Time.deltaTime;
			if (durationRemaining <= 0)
				ObjectPool.instance.Despawn (this);
		}

		public override void OnBodyEntered (Node2D node)
		{
			base.OnBodyEntered (node);
			if (!explodeOnHit)// || Level.instance.type.HasFlag(Level.Type.Teleport))
				return;
			if (playExplodeDelayAnimEntryOnHit)
				explodeDelayAnimEntry.Play ();
			else
				ObjectPool.instance.Despawn (this);
		}

		public override void OnAboutToDestroy ()
		{
			exploded = true;
			GameManager.updatables = GameManager.updatables.Remove(this);
			base.OnDisable ();
			if (!explodeOnDisable || _SceneManager.isLoading || GameManager.isQuitting)
				return;
			Explosion explosion = ObjectPool.instance.Spawn<Explosion>(explosionPrefab.ScenePath, GlobalPosition, GlobalRotation);
			explosion.maxHits = hitsTillDespawn;
		}
	}
}
