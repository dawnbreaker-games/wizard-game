using Godot;
using Extensions;
using System.Collections;
using System.Collections.Generic;

namespace FightRoom
{
	public partial class PoisonBullet : Bullet
	{
		public float addToPoisonRate;
		public float poisonToDamage;
		public float poisonDamage;
		public static Dictionary<IDestructable, PoisonUpdater> poisonUpdatersDict = new Dictionary<IDestructable, PoisonUpdater>();

		public override void OnBodyEntered (Node2D node)
		{
			base.OnBodyEntered (node);
			IDestructable destructable = node.GetParentNode<IDestructable>();
			if (destructable != null && !poisonUpdatersDict.ContainsKey(destructable))
			{
				PoisonUpdater poisonUpdater = new PoisonUpdater(node.GetParentNode<IDestructable>(), this);
				poisonUpdatersDict.Add(destructable, poisonUpdater);
				GameManager.updatables = GameManager.updatables.Add(poisonUpdater);
//				if (destructable == Player.instance)
//					GameManager.instance.acidMeterTrs.GetParent().SetActive(true);
			}
		}

		void OnBodyExited (Node2D node)
		{
			IDestructable destructable = node.GetParentNode<IDestructable>();
			if (destructable != null && (Physics2D.GetIgnoreLayerCollision(node.layer, layer) || Physics2D.OverlapCircle(node.bounds.center, node.bounds.extents.x, int.GetMask(int.LayerToName(layer))) == null))
			{
				PoisonUpdater poisonUpdater;
				if (poisonUpdatersDict.TryGetValue(destructable, out poisonUpdater))
					GameManager.updatables = GameManager.updatables.Remove(poisonUpdater);
				poisonUpdatersDict.Remove(destructable);
//				if (destructable == Player.instance)
//					GameManager.instance.acidMeterTrs.GetParent().SetActive(false);
			}
		}

		public partial class PoisonUpdater : IUpdatable
		{
			IDestructable destructable;
			PoisonBullet poisonBullet;
			float poisonAmount;

			public PoisonUpdater (IDestructable destructable, PoisonBullet poisonBullet)
			{
				this.destructable = destructable;
				this.poisonBullet = poisonBullet;
			}

			public void DoUpdate ()
			{
				if (destructable == null)
					return;
				poisonAmount += poisonBullet.addToPoisonRate * Time.deltaTime;
				while (poisonAmount > poisonBullet.poisonToDamage)
				{
					poisonAmount -= poisonBullet.poisonToDamage;
					destructable.TakeDamage (poisonBullet.poisonDamage);
				}
				if (destructable == Player.instance)
					GameManager.instance.acidMeterTrs.localScale = new Vector2(poisonAmount / poisonBullet.poisonToDamage, 1);
			}
		}
	}
}
