using Godot;
using System;
using Extensions;
using System.Collections;
using System.Collections.Generic;

namespace FightRoom
{
	public partial class Bullet : Hazard
	{
		public float range;
		public float duration;
		[Export]
		public float moveSpeed;
		public AutoDespawnMode autoDespawnMode;
		public ObjectPool.RangedDespawn rangedDespawn;
		public ObjectPool.DelayedDespawn delayedDespawn;
		public uint hitsTillDespawn;
		public int whatReducesHits;
		public Vector2 velocity;
		public Vector2 extraVelocity;
		public Action onDisable;
		public List<EventManager.Event> events = new List<EventManager.Event>();
		public Vector2 previousFacing;
		public new static List<Bullet> instances = new List<Bullet>();
		uint hitsTillDespawnRemaining;
		[Export]
		public RigidBody2D rigid;
		
		public override void _Ready ()
		{
			base._Ready ();
			rigid.LinearVelocity = VectorExtensions.FromFacingAngle(RotationDegrees) * moveSpeed;
			hitsTillDespawnRemaining = hitsTillDespawn;
			if (autoDespawnMode == AutoDespawnMode.RangedAutoDespawn)
				rangedDespawn = ObjectPool.instance.RangeDespawn(prefabIndex, gameObject, trs, range);
			else if (autoDespawnMode == AutoDespawnMode.DelayedAutoDespawn)
				delayedDespawn = ObjectPool.instance.DelayDespawn(prefabIndex, gameObject, trs, duration);
			if (moveSpeed > 0)
			{
				velocity = trsToGlobal(Vector2.Up) * moveSpeed;
				rigid.velocity = velocity + extraVelocity;
			}
			instances.Add(this);
		}

		public override void OnAboutToDestroy ()
		{
			base.OnAboutToDestroy ();
			for (int i = 0; i < events.Count; i ++)
			{
				EventManager.Event _event = events[i];
				EventManager.RemoveEvent (_event);
			}
			if (ObjectPool.instance != null && !_SceneManager.isLoading)
			{
				if (onDisable != null)
				{
					onDisable ();
					onDisable = null;
				}
				if (autoDespawnMode == AutoDespawnMode.RangedAutoDespawn)
					ObjectPool.instance.CancelRangedDespawn (rangedDespawn);
				else if (autoDespawnMode == AutoDespawnMode.DelayedAutoDespawn)
					ObjectPool.instance.CancelDelayedDespawn (delayedDespawn);
			}
			instances.Remove(this);
		}

//		public override void OnCollisionEnter (Node2D node)
//		{
//			Entity entity = coll.gameObject.GetParentNode<Entity>();
//			if (entity != null && entity.enabled)
//			{
//				ContactPoint2D contactPoint = coll.GetContact(0);
//				Instantiate(entity.bloodTrsPrefab, contactPoint.point, Quaternion.LookRotation(Vector3.Forward, -contactPoint.normal));
//			}
//			OnBodyEntered (coll.collider);
//		}

		public override void OnBodyEntered (Node2D node)
		{
			base.OnBodyEntered (node);
			if ((!Level.instance.type.HasFlag(Level.Type.Teleport) || other.gameObject.layer != int.NameToLayer("Wall")) && whatReducesHits.ContainsLayer(other.gameObject.layer))
			{
				hitsTillDespawnRemaining --;
				if (hitsTillDespawnRemaining == 0)
					ObjectPool.instance.Despawn (prefabIndex, gameObject, trs);
			}
		}

		public EventManager.Event AddEvent (Action<object> action, float timeUntilEvent = 0)
		{
			EventManager.Event _event = EventManager.AddEvent(action, timeUntilEvent, this);
			events.Add(_event);
			return _event;
		}

		public void AddEvent (EventManager.Event _event)
		{
			AddEvent (_event.onEvent, _event.timeUntilEvent);
		}

		public enum AutoDespawnMode
		{
			DontAutoDespawn,
			RangedAutoDespawn,
			DelayedAutoDespawn
		}

		public struct Snapshot
		{
			public int prefabIndex;
			public Bullet bullet;
			public Vector2 velocity;
			public Vector2 position;
			public float rotation;
			public float rangeRemaining;
			public float durationRemaining;
			public EventManager.Event[] events;

			public Snapshot (int prefabIndex, Bullet bullet, Vector2 velocity, Vector2 position, float rotation, float rangeRemaining, float durationRemaining, EventManager.Event[] events)
			{
				this.prefabIndex = prefabIndex;
				this.bullet = bullet;
				this.velocity = velocity;
				this.position = position;
				this.rotation = rotation;
				this.rangeRemaining = rangeRemaining;
				this.durationRemaining = durationRemaining;
				this.events = events;
			}

			public Snapshot (Bullet bullet) : this (bullet.prefabIndex, bullet, bullet.velocity, bullet.GlboalPosition, bullet.GlobalRotationDegrees, float.MaxValue, float.MaxValue, null)
			{
				if (bullet.rangedDespawn != null)
					rangeRemaining = bullet.rangedDespawn.rangeRemaining;
				else if (bullet.delayedDespawn != null)
					durationRemaining = bullet.delayedDespawn.timeRemaining;
				events = new EventManager.Event[bullet.events.Count];
				for (int i = 0; i < events.Length; i ++)
					events[i] = new EventManager.Event(bullet.events[i]);
			}

			public Bullet Apply (bool useEvents = true)
			{
				if (bullet == null)
					bullet = ObjectPool.instance.Spawn<Bullet>(prefabIndex, position, Quaternion.Euler(Vector3.Forward * rotation));
				else
				{
					bullet.GlobalPosition = position;
					bullet.GlobalRotationDegrees = rotation;
				}
				bullet.velocity = velocity;
				bullet.rigid.velocity = bullet.velocity + bullet.extraVelocity;
				if (rangeRemaining != float.MaxValue)
				{
					bullet.rangedDespawn.previousPosition = position;
					bullet.rangedDespawn.rangeRemaining = rangeRemaining;
				}
				else if (durationRemaining != float.MaxValue)
				{
					bullet.delayedDespawn.timeRemaining = durationRemaining;
					BombBullet bombBullet = bullet as BombBullet;
					if (bombBullet != null)
						bombBullet.durationRemaining = durationRemaining;
				}
				bullet.events.Clear();
				if (useEvents)
				{
					for (int i = 0; i < events.Length; i ++)
					{
						EventManager.Event _event = events[i];
						_event.arg = bullet;
						bullet.AddEvent (_event);
					}
				}
				return bullet;
			}
		}
	}
}
