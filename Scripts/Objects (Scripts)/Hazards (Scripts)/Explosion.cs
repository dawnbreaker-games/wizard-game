using Godot;
using Extensions;
using System.Collections;
using System.Collections.Generic;

namespace FightRoom
{
	public partial class Explosion : Hazard
	{
		public AnimationPlayer animationPlayer;
		public uint maxHits;
		public CircleShape2D circle;
		public new static List<Explosion> instances = new List<Explosion>();

		public override void _Ready ()
		{
			base._Ready ();
			instances.Add(this);
		}

		public override void OnAboutToDestroy ()
		{
			base.OnAboutToDestroy ();
			instances.Remove(this);
		}

		public void DestroyMe ()
		{
			ObjectPool.instance.Despawn (this);
		}

//		public void DamageDestructables ()
//		{
//			Collider2D[] hitColliders = Physics2D.OverlapCircleAll(GlobalPosition, circle.radius, whatIDamage);
//			List<Transform> hitTransforms = new List<Transform>();
//			for (int i = 0; i < hitColliders.Length; i ++)
//			{
//				Collider2D hitCollider = hitColliders[i];
//				hitTransforms.Add(hitCollider.GetComponent<Transform>());
//			}
//			for (int i = 0; i < Mathf.Min(hitColliders.Length, maxHits); i ++)
//			{
//				Transform closestTrs = trs.GetClosestTransform_2D(hitTransforms.ToArray());
//				hitTransforms.Remove(closestTrs);
//				IDestructable destructable = closestTrs.GetParentNode<IDestructable>();
//				destructable.TakeDamage (damage);
//			}
//		}

		public struct Snapshot
		{
			public int prefabIndex;
			public Explosion explosion;
			public Vector2 position;
			public float normalizedTime;

			public Snapshot (int prefabIndex, Explosion explosion, Vector2 position, float normalizedTime)
			{
				this.prefabIndex = prefabIndex;
				this.explosion = explosion;
				this.position = position;
				this.normalizedTime = normalizedTime;
			}

			public Snapshot (Explosion explosion) : this (explosion.prefabIndex, explosion, explosion.trs.position, float.MaxValue)
			{
				normalizedTime = explosion.animationPlayer.GetCurrentAnimatorStateInfo(0).normalizedTime;
			}

			public Explosion Apply ()
			{
				if (explosion == null)
					explosion = ObjectPool.instance.Spawn<Explosion>(prefabIndex, position);
				else
					explosion.trs.position = position;
				explosion.animationPlayer.Play("Explode", 0, normalizedTime);
				return explosion;
			}
		}
	}
}
