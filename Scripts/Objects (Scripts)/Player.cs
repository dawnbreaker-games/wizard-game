using Godot;
using System;
using Extensions;
using System.Collections;
using System.Collections.Generic;

namespace FightRoom
{
	public partial class Player : Entity
	{
//		public string displayName;
		public SortedList<string, BulletPatternEntry> bulletPatternEntriesSortedList = new SortedList<string, BulletPatternEntry>();
		public Node itemsParent;
		public Item[] items = new Item[0];
		public Weapon[] weapons = new Weapon[0];
		public UseableItem[] useableItems = new UseableItem[0];
//		public bool unlocked;
//		public Achievement unlockOnCompleteAchievement;
//		public Node lockedIndicatorGo;
//		public Node untriedIndicatorGo;
		public static Player instance;
		bool canAttack;

		public override void _Ready ()
		{
			instance = this;
			base._Ready ();
			items = itemsParent.GetChildNodes<Item>();
			useableItems = itemsParent.GetChildNodes<UseableItem>();
			weapons = itemsParent.GetChildNodes<Weapon>();
			for (int i = 0; i < items.Length; i ++)
			{
				Item item = items[i];
				item.OnGain ();
			}
//			if (unlockOnCompleteAchievement == null || unlockOnCompleteAchievement.complete || unlockOnCompleteAchievement.ShouldBeComplete())
//			{
//				unlocked = true;
//				lockedIndicatorGo.SetActive(false);
//				untriedIndicatorGo.SetActive(!SaveAndLoadManager.saveData.triedPlayers.Contains(name.Replace("(Clone)", "")));
//			}
		}

		public override void DoUpdate ()
		{
			if (GameManager.paused)
				return;
			base.DoUpdate ();
			HandleShooting ();
			HandleAbilities ();
		}

		public override void HandleRotating ()
		{
//			Vector2 aimInput = InputManager.AimInput;
//			if (aimInput != Vector2.Zero)
//				trs.rotation = Quaternion.LookRotation(Vector3.Forward, aimInput);
//			else if (InputManager.instance.inputDevice == InputManager.InputDevice.KeyboardAndMouse)
//				trs.rotation = Quaternion.LookRotation(Vector3.Forward, (Vector2) Camera.main.ScreenToWorldPoint((Vector2) InputManager.MousePosition) - (Vector2) trs.position);
		}

		public override void HandleMoving ()
		{
//			Move (InputManager.MoveInput * moveSpeed);
		}

		void HandleAbilities ()
		{
			for (int i = 0; i < useableItems.Length; i ++)
			{
				UseableItem useableItem = useableItems[i];
				if (Input.IsActionPressed(useableItem.useInputActionName))
					useableItem.TryToUse ();
			}
		}
		
		public void ShootBulletPatternEntry (string name)
		{
			if (GameManager.paused)
				return;
			if (!canAttack)
			{
				canAttack = true;
				return;
			}
			bulletPatternEntriesSortedList[name].Shoot ();
		}

		public override void TakeDamage (float amount)
		{
			if (dead || maxHp < 0)
				return;
			hp = MathfExtensions.Clamp(hp - amount, 0, maxHp);
			if (hp == 0)
			{
				dead = true;
				Death ();
			}
		}

		public override void Death ()
		{
//			Level.instance.End ();
			dead = false;
		}

		void HandleShooting ()
		{
			for (int i = 0; i < weapons.Length; i ++)
			{
				Weapon weapon = weapons[i];
//				if (InputManager.ShootInput)
					weapon.animationEntry.Play ();
			}
		}

		void Move (Vector2 move)
		{
			move = move.ClampLength(moveSpeed);
//			if (Level.instance.type.HasFlag(Level.Type.Wind))
//				move += ((Vector2) (trs.position - Level.instance.trs.position)).Rotate270().Normalized() * Level.instance.windSpeed;
			characterBody.Velocity = move.XYToXZ().SetY(characterBody.Velocity.Y);
			characterBody.MoveAndSlide();
		}
	}
}
