#if !IS_BUILD
using Godot;

namespace FightRoom
{
	public partial class SetAudioStreamPitchForDuration : EditorScript
	{
		public AudioStream audioStream;
		public float duration;

		public override void Do ()
		{
			_Do (audioStream, duration);
		}

		public static void _Do (AudioStream audioStream, float duration)
		{
			audioStream.Pitch = audioStream.Clip.Length / duration;
		}
	}
}
#else
namespace FightRoom
{
	public partial class SetAudioStreamPitchForDuration : EditorScript
	{
	}
}
#endif
