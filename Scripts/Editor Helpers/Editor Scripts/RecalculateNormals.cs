//#if !IS_BUILD
//
//
//namespace FightRoom
//{
//	public partial class RecalculateNormals : EditorScript
//	{
//		public MeshFilter meshFilter;
//		public bool useSharedMesh;
//
//		public override void Do ()
//		{
//			if (meshFilter == null)
//				meshFilter = GetComponent<MeshFilter>();
//			Mesh mesh;
//			if (useSharedMesh)
//				mesh = meshFilter.sharedMesh;
//			else
//				mesh = meshFilter.mesh;
//			mesh.RecalculateNormals();
//		}
//	}
//}
//#else
//namespace FightRoom
//{
//	public partial class RecalculateNormals : EditorScript
//	{
//	}
//}
//#endif
