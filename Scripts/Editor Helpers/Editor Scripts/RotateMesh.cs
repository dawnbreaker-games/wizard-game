//#if !IS_BUILD
//using Godot;
//
//namespace FightRoom
//{
//	public partial class RotateMesh : EditorScript
//	{
//		public Mesh mesh;
//		public Vector3 rotation;
//
//		public override void Do ()
//		{
//			_Do (mesh, rotation);
//		}
//
//		public static void _Do (Mesh mesh, Vector3 rotation)
//		{
//			Vector3[] vertices = mesh.vertices;
//			for (int i = 0; i < vertices.Length; i ++)
//				vertices[i] = Quaternion.Euler(rotation) * vertices[i];
//			mesh.vertices = vertices;
//		}
//	}
//}
//#else
//namespace FightRoom
//{
//	public partial class RotateMesh : EditorScript
//	{
//	}
//}
//#endif
