#if !IS_BUILD
using Godot;
using System.Collections.Generic;

namespace FightRoom
{
	public partial class MoveSubmesh : EditorScript
	{
		public Mesh mesh;
		public int submeshIndex;
		public Vector3 move;

		public override void Do ()
		{
			_Do (mesh, move, submeshIndex);
		}

		public static void _Do (Mesh mesh, Vector3 move, int submeshIndex)
		{
			Vector3[] vertices = mesh.vertices;
			List<int> triangles = new List<int>();
			mesh.GetTriangles(triangles, submeshIndex);
			for (int i = 0; i < vertices.Length; i ++)
			{
				if (triangles.Contains(i))
					vertices[i] += move;
			}
			mesh.vertices = vertices;
		}
	}
}
#else
namespace FightRoom
{
	public partial class MoveSubmesh : EditorScript
	{
	}
}
#endif
