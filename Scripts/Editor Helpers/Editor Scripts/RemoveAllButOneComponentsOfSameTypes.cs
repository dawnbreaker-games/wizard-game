//#if !IS_BUILD
//using Godot;
//
//namespace FightRoom
//{
//	public partial class RemoveAllButOneComponentsOfSameTypes : EditorScript
//	{
//		public Node go;
//
//		public override void Do ()
//		{
//			if (this == null)
//				return;
//			if (go == null)
//				go = gameObject;
//			_Do (go);
//		}
//
//		static void _Do (Node go)
//		{
//			Component[] components = go.GetComponents<Component>();
//			for (int i = 0; i < components.Length; i ++)
//			{
//				Component component = components[i];
//				for (int i2 = i + 1; i2 < components.Length; i2 ++)
//				{
//					Component component2 = components[i2];
//					if (component.GetType() == component2.GetType())
//						GameManager.DestroyOnNextEditorUpdate (component2);
//				}
//			}
//		}
//
//		[MenuItem("Tools/Remove all but one components of same types on selected GameObjects")]
//		static void _Do ()
//		{
//			Node[] selectedGos = Selection.gameObjects;
//			for (int i = 0; i < selectedGos.Length; i ++)
//			{
//				Node selectedGo = selectedGos[i];
//				_Do (selectedGo);
//			}
//		}
//	}
//}
//#else
//namespace FightRoom
//{
//	public partial class RemoveAllButOneComponentsOfSameTypes : EditorScript
//	{
//	}
//}
//#endif
