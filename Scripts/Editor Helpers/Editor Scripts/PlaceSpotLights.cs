#if !IS_BUILD
using Godot;
using System;
using Extensions;
using System.Collections;
using System.Collections.Generic;

namespace FightRoom
{
	public partial class PlaceSpotSpotLight3Ds : EditorScript
	{
		public bool stop;
		public int targetPlaceCount;
		public Node[] placePoints = new Node[0];
		public FloatRange totalNoticabilityRange = new FloatRange(0, float.MaxValue);
		public FloatRange eachSpotLight3DNoticabilityRange = new FloatRange(0, float.MaxValue);
		public FloatRange distanceFromContactPointRange = new FloatRange(0, float.MaxValue);
		public FloatRange rangeMultiplierRange = new FloatRange(0, float.MaxValue);
		public FloatRange intensityRange = new FloatRange(0, float.MaxValue);
		public FloatRange outerSpotAngleRange = new FloatRange(0, 180);
		public FloatRange innerSpotAngleMultiplierRange = new FloatRange(0, 1);
		public bool is2DScene;
		public float raycastsPerDegree;
		public ColorMode colorMode;
		public Gradient colorGraidient;
		public ColorPalette colorPalette;
		public int maxTries = 100;
		public bool useSharedMeshes = true;
		public bool useSharedMaterials = true;
		public float maxMeshRadius = float.MaxValue;
//		Coroutine doRoutine;

		public override void Do ()
		{
			if (stop)
			{
				stop = false;
				StopCoroutine(doRoutine);
			}
			else
				StartCoroutine(DoRoutine());
		}

		static void Stop ()
		{
			PlaceSpotSpotLight3Ds[] placeSpotSpotLight3Ds = SelectionExtensions.GetSelected<PlaceSpotSpotLight3Ds>();
			for (int i = 0; i < placeSpotSpotLight3Ds.Length; i ++)
			{
				PlaceSpotSpotLight3Ds instance = placeSpotSpotLight3Ds[i];
				StopCoroutine(instance.doRoutine);
			}
		}

		IEnumerator DoRoutine ()
		{
			MeshFilter[] meshFilters = FindObjectsOfType<MeshFilter>();
			MeshInstance3D[] meshInstances = new MeshInstance3D[meshFilters.Length];
			for (int i = 0; i < meshFilters.Length; i ++)
			{
				MeshFilter meshFilter = meshFilters[i];
				meshInstances[i] = new MeshInstance3D(meshFilter.GetComponent<Transform>(), meshFilter, meshFilter.GetComponent<MeshRenderer>());
			}
			float totalNoticability = 0;
			for (int i = 0; i < targetPlaceCount; i ++)
			{
				for (int i2 = 0; i2 < maxTries; i2 ++)
				{
					yield return new WaitForEndOfFrame();
					Vector3 position = placePoints[Random.Range(0, placePoints.Length)].position;
					Vector3 forward = Random.onUnitSphere;
					if (is2DScene)
						forward.z = Mathf.Abs(forward.z);
					ContactPoint contactPoint = Raycast(position, forward, meshInstances, is2DScene, useSharedMeshes, useSharedMaterials, maxMeshRadius: maxMeshRadius);
					if (contactPoint == null)
					{
						print("No light was made because an orientation was picked that wasn't facing any mesh. The position was " + position._ToString() + " and the facing was " + forward._ToString() + ".");
						continue;
					}
					float distance = Mathf.Sqrt(contactPoint.distanceSqr);
					float newDistance = distanceFromContactPointRange.Get(Random.value);
					position += forward * distance - (forward * newDistance);
					contactPoint = Raycast(position, forward, meshInstances, is2DScene, useSharedMeshes, useSharedMaterials, maxMeshRadius: maxMeshRadius);
					distance = Mathf.Sqrt(contactPoint.distanceSqr);
					SpotLight3D light = new Node().AddComponent<SpotLight3D>();
					light.type = SpotLight3DType.Spot;
					light.intensity = intensityRange.Get(Random.value);
					light.spotAngle = outerSpotAngleRange.Get(Random.value);
					light.innerSpotAngle = MathfExtensions.Clamp(light.spotAngle * innerSpotAngleMultiplierRange.Get(Random.value), 0, light.spotAngle);
					if (colorMode == ColorMode.Gradient)
						light.color = colorGraidient.Evaluate(Random.value);
					else if (colorMode == ColorMode.Palette)
						light.color = colorPalette.Get(Random.value);
					else // if (colorMode == ColorMode.Random)
						light.color = ColorExtensions.GetRandom();
					light.range = distance * rangeMultiplierRange.Get(Random.value);
					light.position = position;
					light.forward = forward;
					float noticability = GetNoticability(light, raycastsPerDegree, meshInstances, is2DScene);
					if (!eachSpotLight3DNoticabilityRange.Contains(noticability, true, true))
					{
						if (noticability > eachSpotLight3DNoticabilityRange.max)
							print("A light with noticability " + noticability + " was made and then destroyed because it was over the max noticability value " + eachSpotLight3DNoticabilityRange.max + ". The light's position was " + position._ToString() + " and the facing was " + forward._ToString() + ".");
						else //if (noticability < eachSpotLight3DNoticabilityRange.min)
							print("A light with noticability " + noticability + " was made and then destroyed because it was under the min noticability value " + eachSpotLight3DNoticabilityRange.min + ". The light's position was " + position._ToString() + " and the facing was " + forward._ToString() + ".");
						GameManager.DestroyOnNextEditorUpdate (light.gameObject);
						continue;
					}
					float newTotalNoticability = totalNoticability + noticability;
					if (!totalNoticabilityRange.Contains(newTotalNoticability, true, true))
					{
						if (noticability > totalNoticabilityRange.max)
							print("A light with noticability " + noticability + " was made and then destroyed because it was over the max total noticability value " + totalNoticabilityRange.max + ". The light's position was " + position._ToString() + " and the facing was " + forward._ToString() + ".");
						else //if (noticability < totalNoticabilityRange.min)
							print("A light with noticability " + noticability + " was made and then destroyed because it was under the min total noticability value " + totalNoticabilityRange.min + ". The light's position was " + position._ToString() + " and the facing was " + forward._ToString() + ".");
						GameManager.DestroyOnNextEditorUpdate (light.gameObject);
						continue;
					}
					string positionString = position._ToString();
					string forwardString = forward._ToString();
					print("Made a light with noticability " + noticability + " with position " + positionString + " and facing " + forwardString);
					light.name = "Spot SpotLight3D [Noticability=" + noticability + ", Position=" + positionString + ", Facing=" + forwardString + "]";
					totalNoticability = newTotalNoticability;
					break;
				}
			}
		}

		public static ContactPoint Raycast (Vector3 position, Vector3 direction, MeshInstance3D[] meshInstances, bool is2DScene, bool useSharedMeshes = true, bool useSharedMaterials = true, float checkDistance = 99999, float maxMeshRadius = float.MaxValue)
		{
			ContactPoint output = null;
			float minDistanceSqr = float.MaxValue;
			for (int i = 0; i < meshInstances.Length; i ++)
			{
				if (is2DScene)
				{
					Vector3 hitPoint;
					Plane plane = new Plane(Vector3.back, Vector3.Zero);
					Ray ray = new Ray(position, direction);
					plane.Raycast(ray, out hitPoint);
					if ((hitPoint - meshInstance.meshRenderer.bounds.center).LengthSquared() > maxMeshRadius * maxMeshRadius)
						continue;
				}
				Shape3D shape = meshInstance.ToShape3D(useSharedMeshes);
				(Shape3D.Face face, Vector3 point) intersection = shape.GetFaceAndIntersectionPointWithLineSegment(new LineSegment3D(position, position + direction * checkDistance), true, checkDistance);
				if (intersection.face != null)
				{
					float distanceSqr = (position - intersection.point).LengthSquared();
					if (distanceSqr < minDistanceSqr)
					{
						minDistanceSqr = distanceSqr;
						Mesh mesh;
						if (useSharedMeshes)
							mesh = meshInstance.meshFilter.sharedMesh;
						else
							mesh = meshInstance.meshFilter.mesh;
						Material material;
						if (useSharedMaterials)
							material = meshInstance.meshRenderer.sharedMaterial;
						else 
							material = meshInstance.meshRenderer.material;
						Color[] colors = new Color[intersection.face.corners.Length];
						if (material != null)
						{
							Texture2D texture = (Texture2D) material.mainTexture;
							if (texture != null)
							{
								Shape3D.Face hitFace = intersection.face;
								for (int i2 = 0; i2 < intersection.face.corners.Length; i2 ++)
								{
									Vector3 corner = intersection.face.corners[i2];
									colors[i2] = meshInstance.GetColorAtCorner(mesh.vertices.IndexOf<Vector3>(corner), useSharedMeshes);
								}
							}
						}
						output = new ContactPoint(distanceSqr, ColorExtensions.GetAverage(colors));
						if (is2DScene)
							return output;
					}
				}
			}
			return output;
		}

		public static (ContactPoint[], int) GetContactPoints (SpotLight3D light, float raycastsPerDegree, MeshInstance3D[] meshInstances, bool is2DScene, bool useSharedMeshes = true, bool useSharedMaterials = true, float maxMeshRadius = float.MaxValue)
		{
			Vector3 up = Random.onUnitSphere;
			Vector3 forward = light.ToGlobal(Vector3.Forward);
			Vector3.OrthoNormalize(ref forward, ref up);
			List<ContactPoint> contactPoints = new List<ContactPoint>();
			int raycastCount = 0;
			float rotateAmount = 1f / raycastsPerDegree;
			for (float angle = 0; angle <= light.spotAngle; angle += rotateAmount)
			{
				for (float angle2 = 0; angle2 < 360; angle2 += rotateAmount)
				{
					Vector3 direction = Vector3.RotateTowards(forward, up, angle, 0);
					direction = Quaternion.AngleAxis(angle2, forward) * direction;
					if (!is2DScene || direction.z > 0)
					{
						ContactPoint contactPoint = Raycast(light.position, direction, meshInstances, is2DScene, useSharedMeshes, useSharedMaterials, maxMeshRadius: maxMeshRadius);
						if (contactPoint != null)
							contactPoints.Add(contactPoint);
						raycastCount ++;
					}
				}
			}
			return (contactPoints.ToArray(), raycastCount);
		}

		public static float GetNoticability (SpotLight3D light, float raycastsPerDegree, MeshInstance3D[] meshInstances, bool is2DScene, bool useSharedMeshes = true, bool useSharedMaterials = true, float maxMeshRadius = float.MaxValue)
		{
			(ContactPoint[] contactPoints, int raycastCount) contactPointsAndRaycastCount = GetContactPoints(light, light, raycastsPerDegree, meshInstances, is2DScene, useSharedMeshes, useSharedMaterials, maxMeshRadius);
			return GetNoticability(light, light, contactPointsAndRaycastCount.contactPoints, contactPointsAndRaycastCount.raycastCount, is2DScene);
		}

		public static float GetNoticability (SpotLight3D light, ContactPoint[] contactPoints, int raycastCount, bool is2DScene)
		{
			float output = 0;
			for (int i = 0; i < contactPoints.Length; i ++)
			{
				ContactPoint contactPoint = contactPoints[i];
				float distanceSqr = contactPoint.distanceSqr;
				float distance = Mathf.Sqrt(distanceSqr);
				Color lightColor = light.color;
				output += light.intensity * lightColor.GetComponentAverage() * (1f / (distance * distance)) * (1f - contactPoint.color.GetSimilarity(lightColor)) * Mathf.InverseLerp(Vector3.Angle((light.position + light.forward * distance) - light.position, light.forward), light.spotAngle, light.innerSpotAngle);
			}
			output /= raycastCount;
			return output;
		}

		[Serializable]
		public struct ColorPalette
		{
			public Curve redCurve;
			public Curve greenCurve;
			public Curve blueCurve;

			public Color Get (float value)
			{
				return new Color(redCurve.Evaluate(value), greenCurve.Evaluate(value), blueCurve.Evaluate(value));
			}
		}

		public partial class ContactPoint
		{
			public float distanceSqr;
			public Color color;

			public ContactPoint (float distanceSqr, Color color)
			{
				this.distanceSqr = distanceSqr;
				this.color = color;
			}
		}

		public enum ColorMode
		{
			Gradient,
			Palette,
			Random
		}
	}
}
#else
namespace FightRoom
{
	public partial class PlaceSpotSpotLight3Ds : EditorScript
	{
	}
}
#endif
