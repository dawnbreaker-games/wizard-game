//#if !IS_BUILD
//using Godot;
//using Extensions;
//
//namespace FightRoom
//{
//	public partial class SetRectTransformAnchorsToRect : EditorScript
//	{
//		public RectTransform rectTrs;
//
//		public override void Do ()
//		{
//			if (rectTrs == null)
//				rectTrs = GetComponent<RectTransform>();
//			_Do (rectTrs);
//		}
//
//		static void _Do (RectTransform rectTrs)
//		{
//			rectTrs.SetAnchorsToRect ();
//		}
//
//		static void _Do ()
//		{
//			Transform[] selectedTransforms = Selection.transforms;
//			for (int i = 0; i < selectedTransforms.Length; i ++)
//			{
//				Transform selectedTrs = selectedTransforms[i];
//				RectTransform selectedRectTrs = selectedTrs as RectTransform;
//				if (selectedRectTrs != null)
//					_Do (selectedRectTrs);
//			}
//		}
//	}
//}
//#else
//namespace FightRoom
//{
//	public partial class SetRectTransformAnchorsToRect : EditorScript
//	{
//	}
//}
//#endif
