#if !IS_BUILD
using Godot;
using Extensions;
using System.Collections;
using System.Collections.Generic;

namespace FightRoom
{
	public partial class DeselectGameObject : EditorScript
	{
		public Node node;

		public override void Do ()
		{
			_Do (go);
		}

		public static void _Do (Node go)
		{
			Selection.objects = Selection.objects.Remove(go);
		}

		static void _Do ()
		{
			Node[] selectedGos = Selection.gameObjects;
			for (int i = 0; i < selectedGos.Length; i ++)
			{
				Node go = selectedGos[i];
				DeselectGameObject deselectGo = node.GetComponent<DeselectGameObject>();
				if (deselectGo != null)
					Selection.objects = Selection.objects.Remove(go);
			}
		}
	}
}
#else
namespace FightRoom
{
	public partial class DeselectObject : EditorScript
	{
	}
}
#endif
