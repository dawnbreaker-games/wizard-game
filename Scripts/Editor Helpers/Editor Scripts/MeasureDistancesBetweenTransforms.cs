#if !IS_BUILD
using Godot;
using Extensions;

namespace FightRoom
{
	public partial class MeasureDistancesBetweenNodes : EditorScript
	{
		static void _Do ()
		{
			Node[] selectedNodes = SelectionExtensions.GetSelected<Node>();
			for (int i = 0; i < selectedNodes.Length; i ++)
			{
				Node selectedNode = selectedNodes[i];
				for (int i2 = i + 1; i2 < selectedNodes.Length; i2 ++)
				{
					Node selectedNode2 = selectedNodes[i2];
					string output = Vector3.Distance(selectedNode.position, selectedNode2.position) + " is the distance between " + selectedNode.name + " and " + selectedNode2.name;
					output += "\n" + Mathf.Abs(selectedNode.position.X - selectedNode2.position.X) + " is the distance on the x-axis between " + selectedNode.name + " and " + selectedNode2.name;
					output += "\n" + Mathf.Abs(selectedNode.position.Y - selectedNode2.position.Y) + " is the distance on the y-axis between " + selectedNode.name + " and " + selectedNode2.name;
					output += "\n" + Mathf.Abs(selectedNode.position.Z - selectedNode2.position.Z) + " is the distance on the z-axis between " + selectedNode.name + " and " + selectedNode2.name;
					print(output);
				}
			}
		}
	}
}
#else
namespace FightRoom
{
	public partial class MeasureDistanceBetweenNodes : EditorScript
	{
	}
}
#endif
