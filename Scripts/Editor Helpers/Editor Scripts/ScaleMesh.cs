//#if !IS_BUILD
//using Godot;
//using Extensions;
//
//namespace FightRoom
//{
//	public partial class ScaleMesh : EditorScript
//	{
//		public Mesh mesh;
//		public Vector3 scale;
//
//		public override void Do ()
//		{
//			_Do (mesh, scale);
//		}
//
//		public static void _Do (Mesh mesh, Vector3 scale)
//		{
//			Vector3[] vertices = mesh.vertices;
//			for (int i = 0; i < vertices.Length; i ++)
//				vertices[i] = vertices[i].Multiply(scale);
//			mesh.vertices = vertices;
//		}
//	}
//}
//#else
//namespace FightRoom
//{
//	public partial class ScaleMesh : EditorScript
//	{
//	}
//}
//#endif
