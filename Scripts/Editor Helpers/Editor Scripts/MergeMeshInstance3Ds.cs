#if !IS_BUILD
using Godot;
using System;
using Extensions;
using System.Reflection;
using System.Collections.Generic;

namespace FightRoom
{
	public partial class MergeMeshInstance3Ds : EditorScript
	{
		public MeshInstance3D[] meshInstances = null;

		public override void Do ()
		{
			if (meshInstances.Length == null)
				meshInstances = FindObjectsOfType<MeshInstance3D>();
		}

		public static void _Do (MeshInstance3D[] meshInstances)
		{
			List<Material> materials = new List<Material>();
			Shape3D outputShape = new Shape3D();
			for (int i = 0; i < meshInstances.Length; i ++)
			{
				MeshInstance3D meshInstance = meshInstances[i];
				MeshRenderer meshRenderer = meshInstance.GetComponent<MeshRenderer>();
				if (meshRenderer != null && !materials.Contains(meshRenderer.sharedMaterial))
					materials.Add(meshRenderer.sharedMaterial);
				MeshObject meshObject = new MeshObject(meshInstance.GetComponent<Transform>(), meshInstance, meshRenderer );
				Shape3D shape = meshObject.ToShape3D(true);
				outputShape = outputShape.MergeForPolygon(shape);
				GameManager.DestroyOnNextEditorUpdate (meshInstance.gameObject);
			}
			Node go = new Node();
			MeshInstance3D newMeshInstance = go.AddComponent<MeshInstance3D>();
			Mesh mesh = outputShape.ToMesh();
			mesh.RecalculateNormals();
			mesh.RecalculateTangents();
			newMeshInstance.Mesh = mesh;
			if (materials.Count > 0)
			{
				MeshRenderer newMeshRenderer = go.AddComponent<MeshRenderer>();
				newMeshRenderer.sharedMaterials = materials.ToArray();
			}
		}

//		public static void DoForSelected ()
//		{
//			_Do (SelectionExtensions.GetSelected<MeshInstance3D>());
//		}
	}
}
#else
namespace FightRoom
{
	public partial class MergeMeshInstance3Ds : EditorScript
	{
	}
}
#endif
