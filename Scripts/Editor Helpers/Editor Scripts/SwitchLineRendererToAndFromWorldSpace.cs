//using Godot;
//
//namespace FightRoom
//{
//	public partial class SwitchlineToAndFromWorldSpace : EditorScript
//	{
//		public Line2D line;
//		public Transform trs;
//
//		public override void Do ()
//		{
//			Vector3[] positions = new Vector3[line.positionCount];
//			line.GetPositions(positions);
//			for (int i = 0; i < line.positionCount; i ++)
//			{
//				Vector3 position = positions[i];
//				if (line.useWorldSpace)
//					positions[i] = trs.InverseTransformPoint(position);
//				else
//					positions[i] = trs.TransformPoint(position);
//			}
//			line.SetPositions(positions);
//			line.useWorldSpace = !line.useWorldSpace;
//		}
//	}
//}
