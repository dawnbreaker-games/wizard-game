#if !IS_BUILD
using System;
using System.Reflection;
using System.Collections.Generic;

namespace FightRoom
{
	public partial class AutoBuildProject : EditorScript
	{
		public uint buildAfterInactiveTime;
		float timeSinceBuilt;
		
		public override void Do ()
		{
			if (UnityEditorInternal.InternalEditorUtility.isApplicationActive)
				timeSinceBuilt = Time.realtimeSinceStartup;
			else if (Time.realtimeSinceStartup - timeSinceBuilt > buildAfterInactiveTime)
			{
				BuildManager.instance._Build ();
				timeSinceBuilt = Time.realtimeSinceStartup;
			}
		}
	}
}
#else
namespace FightRoom
{
	public partial class AutoBuildProject : EditorScript
	{
	}
}
#endif
