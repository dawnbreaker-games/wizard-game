#if !IS_BUILD
using Godot;
using Extensions;
using System.Collections;
using System.Collections.Generic;

namespace FightRoom
{
	public partial class CopyFontGlyphMetrics : EditorScript
	{
		public Font copyFromFont;
		public Font copyToFont;

		public override void Do ()
		{
			for (int i = 0; i < copyFromFont.glyphTable.Count; i ++)
			{
				Glyph glyph = copyFromFont.glyphTable[i];
				Glyph glyph2;
				if (copyToFont.glyphLookupTable.TryGetValue(glyph.index, out glyph2))
					glyph2.metrics = glyph.metrics;
			}
		}
	}
}
#else
namespace FightRoom
{
	public partial class CopyFontGlyphMetrics : EditorScript
	{
	}
}
#endif
