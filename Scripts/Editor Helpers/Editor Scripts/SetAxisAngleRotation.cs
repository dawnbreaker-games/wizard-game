#if !IS_BUILD
using Godot;
using Extensions;
using System.Collections;
using System.Collections.Generic;

namespace FightRoom
{
	public partial class SetAxisAngleRotation : EditorScript
	{
		public Vector3 axis;
		public float angle;

		public override void Do ()
		{
			trs.rotation = Quaternion.AxisAngle(axis, angle);
		}
	}
}
#else
namespace FightRoom
{
	public partial class SetAxisAngleRotation : EditorScript
	{
	}
}
#endif
