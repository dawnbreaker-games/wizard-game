#if !IS_BUILD
using Extensions;
using System.Collections;
using System.Collections.Generic;

namespace FightRoom
{
	public partial class DestroyAndRemake_Text : EditorScript
	{
		public _Text text;

		public override void Do ()
		{
			_Text newText = ObjectPool.instance.Spawn<_Text>(text.ScenePath, text.Parent);
			text.Free();
			text = newText;
		}
	}
}
#else
namespace FightRoom
{
	public partial class DestroyAndRemake_Text : EditorScript
	{
	}
}
#endif
