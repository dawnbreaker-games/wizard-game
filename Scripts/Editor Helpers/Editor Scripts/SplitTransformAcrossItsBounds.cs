//#if !IS_BUILD
//using Godot;
//using System;
//using Extensions;
//using System.Collections;
//using System.Collections.Generic;
//
//namespace FightRoom
//{
//	public partial class SplitTransformAcrossItsAABB : EditorScript
//	{
//		public Vector3 splitInterval = Vector3.one;
//		public AABBOffset splitOffset;
//
//		public override void Do ()
//		{
//			_Do (node, splitInterval, splitOffset);
//		}
//
//		static void _Do (Node node, Vector3 splitInterval, AABBOffset splitOffset)
//		{
//			AABB aabb = node.GetAABB().ToAABBInt(MathfExtensions.RoundingMethod.HalfOrMoreRoundsUp, MathfExtensions.RoundingMethod.HalfOrLessRoundsDown).ToAABB().Abs();
//			Vector3[] pointsInside = aabb.GetPointsInside(splitInterval, splitOffset);
//			node.position = pointsInside[0] + Vector3.one / 2;
//			node.SetWorldScale (splitInterval);
//			for (int i = 1; i < pointsInside.Length; i ++)
//			{
//				Vector3 pointInside = pointsInside[i] + Vector3.one / 2;
//				Instantiate(node, pointInside, node.rotation, node.parent);
//			}
//		}
//
//		static void _Do ()
//		{
//			Transform[] selectedTransforms = Selection.transforms;
//			for (int i = 0; i < selectedTransforms.Length; i ++)
//			{
//				Transform selectedTrs = selectedTransforms[i];
//				_Do (selectedTrs, Vector3.one, new AABBOffset(Vector3.one / 2, Vector3.Zero));
//			}
//		}
//	}
//}
//#else
//namespace FightRoom
//{
//	public partial class SplitTransformAcrossItsAABB : EditorScript
//	{
//	}
//}
//#endif
