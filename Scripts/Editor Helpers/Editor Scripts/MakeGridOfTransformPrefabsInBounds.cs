//#if !IS_BUILD
//using Godot;
//using System;
//using Extensions;
//using System.Collections;
//using System.Collections.Generic;
//
//namespace FightRoom
//{
//	public partial class MakeGridOfObjectsInAABB : EditorScript
//	{
//		public string objectScenePath;
//		public Vector3 gridCellSize = Vector3.one;
//		public AABBOffset gridCellOffset = new AABBOffset(Vector3.one / 2, Vector3.Zero);
//		public AABB aabb;
//
//		public override void Do ()
//		{
//			_Do (objectScenePath, aabb, gridCellSize, gridCellOffset, trs.parent);
//		}
//
//		public static Node[] _Do (string objectScenePath, AABB aabb, Vector3 gridCellSize, AABBOffset gridCellOffset, Node makeUnderParent = null)
//		{
//			List<Transform> output = new List<Transform>();
//			Vector3[] pointsInside = aabb.GetPointsInside(gridCellSize, gridCellOffset);
//			for (int i = 0; i < pointsInside.Length; i ++)
//			{
//				Vector3 pointInside = pointsInside[i];
//				Transform trs = (Transform) PrefabUtility.InstantiatePrefab(trsPrefab);
//				trs.SetParent(makeUnderParent);
//				trs.position = pointInside;
//				trs.SetWorldScale (gridCellSize);
//				output.Add(trs);
//			}
//			return output.ToArray();
//		}
//
//		static void DoForSelected ()
//		{
//			Transform[] selectedTransforms = Selection.transforms;
//			for (int i = 0; i < selectedTransforms.Length; i ++)
//			{
//				Transform selectedTrs = selectedTransforms[i];
//				MeshFilter meshFilter = selectedTrs.GetComponent<MeshFilter>();
//				if (meshFilter != null)
//				{
//					AABB aabb = meshFilter.sharedMesh.aabb;
//					aabb.center += selectedTrs.position;
//					aabb.size = selectedTrs.rotation * aabb.size.Multiply(selectedTrs.lossyScale);
//					_Do (PrefabUtility.GetCorrespondingObjectFromSource(selectedTrs), aabb, Vector3.one, new AABBOffset(Vector3.one / 2, Vector3.Zero), selectedTrs.parent);
//				}
//			}
//		}
//	}
//}
//#else
//namespace FightRoom
//{
//	public partial class MakeGridOfObjectsInAABB : EditorScript
//	{
//	}
//}
//#endif
