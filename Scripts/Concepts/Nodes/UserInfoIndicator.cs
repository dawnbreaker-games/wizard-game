using Godot;

namespace FightRoom
{
	public partial class UserInfoIndicator : Node
	{
		public _Text usernameText;
		public string username;
		const string REPLACE_STRING = "_";

		public virtual void Init (string username)
		{
			this.username = username;
			usernameText.Text = usernameText.Text.Replace(REPLACE_STRING, username);
		}
	}
}
