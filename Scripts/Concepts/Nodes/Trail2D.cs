using Godot;
using System;
using System.Collections.Generic;

[Serializable]
public partial class Trail2D : Node2D
{
	public Node2D node;
	public Line2D line;
	public float minMovemnetIndicatorPointSeparation;
	public float targetlength;
	List<Vector2> previousPositions = new List<Vector2>();
	float length;
	Vector2 previousPosition;
	List<Point> points = new List<Point>();

	public void Init ()
	{
		previousPosition = GlobalPosition;
	}

	public void MoveTo (Vector2 position)
	{
		GlobalPosition = position;
		if (position != previousPosition)
		{
			Vector2 move = position - previousPosition;
			float distanceToPreviousPosition = move.Length();
			float totalMoveAmount = distanceToPreviousPosition;
			while (totalMoveAmount > 0)
			{
				float moveAmount = Mathf.Min(minMovemnetIndicatorPointSeparation, totalMoveAmount);
				previousPosition += move.Normalized() * moveAmount;
				previousPositions.Add(previousPosition);
				points.Add(new Point(previousPosition, moveAmount));
				length += moveAmount;
				totalMoveAmount -= moveAmount;
			}
			if (points.Count > 0)
			{
				totalMoveAmount = length - targetlength;
				while (totalMoveAmount > 0)
				{
					Point Point = points[0];
					points.RemoveAt(0);
					previousPositions.RemoveAt(0);
					length -= Point.distanceToPreviousPoint;
					if (points.Count == 0)
						break;
					totalMoveAmount -= Point.distanceToPreviousPoint;
				}
				previousPosition = previousPositions[0];
				move = (previousPosition - previousPositions[1]).Normalized();
				totalMoveAmount = targetlength - length;
				while (totalMoveAmount > 0)
				{
					float moveAmount = Mathf.Min(minMovemnetIndicatorPointSeparation, totalMoveAmount);
					previousPosition += move.Normalized() * moveAmount;
					previousPositions.Insert(0, previousPosition);
					points.Insert(0, new Point(previousPosition, moveAmount));
					length += moveAmount;
					totalMoveAmount -= moveAmount;
				}
			}
			previousPosition = GlobalPosition;
			line.Points = previousPositions.ToArray();
		}
	}

	public struct Point
	{
		public Vector2 position;
		public float distanceToPreviousPoint;

		public Point (Vector2 position, float distanceToPreviousPoint)
		{
			GlobalPosition = position;
			this.distanceToPreviousPoint = distanceToPreviousPoint;
		}
	}
}
