//using FightRoom;
//
//public partial class InputRebinder : MonoBehaviour
//{
//	public string actionName;
//	public int bindingIndex;
//	public DeviceDisplayConfigurator deviceDisplaySettings;
//	public _Text actionNameText;
//	public _Text bindingNameText;
//	public Image bindingImage;
//	public Button rebindButton;
//	public Button resetButton;
//	public Node waitingForInputGo;
//	InputAction inputAction;
//	InputActionRebindingExtensions.RebindingOperation rebindOperation;
//
//	void _Ready ()
//	{
//		inputAction = InputManager.instance.inputActionAsset.FindAction(actionName);
//		if (string.IsNullOrEmpty(actionNameText.Text))
//			actionNameText.Text = actionName;
//		UpdateBindingDisplay ();
//	}
//
//	void OnDisable ()
//	{
//		if (rebindOperation != null)
//			OnDone ();
//	}
//
//	public void BeginRebind ()
//	{
//		rebindButton.gameObject.SetActive(false);
//		resetButton.gameObject.SetActive(false);
//		waitingForInputGo.SetActive(true);
//		inputAction.Disable();
//		rebindOperation = inputAction.PerformInteractiveRebinding(bindingIndex)
//			.WithControlsExcluding("<Mouse>/position")
//			.WithControlsExcluding("<Mouse>/delta")
//			.OnMatchWaitForAnother(0.1f)
//			.OnComplete((InputActionRebindingExtensions.RebindingOperation rebingOperation) => { OnDone (); });
//		rebindOperation.Start();
//	}
//
//
//	void OnDone ()
//	{
//		rebindOperation.Dispose();
//		rebindOperation = null;
//		rebindButton.gameObject.SetActive(true);
//		resetButton.gameObject.SetActive(true);
//		waitingForInputGo.SetActive(false);
//		UpdateBindingDisplay ();
//		inputAction.Enable();
//		SaveAndLoadManager.Save (SaveAndLoadManager.filePath);
//	}
//
//	public void ResetBinding ()
//	{
//		InputActionRebindingExtensions.RemoveAllBindingOverrides(inputAction);
//		UpdateBindingDisplay ();
//		SaveAndLoadManager.Save (SaveAndLoadManager.filePath);
//	}
//
//	void UpdateBindingDisplay ()
//	{
//		string currentBindingInput = InputControlPath.ToHumanReadableString(inputAction.bindings[bindingIndex].effectivePath, InputControlPath.HumanReadableStringOptions.OmitDevice);
//		Sprite sprite = deviceDisplaySettings.GetDeviceBindingIcon(currentBindingInput);
//		if (sprite != null)
//		{
//			bindingNameText.gameObject.SetActive(false);
//			bindingImage.gameObject.SetActive(true);
//			bindingImage.sprite = sprite;
//		}
//		else
//		{
//			bindingImage.gameObject.SetActive(false);
//			bindingNameText.Text = currentBindingInput;
//		}
//	}
//}
