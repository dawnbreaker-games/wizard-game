using Godot;

namespace FightRoom
{
	public partial class ObjectInWorld : Node2D
	{
		public ObjectInWorld[] objectsToLoadAndUnloadWithMe = new ObjectInWorld[0];
		public bool dontUnloadAfterLoad;

#if !IS_BUILD
		// 
		public ObjectInWorld duplicate;
		// 
		public WorldPiece pieceIAmIn;
#endif

		public override void _Ready ()
		{
			if (dontUnloadAfterLoad)
				SetParent(null);
		}
	}
}
