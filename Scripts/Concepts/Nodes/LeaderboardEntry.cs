using Godot;

namespace FightRoom
{
	public partial class LeaderboardEntry : Node2D
	{
		public _Text rankText;
		public _Text valueText;
		public _Text usernameText;
		public Node2D[] valueTypeIndicators = new Node2D[0];

		public enum ValueType
		{
			Time,
			TotalTime,
			Tasks
		}
	}
}
