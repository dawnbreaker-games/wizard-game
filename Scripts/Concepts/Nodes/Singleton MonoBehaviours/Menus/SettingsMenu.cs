//using Godot;
//using Extensions;
//
//namespace FightRoom
//{
//	public partial class SettingsMenu : Menu
//	{
//		public Toggle lightToggle;
//		public new static SettingsMenu instance;
//		public static bool AllowLights
//		{
//			get
//			{
//				return PlayerPrefsExtensions.GetBool("Allow lights", true);
//			}
//			set
//			{
//				SetLightsActive (value);
//				PlayerPrefsExtensions.SetBool ("Allow lights", value);
//			}
//		}
//
//		public override void _Ready ()
//		{
//			base.Awake ();
//			lightToggle.isOn = AllowLights;
//		}
//
//		public static void SetLightsActive (bool allow)
//		{
//			Light[] lights = FindObjectsOfType<Light>(true);
//			for (int i = 0; i < lights.Length; i ++)
//			{
//				Light light = lights[i];
//				light.gameObject.SetActive(allow);
//			}
//		}
//	}
//}
