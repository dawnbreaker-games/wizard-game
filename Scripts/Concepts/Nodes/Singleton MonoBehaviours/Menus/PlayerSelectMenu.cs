using Godot;
using System;
using Extensions;
using System.Collections;
using System.Collections.Generic;

namespace FightRoom
{
	public partial class PlayerSelectMenu : Menu, IUpdatable
	{
		public _Text currentPlayerNameText;
		public Node playersParent;
		public Node3D currentPlayerIndicatorTrs;
		public _Text itemsDescriptionText;
		public new static PlayerSelectMenu instance;
		public static Player[] players = new Player[0];
		public static uint currentPlayerIndex;
		static int previousSwitchPlayerInput;
		public static bool justClosed;
		bool previousSelectInput = true;

		public void Init ()
		{
			instance = this;
			players = playersParent.GetChildNodes<Player>();
			SetCurrentPlayer (players[currentPlayerIndex]);
		}

		public override void Open ()
		{
			base.Open ();
			MapMenu.instance.levelNameText.gameObject.SetActive(false);
			SetCurrentPlayer (players[currentPlayerIndex]);
			GameManager.updatables = GameManager.updatables.Add(this);
			justClosed = false;
		}

		public override void Close ()
		{
			MapMenu.instance.levelNameText.gameObject.SetActive(true);
			gameObject.SetActive(false);
			GameManager.updatables = GameManager.updatables.Remove(this);
			currentPlayerNameText.Text = "";
			justClosed = true;
		}

		public void DoUpdate ()
		{
			bool selectInput = InputManager.SelectInput;
			Vector2 movementInput = InputManager.MoveInput;
			if (movementInput.x != 0 && previousSwitchPlayerInput == 0)
			{
				if (movementInput.x < 0)
				{
					if (currentPlayerIndex > 0)
						currentPlayerIndex --;
					else
						currentPlayerIndex = (uint) players.Length - 1;
				}
				else
				{
					if (currentPlayerIndex < players.Length - 1)
						currentPlayerIndex ++;
					else
						currentPlayerIndex = 0;
				}
				SetCurrentPlayer (players[currentPlayerIndex]);
			}
			if ((Player.instance.unlocked || BuildManager.instance.unlockEverything) && !EventSystem.current.IsPointerOverGameObject() && selectInput && !previousSelectInput)
				Close ();
			previousSwitchPlayerInput = MathfExtensions.Sign(movementInput.x);
			previousSelectInput = selectInput;
		}

		public void SetCurrentPlayer (Player player)
		{
			Player.instance = player;
			currentPlayerIndicatorTrs.position = player.trs.position;
			string text = player.displayName;
			if (player.unlockOnCompleteAchievement != null)
			{
				text += "\n";
				if (player.unlockOnCompleteAchievement.complete)
					text += "(Done) ";
				text += player.unlockOnCompleteAchievement.displayName;
			}
			currentPlayerNameText.Text = text;
			text = "Move speed: " + player.moveSpeed + "\n";
			if (player.weapons.Length == 1)
				text += "\nWeapon:\n";
			else if (player.weapons.Length > 1)
				text += "\nWeapons:\n";
			for (int i = 0; i < player.weapons.Length; i ++)
			{
				Weapon weapon = player.weapons[i];
				text += weapon.description;
				Bullet bulletPrefab = weapon.bulletPatternEntry.bulletPrefab;
				if (bulletPrefab != null)
				{
					text += " Projectile move speed: " + bulletPrefab.moveSpeed + ". Projectile damage: " + bulletPrefab.damage + ".";
					if (bulletPrefab.autoDespawnMode == Bullet.AutoDespawnMode.RangedAutoDespawn)
						text +=  " Projectile range: " + bulletPrefab.range + ".";
					else if (bulletPrefab.autoDespawnMode == Bullet.AutoDespawnMode.DelayedAutoDespawn)
						text += " Projectile range: " + bulletPrefab.duration * bulletPrefab.moveSpeed + ".";
				}
				text += " Cooldown: " + weapon.animationEntry.duration + "s.\n";
			}
			if (player.useableItems.Length == 1)
				text += "\nAbility:\n";
			else if (player.useableItems.Length > 1)
				text += "\nAbilities:\n";
			for (int i = 0; i < player.useableItems.Length; i ++)
			{
				UseableItem useableItem = player.useableItems[i];
				text += useableItem.description;
				if (useableItem.maxUseTime > 0)
					text += " Max use time: " + useableItem.maxUseTime + "s. ";
				text += " Cooldown: " + useableItem.cooldown + "s.\n";
			}
			itemsDescriptionText.Text = text;
		}
	}
}
