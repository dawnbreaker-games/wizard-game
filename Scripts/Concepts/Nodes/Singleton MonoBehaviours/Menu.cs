
using System.Collections;
using System.Collections.Generic;

namespace FightRoom
{
	public partial class Menu : SingletonNode2D<Menu>
	{
		// public CustomString welcomeTextString;
		// string previousNotificationTextString;

		// public override void _Ready ()
		// {
		// 	base.Awake ();
		// 	gameObject.SetActive(false);
		// }

		public virtual void Open ()
		{
			GameManager.paused = true;
			_Animator.instances = FindObjectsOfType<_Animator>();
			for (int i = 0; i < _Animator.instances.Length; i ++)
			{
				_Animator animator = _Animator.instances[i];
				animator.animator.enabled = false;
			}
			gameObject.SetActive(true);
			// previousNotificationTextString = GameManager.instance.notificationText.Text;
			// GameManager.instance.notificationText.Text = welcomeTextString.Value;
		}

		public virtual void Close ()
		{
			GameManager.paused = false;
			_Animator.instances = FindObjectsOfType<_Animator>();
			for (int i = 0; i < _Animator.instances.Length; i ++)
			{
				_Animator animator = _Animator.instances[i];
				animator.animator.enabled = true;
			}
			if (this != null)
				gameObject.SetActive(false);
			// GameManager.instance.notificationText.Text = previousNotificationTextString;
		}
	}
}
