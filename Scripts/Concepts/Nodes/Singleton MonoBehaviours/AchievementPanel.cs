//using Godot;
//using Extensions;
//
//namespace FightRoom
//{
//	public partial class AchievementPanel : SingletonUpdateNode2D<AchievementPanel>
//	{
//		public Node achievementIndicatorsParent;
//		public AchievementIndicator achievementIndicatorPrefab;
//
//		public override void DoUpdate ()
//		{
//			AchievementIndicator[] achievementIndicators = FindObjectsOfType<AchievementIndicator>();
//			for (int i = 0; i < achievementIndicators.Length; i ++)
//			{
//				AchievementIndicator achievementIndicator = achievementIndicators[i];
//				Destroy(achievementIndicator.gameObject);
//			}
//			Achievement[] achievements = MapMenu.targetLevel.GetChildNodes<Achievement>();
//			for (int i = 0; i < achievements.Length; i ++)
//			{
//				Achievement achievement = achievements[i];
//				AchievementIndicator achievementIndicator = Instantiate(achievementIndicatorPrefab, achievementIndicatorsParent);
//				achievementIndicator.rectTrs.localScale = Vector3.one;
//				string text;
//				if (achievement.unlockLevelOnComplete.trs.position.x > MapMenu.targetLevel.trs.position.x)
//					text = ">";
//				else if (achievement.unlockLevelOnComplete.trs.position.x < MapMenu.targetLevel.trs.position.x)
//					text = "<";
//				else if (achievement.unlockLevelOnComplete.trs.position.y > MapMenu.targetLevel.trs.position.y)
//					text = "^";
//				else
//					text = "v";
//				if (achievement.complete)
//					text += " (Done)";
//				text += " " + achievement.displayName;
//				if (!achievement.complete)
//					text += " (" + achievement.GetProgress() + "/" + achievement.GetMaxProgress() + ")";
//				achievementIndicator.text.Text = text;
//				Vector2 spawnPosition = new Vector2();
//				Vector3[] positions = new Vector3[2];
//				achievement.incompleteIndicatorLineRenderer.GetPositions(positions);
//				spawnPosition = (positions[0] + positions[1]) / 2;
//				Vector2 offset = achievement.unlockLevelOnComplete.trs.position - achievement.levelIAmFoundOn.trs.position;
//				if (offset.x != 0)
//					spawnPosition.x += achievementIndicatorPrefab.rectTrs.sizeDelta.x / 2 * Mathf.Sign(offset.x);
//				achievementIndicator = Instantiate(achievementIndicatorPrefab, spawnPosition, Quaternion.identity);
//				text = achievement.displayName;
//				if (!achievement.complete)
//					text += " (" + achievement.GetProgress() + "/" + achievement.GetMaxProgress() + ")";
//				else
//					text += " (Done)";
//				achievementIndicator.text.gameObject.layer = int.NameToLayer("Map");
//				achievementIndicator.text.Text = text;
//			}
//		}
//	}
//}
