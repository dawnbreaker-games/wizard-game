using Godot;
using System;
using Extensions;
using System.Collections.Generic;

namespace FightRoom
{
	public partial class ObjectPool : SingletonUpdateNode<ObjectPool>
	{
		public bool preloadOnAwake = true;
		public SpawnEntry[] spawnEntries = new SpawnEntry[0];
		public DelayedDespawn[] delayedDespawns = new DelayedDespawn[0];
		public RangedDespawn[] rangedDespawns = new RangedDespawn[0];
		public List<SpawnedEntry> spawnedEntries = new List<SpawnedEntry>();
		
		public override void _Ready ()
		{
			update = false;
			base._Ready ();
			if (!preloadOnAwake)
				return;
			for (int i = 0 ; i < spawnEntries.Length; i ++)
			{
				SpawnEntry spawnEntry = spawnEntries[i];
				for (int i2 = 0; i2 < spawnEntry.preload; i2 ++)
					Preload (spawnEntry);
			}
		}
		
		public override void DoUpdate ()
		{
			for (int i = 0; i < delayedDespawns.Length; i ++)
			{
				DelayedDespawn delayedDespawn = delayedDespawns[i];
				delayedDespawn.timeRemaining -= Time.deltaTime;
				if (delayedDespawn.timeRemaining <= 0)
				{
					Despawn (delayedDespawn.prefabIndex, delayedDespawn.node, delayedDespawn.trs);
					if (i >= delayedDespawns.Length)
						continue;
					delayedDespawns = delayedDespawns.RemoveAt(i);
					i --;
					if (delayedDespawns.Length == 0 && rangedDespawns.Length == 0)
						enabled = false;
				}
				else
					delayedDespawns[i] = delayedDespawn;
			}
			for (int i = 0; i < rangedDespawns.Length; i ++)
			{
				RangedDespawn rangedDespawn = rangedDespawns[i];
				rangedDespawn.rangeRemaining -= (rangedDespawn.trs.position - rangedDespawn.previousPosition).Length();
				rangedDespawn.previousPosition = rangedDespawn.trs.position;
				if (rangedDespawn.rangeRemaining <= 0)
				{
					Despawn (rangedDespawn.prefabIndex, rangedDespawn.node, rangedDespawn.trs);
					if (i >= rangedDespawns.Length)
						continue;
					rangedDespawns = rangedDespawns.RemoveAt(i);
					i --;
					if (rangedDespawns.Length == 0 && delayedDespawns.Length == 0)
						enabled = false;
				}
				else
					rangedDespawns[i] = rangedDespawn;
			}
		}
		
		public virtual void RemoveSpawnedEntry (SpawnedEntry spawnedEntry)
		{
			spawnedEntries.Remove(spawnedEntry);
		}
		
		public virtual void RemoveSpawnedEntry (Node clone)
		{
			SpawnedEntry spawnedEntry = new SpawnedEntry(clone);
			spawnedEntries.Remove(spawnedEntry);
		}
		
		public virtual DelayedDespawn DelayDespawn (string scenePath, Node clone, float delay)
		{
			DelayedDespawn delayedDespawn = new DelayedDespawn(delay);
			delayedDespawn.node = clone;
			delayedDespawn.trs = trs;
			// delayedDespawn.prefab = spawnEntries[prefabIndex].prefab;
			delayedDespawn.scenePath = scenePath;
			delayedDespawns = delayedDespawns.Add(delayedDespawn);
			GameManager.updatables = GameManager.updatables.Add(this);
			return delayedDespawn;
		}

		public virtual void CancelDelayedDespawn (DelayedDespawn delayedDespawn)
		{
			int indexOfDelayedDespawn = delayedDespawns.IndexOf(delayedDespawn);
			if (indexOfDelayedDespawn != -1)
			{
				delayedDespawns = delayedDespawns.RemoveAt(indexOfDelayedDespawn);
				if (delayedDespawns.Length == 0 && rangedDespawns.Length == 0)
					GameManager.updatables = GameManager.updatables.Remove(this);
			}
		}
		
		public virtual RangedDespawn RangeDespawn (string scenePath, Node clone, float range)
		{
			RangedDespawn rangedDespawn = new RangedDespawn(clone.Position, range);
			rangedDespawn.node = clone;
			rangedDespawn.trs = trs;
			// rangedDespawn.prefab = spawnEntries[prefabIndex].prefab;
			rangedDespawn.prefabIndex = prefabIndex;
			rangedDespawn.previousPosition = clone.Position;
			rangedDespawns = rangedDespawns.Add(rangedDespawn);
			GameManager.updatables = GameManager.updatables.Add(this);
			return rangedDespawn;
		}

		public virtual void CancelRangedDespawn (RangedDespawn rangedDespawn)
		{
			int indexOfRangedDespawn = rangedDespawns.IndexOf(rangedDespawn);
			if (indexOfRangedDespawn != -1)
			{
				rangedDespawns = rangedDespawns.RemoveAt(indexOfRangedDespawn);
				if (rangedDespawns.Length == 0 && delayedDespawns.Length == 0 && this != null)
					GameManager.updatables = GameManager.updatables.Remove(this);
			}
		}
		
		public virtual T Spawn<T> (ISpawnable spawnable, Vector3 position = new Vector3(), Quaternion rotation = new Quaternion(), Node parent = null) where T : Node, ISpawnable
		{
			return Spawn(spawnable.ScenePath, position, rotation, parent);
		}
		
		public virtual T Spawn<T> (string scenePath, Vector3 position = new Vector3(), Quaternion rotation = new Quaternion(), Node parent = null) where T : Node
		{
			return (T) Load(scenePath).Instantiate();
		}

		public virtual void Despawn (SpawnedEntry spawnedEntry)
		{
			Despawn (spawnedEntry.node);
		}
		
		public virtual void Despawn (Node node)
		{
			if (node != null)
				node.Free();
		}
		
		public virtual Node Preload (SpawnEntry spawnEntry)
		{
			Node clone = Load(spawnEntry.scenePath).Instantiate();
			spawnEntry.cache.Add(clone);
			spawnEntries[prefabIndex] = spawnEntry;
			return output;
		}

		[Serializable]
		public partial class ObjectPoolEntry
		{
			public Node prefab;
			public string scenePath;
			
			public ObjectPoolEntry ()
			{
			}
			
			public ObjectPoolEntry (Node prefab, string scenePath)
			{
				this.prefab = prefab;
				this.scenePath = scenePath;
			}
		}
		
		[Serializable]
		public partial class SpawnEntry : ObjectPoolEntry
		{
			public int preload;
			public List<Node> cache = new List<Node>();
			
			public SpawnEntry ()
			{
			}
			
			public SpawnEntry (int preload, List<Node> cache)
			{
				this.preload = preload;
				this.cache = cache;
			}
		}
		
		public partial class SpawnedEntry : ObjectPoolEntry
		{
			public Node node;
			
			public SpawnedEntry ()
			{
			}
			
			public SpawnedEntry (Node node)
			{
				this.node = node;
			}
		}
		
		public partial class DelayedDespawn : SpawnedEntry
		{
			public float timeRemaining;
			
			public DelayedDespawn ()
			{
			}
			
			public DelayedDespawn (float timeRemaining)
			{
				this.timeRemaining = timeRemaining;
			}
		}
		
		public partial class RangedDespawn : SpawnedEntry
		{
			public Vector3 previousPosition;
			public float rangeRemaining;
			
			public RangedDespawn ()
			{
			}
			
			public RangedDespawn (Vector3 previousPosition, float rangeRemaining)
			{
				this.previousPosition = previousPosition;
				this.rangeRemaining = rangeRemaining;
			}
		}
	}
}
