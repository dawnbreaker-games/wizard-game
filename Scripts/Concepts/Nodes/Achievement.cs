//using Godot;
//using System;
//using Extensions;
//using System.Collections.Generic;
//
//namespace FightRoom
//{
//	public partial class Achievement : UpdateNode3D
//	{
//		public Level levelIAmFoundOn;
//		public string displayName;
//		public Node incompleteIndicatorNode;
//		public Level unlockLevelOnComplete;
//		public Level[] levels = new Level[0];
//		public bool complete;
//		public Line2D incompleteIndicatorLine;
//#if !IS_BUILD
//		public Line2D completeIndicatorLine;
//		public float lineRendererSepeartion;
//#endif
//		public static Achievement[] instances = new Achievement[0];
//		public static uint completeCount;
//
//#if !IS_BUILD
//		void OnValidate ()
//		{
//			Transform trs = GetComponent<Transform>();
//			if (completeIndicatorLine == null)
//				completeIndicatorLine = trs.Find("Complete Indicator").GetComponent<Line2D>();
//			if (incompleteIndicatorLine == null)
//				incompleteIndicatorLine = trs.Find("Incomplete Indicator").GetComponent<Line2D>();
//			if (incompleteIndicatorNode == null)
//				incompleteIndicatorNode = completeIndicatorLine.gameObject;
//			levelIAmFoundOn = GetParentNode<Level>();
//			if (levelIAmFoundOn != null)
//			{
//				Vector3 offset = ((Vector2) (unlockLevelOnComplete.trs.position - levelIAmFoundOn.trs.position)).Normalized().Rotate90() * lineRendererSepeartion / 2;
//				completeIndicatorLine.SetPositions(new Vector3[] { levelIAmFoundOn.trs.position + offset, unlockLevelOnComplete.trs.position + offset });
//				incompleteIndicatorLine.SetPositions(new Vector3[] { levelIAmFoundOn.trs.position + offset, unlockLevelOnComplete.trs.position + offset });
//			}
//		}
//#endif
//
//		public void Init ()
//		{
//			if (SaveAndLoadManager.saveData.completeAchievementsNames.Contains(name))
//				Complete ();
//		}
//
//		public bool ShouldBeComplete ()
//		{
//			return (levelIAmFoundOn == null || levelIAmFoundOn.unlocked) && GetProgress() >= GetMaxProgress();
//		}
//
//		public virtual void Complete ()
//		{
//			if (complete)
//				return;
//			incompleteIndicatorNode.SetActive(false);
//			if (!SaveAndLoadManager.saveData.completeAchievementsNames.Contains(name))
//				SaveAndLoadManager.saveData.completeAchievementsNames = SaveAndLoadManager.saveData.completeAchievementsNames.Add(name);
//			unlockLevelOnComplete.unlocked = true;
//			unlockLevelOnComplete.lockedIndicatorNode.SetActive(false);
//			complete = true;
//			completeCount ++;
//			print(name + " complete");
//		}
//
//		public virtual uint GetProgress ()
//		{
//			throw new NotImplementedException();
//		}
//
//		public virtual uint GetMaxProgress ()
//		{
//			throw new NotImplementedException();
//		}
//	}
//}
