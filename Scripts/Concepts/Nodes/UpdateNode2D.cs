using Godot;
using Extensions;

namespace FightRoom
{
	public partial class UpdateNode2D : Node2D, IUpdatable
	{
		[Export]
		bool update;
		[Export]
		bool updatePhysics;
		
		public override void _Ready ()
		{
			if (update)
			{
				GameManager.updatables = GameManager.updatables.Add(this);
				TreeEiting += OnAboutToDestroy;
			}
		}

		public virtual void DoUpdate ()
		{
		}

		public virtual void OnAboutToDestroy ()
		{
			GameManager.updatables = GameManager.updatables.Remove(this);
		}
	}
}
