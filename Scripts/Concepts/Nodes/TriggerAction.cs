using Godot;
using System;
using System.Collections;

namespace FightRoom
{
	public partial class TriggerAction : Node
	{
		public Action action;
		public bool onEnable;
		public bool onAwake;
		public bool onReady;
		public bool onDisable;
		public bool onDestroy;
//		public bool onLevelLoaded;
//		public bool onLevelUnloaded;
		public bool onBodyEntered;
		public bool onBodyExited;
		public int initTriggersUntilAct = 1;
		int triggersUntilAct;
		
		public override void _Ready ()
		{
			if (onReady)
				Trigger ();
//			if (onLevelLoaded)
//				SceneManager.sceneLoaded += LevelLoaded;
//			if (onLevelUnloaded)
//				SceneManager.sceneUnloaded += LevelUnloaded;
		}
		
		void OnDisable ()
		{
			if (onDisable)
				Trigger ();
		}
		
		void OnDestroy ()
		{
			if (onDestroy)
				Trigger ();
		}
		
		void OnBodyEntered (Node other)
		{
			if (onBodyEntered)
				Trigger ();
		}
		
		void OnBodyExited (Node other)
		{
			if (onBodyExited)
				Trigger ();
		}
		
//		void LevelLoaded (Scene scene, LoadSceneMode loadMode)
//		{
//			if (this != null)
//				Trigger ();
//		}
		
//		void LevelUnloaded (Scene scene)
//		{
//			if (this != null)
//				Trigger ();
//		}
		
		public void Trigger ()
		{
			triggersUntilAct --;
			if (triggersUntilAct == 0 && action != null)
				action.Invoke ();
		}
		
		public void Restart (int triggersUntilAct)
		{
			this.triggersUntilAct = triggersUntilAct;
		}
		
		public void Restart ()
		{
			Restart (initTriggersUntilAct);
		}
	}
}
