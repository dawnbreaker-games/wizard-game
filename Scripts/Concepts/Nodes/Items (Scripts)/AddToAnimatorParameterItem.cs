using Extensions;


namespace FightRoom
{
	public partial class AddToAnimatorParameterItem : Item
	{
		public string parameterName;
		public float amount;

		public override void OnGain ()
		{
			Animator animator = Player.instance.animator;
			animator.SetFloat(parameterName, animator.GetFloat(parameterName) + amount);
		}
	}
}