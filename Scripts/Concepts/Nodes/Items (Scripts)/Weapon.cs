namespace FightRoom
{
	public partial class Weapon : Item
	{
		public Player owner;
		public BulletPatternEntry bulletPatternEntry;
		public AnimationEntry animationEntry;

		public override void OnGain ()
		{
			bulletPatternEntry.spawner = owner.bulletSpawnersParent;
			animationEntry.animator = owner.animator;
			owner.bulletPatternEntriesSortedList[bulletPatternEntry.name] = bulletPatternEntry;
			animationEntry.Play ();
		}

		void OnDestroy ()
		{
			if (owner == null)
				return;
			owner.bulletPatternEntriesSortedList.Remove(bulletPatternEntry.name);
			animationEntry.animator.Play("None", animationEntry.layer);
		}
	}
}
