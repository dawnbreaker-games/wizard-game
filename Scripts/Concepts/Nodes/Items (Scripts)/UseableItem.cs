using Godot;
using Extensions;

namespace FightRoom
{
	public partial class UseableItem : Item
	{
		public string useInputActionName;
		public float cooldown;
		public float cooldownRemaining;
		public float maxUseTime;
		float lastUseTime;
		UseUpdater useUpdater;

		public override void OnGain ()
		{
		}

		public void TryToUse ()
		{
			if (!GameManager.paused && Time.time - lastUseTime >= cooldown)
				Use ();
		}

		public virtual void Use ()
		{
			lastUseTime = Time.time;
			if (maxUseTime > 0 && useUpdater == null)
				OnBeganUsing ();
		}

		public virtual void OnBeganUsing ()
		{
			useUpdater = new UseUpdater(this);
			GameManager.updatables = GameManager.updatables.Add(useUpdater);	
		}

		public virtual void OnStopUsing ()
		{
			float timeOvershoot = Time.time - lastUseTime - maxUseTime;
			if (timeOvershoot > 0)
				lastUseTime = Time.time - timeOvershoot;
			GameManager.updatables = GameManager.updatables.Remove(useUpdater);
			useUpdater = null;
		}

		class UseUpdater : IUpdatable
		{
			UseableItem useableItem;
			float usedTime;

			public UseUpdater (UseableItem useableItem)
			{
				this.useableItem = useableItem;
			}

			public void DoUpdate ()
			{
				if (Input.IsActionPressed(useableItem.useInputActionName))
				{
					useableItem.Use ();
					usedTime += Time.deltaTime;
					if (usedTime >= useableItem.maxUseTime)
						useableItem.OnStopUsing ();
				}
				else
					useableItem.OnStopUsing ();
			}
		}
	}
}
