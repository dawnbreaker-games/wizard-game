namespace FightRoom
{
	public partial class UseableWeapon : UseableItem
	{
		public BulletPatternEntry bulletPatternEntry;
		public AnimationEntry animationEntry;
		public bool autoShoot;

		public override void OnGain ()
		{
			base.OnGain ();
			if (!autoShoot)
				Player.instance.bulletPatternEntriesSortedList.Add(bulletPatternEntry.name, bulletPatternEntry);
		}

		public override void Use ()
		{
			base.Use ();
			if (autoShoot)
				bulletPatternEntry.Shoot ();
			animationEntry.Play ();
		}
	}
}
