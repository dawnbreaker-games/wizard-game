using Godot;
using Extensions;
using System.Collections.Generic;

namespace FightRoom
{
	public partial class Hourglass : UseableItem, IUpdatable
	{
		public float rewindTime;
		public Node rewindPoitionIndicatorNode;
		public static List<Snapshot> snapshots = new List<Snapshot>();

		public override void _Ready ()
		{
			base._Ready ();
			TreeExiting += OnAboutToDestroy;
			GameManager.updatables = GameManager.updatables.Add(this);
		}

		public void DoUpdate ()
		{
			snapshots.Add(new Snapshot());
			while (GameManager.TimeSinceLevelLoad - snapshots[0].time > rewindTime)
				snapshots.RemoveAt(0);
			rewindPoitionIndicatorNode.position = snapshots[0].position;
		}

		void OnAboutToDestroy ()
		{
			GameManager.updatables = GameManager.updatables.Remove(this);
		}

		public override void Use ()
		{
			base.Use ();
			Player.instance.trs.position = snapshots[0].position;
		}

		public partial class Snapshot
		{
			public Vector2 position;
			public float time;

			public Snapshot ()
			{
				position = Player.instance.trs.position;
				time = GameManager.TimeSinceLevelLoad;
			}
		}
	}
}
