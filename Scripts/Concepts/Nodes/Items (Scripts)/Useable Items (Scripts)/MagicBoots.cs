using Extensions;

namespace FightRoom
{
	public partial class MagicBoots : UseableItem, IUpdatable
	{
		public float addToMoveSpeed;
		public Trail2D trail;
		public float dissolveRate;
		
		public override void _Ready ()
		{
			base._Ready ();
			TreeExiting += OnAboutToDestroy;
		}

		public override void OnBeganUsing ()
		{
			trail.widthCurve = Curve.Linear(0, Player.instance.radius * 2, 1, 0);
			trail.gameObject.SetActive(true);
			Player.instance.moveSpeed += addToMoveSpeed;
//			Physics2D.IgnoreLayerCollision(Player.instance.gameObject.layer, int.NameToLayer("Enemy Bullet"), true);
//			Physics2D.IgnoreLayerCollision(Player.instance.gameObject.layer, int.NameToLayer("Hazard"), true);
			base.OnBeganUsing ();
		}

		public void DoUpdate ()
		{
			float startWidth = trail.widthCurve.keys[0].value - dissolveRate * Time.deltaTime;
			trail.widthCurve = Curve.Linear(0, startWidth, 1, 0);
			if (startWidth <= 0)
			{
				trail.gameObject.SetActive(false);
				trail.Clear();
				GameManager.updatables = GameManager.updatables.Remove(this);
			}
		}

		public override void OnStopUsing ()
		{
			Player.instance.moveSpeed -= addToMoveSpeed;
//			Physics2D.IgnoreLayerCollision(Player.instance.gameObject.layer, int.NameToLayer("Enemy Bullet"), false);
//			Physics2D.IgnoreLayerCollision(Player.instance.gameObject.layer, int.NameToLayer("Hazard"), false);
			GameManager.updatables = GameManager.updatables.Add(this);
			base.OnStopUsing ();
		}

		void OnAboutToDestroy ()
		{
			trail.SetActive(false);
			trail.Clear();
			GameManager.updatables = GameManager.updatables.Remove(this);
		}
	}
}
