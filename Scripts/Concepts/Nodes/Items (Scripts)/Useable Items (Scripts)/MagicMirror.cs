using Godot;
using Extensions;

namespace FightRoom
{
	public partial class MagicMirror : UseableItem, IUpdatable
	{
		public Node2D mirrorPositionIndicatorNode;

		public override void _Ready ()
		{
			TreeExiting += OnAboutToDestroy;
			mirrorPositionIndicatorNode.SetParent(null);
			GameManager.updatables = GameManager.updatables.Add(this);
		}

		void OnAboutToDestroy ()
		{
			if (mirrorPositionIndicatorNode != null)
				mirrorPositionIndicatorNode.gameObject.SetActive(false);
			GameManager.updatables = GameManager.updatables.Remove(this);
		}

		public void DoUpdate ()
		{
			mirrorPositionIndicatorNode.position = Level.instance.trs.position + (Level.instance.trs.position - Player.instance.trs.position);
			mirrorPositionIndicatorNode.up = -Player.instance.trs.up;
		}

		public override void Use ()
		{
			base.Use ();
			Player.instance.trs.position = Level.instance.trs.position + (Level.instance.trs.position - Player.instance.trs.position);
		}
	}
}
