using Extensions;

namespace FightRoom
{
	public partial class Heart : Item
	{
		public override void OnGain ()
		{
			Player.instance.TakeDamage (-1);
			Player.instance.items = Player.instance.items.Remove(this);
			Destroy(gameObject);
		}
	}
}