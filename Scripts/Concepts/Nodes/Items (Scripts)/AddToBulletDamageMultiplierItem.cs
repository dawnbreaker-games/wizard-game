using System;
using Extensions;

namespace FightRoom
{
	public partial class AddToBulletDamageMultiplierItem : Item
	{
		public float amount;
		static float multiplier;

		public override void OnGain ()
		{
			float previousMultiplier = multiplier;
			multiplier += amount;
			for (int i = 0; i < Player.instance.bulletPatternEntriesSortedList.Values.Count; i ++)
			{
				BulletPatternEntry bulletPatternEntry = Player.instance.bulletPatternEntriesSortedList.Values[i];
				GameManager.DoActionToObjectAndSpawnedInstances<Bullet> ((Bullet bullet) => { bullet.damage /= previousMultiplier; bullet.damage *= multiplier; }, bulletPatternEntry.bulletPrefab);
			}
		}
	}
}