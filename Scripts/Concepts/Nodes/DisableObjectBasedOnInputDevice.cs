using Godot;
using System.Collections;
using System.Collections.Generic;

namespace FightRoom
{
	public partial class DisableObjectBasedOnInputDevice : Node
	{
		public bool disableIfUsing;
		public InputManager.InputDevice inputDevice;
		
		void Start ()
		{
			SetActive((InputManager.instance.inputDevice == inputDevice || (InputManager.UsingGamepad && inputDevice == InputManager.InputDevice.Gamepad)) != disableIfUsing);
		}
	}
}
