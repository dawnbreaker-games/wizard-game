using Godot;
using System;

namespace FightRoom
{
	public partial class Asset : Node
	{
		public object data;
		public Data _Data
		{
			get
			{
				return (Data) data;
			}
			set
			{
				data = value;
			}
		}

		public override void _Ready ()
		{
			if (_Data == null)
				_Data = new Data();
		}

		public virtual void SetData ()
		{
			throw new NotImplementedException();
		}

		[Serializable]
		public partial class Data : ISaveableAndLoadable
		{
			[SaveAndLoadValue]
			public string name;

			public virtual object MakeAsset ()
			{
				throw new NotImplementedException();
			}

			public virtual void Apply (Asset asset)
			{
				throw new NotImplementedException();
			}
		}
	}
}
