using Godot;
using System;

namespace FightRoom
{
	[Serializable]
	public partial class Team<T>
	{
		public T representative;
		public Color color;
		public Material material;
		public Team<T> opponent;
		public Team<T>[] opponents = new Team<T>[0];
	}
}
