

namespace FightRoom
{
	public partial class AbilityTutorial : Tutorial
	{
		public override void DoUpdate ()
		{
			if (Bullet.instances.Count > 0)
			{
				for (int i = 0; i < Bullet.instances.Count; i ++)
				{
					Bullet bullet = Bullet.instances[i];
					if (bullet.gameObject.layer == int.NameToLayer("Enemy Bullet") && Player.instance.trs.position.y > bullet.trs.position.y)
					{
						Finish ();
						return;
					}
				}
			}
		}
	}
}
