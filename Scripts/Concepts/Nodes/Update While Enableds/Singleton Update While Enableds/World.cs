//using Godot;
//using Extensions;
//using System.Collections.Generic;
//
//namespace FightRoom
//{
//	public partial class World : SingletonUpdateNode2D
//	{
//		public Vector2I sizeOfPieces;
//		public Dictionary<Vector2I, WorldPiece> piecesDict = new Dictionary<Vector2I, WorldPiece>();
//		public WorldPiece[,] pieces;
//		public Vector2I maxPieceLocation;
//		public Transform piecesParent;
//		public Vector2I loadPiecesRange;
//		public List<WorldPiece> activePieces = new List<WorldPiece>();
//		public Transform[] extraTransforms = new Transform[0];
//#if !IS_BUILD
//		public ObjectInWorld[] worldObjects = new ObjectInWorld[0];
//		public WorldPiece piecePrefab;
//		public RectInt rect;
//		public Transform initExtraTransformsParent;
//		public bool updateValues;
//#endif
//		WorldPiece piecePlayerIsIn;
//
//		void Start ()
//		{
//			SettingsMenu.SetLightsActive (SettingsMenu.AllowLights);
//#if !IS_BUILD
//			SetWorldActive (false);
//#endif
//			SetPieces (true);
//		}
//
//#if !IS_BUILD
//		public void OnValidate ()
//		{
//			if (!updateValues)
//				return;
//			updateValues = false;
//			worldObjects = FindObjectsOfType<ObjectInWorld>();
//			for (int i = 0; i < worldObjects.Length; i ++)
//			{
//				ObjectInWorld worldObject = worldObjects[i];
//				if (worldObject.trs == null)
//					MonoBehaviour.print(worldObject);
//				if (!worldObject.enabled || (worldObject.trs.parent != null && worldObject.trs.parent.GetComponent<ObjectInWorld>() != null))
//				{
//					worldObjects = worldObjects.RemoveAt(i);
//					i --;
//				}
//			}
//			for (int i = 0; i < extraTransforms.Length; i ++)
//			{
//				Transform extraTrs = extraTransforms[i];
//				extraTrs.SetParent(initExtraTransformsParent);
//			}
//			Collider2D[] colliders = FindObjectsOfType<Collider2D>();
//			rect = colliders[0].bounds.ToRectInt();
//			for (int i = 1; i < colliders.Length; i ++)
//			{
//				Collider2D collider = colliders[i];
//				rect = new RectInt[] { collider.bounds.ToRectInt(), rect }.Combine();
//			}
//		}
//#endif
//
//		public virtual void SetPieces (bool parentExtraTransforms)
//		{
//			pieces = new WorldPiece[maxPieceLocation.x + 1, maxPieceLocation.y + 1];
//			piecesDict.Clear();
//			piecePlayerIsIn = null;
//			List<Transform> _extraTransforms = new List<Transform>(extraTransforms);
//			for (int i = 0; i < piecesParent.childCount; i ++)
//			{
//				WorldPiece piece = piecesParent.GetChild(i).GetComponent<WorldPiece>();
//				piecesDict.Add(piece.location, piece);
//				pieces[piece.location.x, piece.location.y] = piece;
//				if (piecePlayerIsIn == null && piece.rect.Contains(GameCamera.instance.trs.position.ToVec2Int()))
//					piecePlayerIsIn = piece;
//				if (parentExtraTransforms)
//				{
//					for (int i2 = 0; i2 < _extraTransforms.Count; i2 ++)
//					{
//						Transform extraTrs = _extraTransforms[i2];
//						if (piece.rect.Contains(extraTrs.position.ToVec2Int()))
//						{
//							extraTrs.SetParent(piece.trs);
//							_extraTransforms.RemoveAt(i2);
//							i2 --;
//						}
//					}
//				}
//			}
//		}
//
//		public override void DoUpdate ()
//		{
//			List<WorldPiece> previousActivePieces = new List<WorldPiece>(activePieces);
//			activePieces.Clear();
//			List<WorldPiece> worldPieces = new List<WorldPiece>(new WorldPiece[] { piecePlayerIsIn });
//			while (!piecePlayerIsIn.rect.Contains(GameCamera.instance.trs.position.ToVec2Int()))
//			{
//				WorldPiece[] surroundingPieces = GetSurroundingPieces(worldPieces.ToArray());
//				for (int i = 0; i < surroundingPieces.Length; i ++)
//				{
//					WorldPiece surroundingPiece = surroundingPieces[i];
//					if (surroundingPiece.rect.Contains(GameCamera.instance.trs.position.ToVec2Int()))
//					{
//						piecePlayerIsIn = surroundingPiece;
//						break;
//					}
//				}
//				worldPieces.AddRange(surroundingPieces);
//			}
//			activePieces.Add(piecePlayerIsIn);
//			piecePlayerIsIn.gameObject.SetActive(true);
//			WorldPiece[] surroundingPieces2 = GetSurroundingPieces(piecePlayerIsIn);
//			for (int i = 0; i < surroundingPieces2.Length; i ++)
//			{
//				WorldPiece surroundingPiece = surroundingPieces2[i];
//				RectInt loadPieceRangeRect = surroundingPiece.rect.Expand(loadPiecesRange * 2);
//				if (GameCamera.instance.viewRect.IsIntersecting(loadPieceRangeRect.ToRect()))
//				{
//					activePieces.Add(surroundingPiece);
//					surroundingPiece.gameObject.SetActive(true);
//					for (int i2 = 0; i2 < surroundingPiece.piecesToLoadAndUnloadWithMe.Length; i2 ++)
//					{
//						WorldPiece worldPiece = surroundingPiece.piecesToLoadAndUnloadWithMe[i2];
//						worldPiece.gameObject.SetActive(true);
//						activePieces.Add(worldPiece);
//					}
//				}
//				else
//				{
//					activePieces.Remove(surroundingPiece);
//					surroundingPiece.gameObject.SetActive(false);
//				}
//			}
//			for (int i = 0; i < piecePlayerIsIn.piecesToLoadAndUnloadWithMe.Length; i ++)
//			{
//				WorldPiece worldPiece = piecePlayerIsIn.piecesToLoadAndUnloadWithMe[i];
//				worldPiece.gameObject.SetActive(true);
//				activePieces.Add(worldPiece);
//			}
//			for (int i = 0; i < previousActivePieces.Count; i ++)
//			{
//				WorldPiece previousActivePiece = previousActivePieces[i];
//				if (!activePieces.Contains(previousActivePiece))
//					previousActivePiece.gameObject.SetActive(false);
//			}
//		}
//
//		public virtual WorldPiece[] GetSurroundingPieces (params WorldPiece[] innerPieces)
//		{
//			List<WorldPiece> output = new List<WorldPiece>();
//			for (int i = 0; i < innerPieces.Length; i ++)
//			{
//				WorldPiece piece = innerPieces[i];
//				bool hasPieceRight = piece.location.x < maxPieceLocation.x;
//				bool hasPieceLeft = piece.location.x > 0;
//				bool hasPieceUp = piece.location.y < maxPieceLocation.y;
//				bool hasPieceDown = piece.location.y > 0;
//				if (hasPieceUp)
//				{
//					output.Add(pieces[piece.location.x, piece.location.y + 1]);
//					if (hasPieceRight)
//						output.Add(pieces[piece.location.x + 1, piece.location.y + 1]);
//					if (hasPieceLeft)
//						output.Add(pieces[piece.location.x - 1, piece.location.y + 1]);
//				}
//				if (hasPieceDown)
//				{
//					output.Add(pieces[piece.location.x, piece.location.y - 1]);
//					if (hasPieceRight)
//						output.Add(pieces[piece.location.x + 1, piece.location.y - 1]);
//					if (hasPieceLeft)
//						output.Add(pieces[piece.location.x - 1, piece.location.y - 1]);
//				}
//				if (hasPieceRight)
//					output.Add(pieces[piece.location.x + 1, piece.location.y]);
//				if (hasPieceLeft)
//					output.Add(pieces[piece.location.x - 1, piece.location.y]);
//				for (int i2 = 0; i2 < innerPieces.Length; i2 ++)
//				{
//					WorldPiece piece2 = innerPieces[i2];
//					output.Remove(piece2);
//				}
//			}
//			return output.ToArray();
//		}
//
//#if !IS_BUILD
//		public static void ShowPieces (bool show)
//		{
//			instance.SetPieces (false);
//			foreach (WorldPiece worldPiece in instance.piecesDict.Values)
//				worldPiece.gameObject.SetActive(show);
//		}
//
//		public static void SetWorldActive (bool active)
//		{
//			for (int i = 0; i < instance.worldObjects.Length; i ++)
//			{
//				ObjectInWorld worldObject = instance.worldObjects[i];
//				if (worldObject != null && worldObject.enabled)
//					worldObject.gameObject.SetActive(active);
//			}
//		}
//
//		public static void SetPiecesActive (bool active)
//		{
//			instance.piecesParent.gameObject.SetActive(active);
//		}
//#endif
//	}
//}
