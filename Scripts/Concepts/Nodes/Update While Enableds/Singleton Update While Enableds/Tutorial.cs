using Godot;

namespace FightRoom
{
	public partial class Tutorial : SingletonUpdateNode3D<Tutorial>
	{
		public Node[] activateOnFinish = new Node[0];

		public virtual void Finish ()
		{
			gameObject.SetActive(false);
			for (int i = 0; i < activateOnFinish.Length; i ++)
			{
				Node node = activateOnFinish[i];
				go.SetActive(true);
			}
		}
	}
}
