using Godot;
using System;
using Extensions;
using System.Collections.Generic;

namespace FightRoom
{
	public partial class Enemy : Entity
	{
		public FloatRange targetRangeFromPlayer;
		public Node3D bulletSpawnersParent;
		public BulletPatternEntry[] bulletPatternEntries = new BulletPatternEntry[0];
		public Dictionary<string, BulletPatternEntry> bulletPatternEntriesDict = new Dictionary<string, BulletPatternEntry>();
		public float dodgeBulletDistance;
		public float safeDistance;
		public float difficulty;
		public static List<Enemy> instances = new List<Enemy>();

		public override void OnAboutToDestroy ()
		{
			base.OnAboutToDestroy ();
			instances.Remove(this);
			if (bulletSpawnersParent != null)
				bulletSpawnersParent.Free();
		}

		public override void _Ready ()
		{
			base._Ready ();
			instances.Add(this);
			bulletSpawnersParent.SetParent(null);
			for (int i = 0; i < bulletPatternEntries.Length; i ++)
			{
				BulletPatternEntry bulletPatternEntry = bulletPatternEntries[i];
				bulletPatternEntriesDict.Add(bulletPatternEntry.name, bulletPatternEntry);
			}
		}

		public void ShootBulletPatternEntry (string name)
		{
			bulletPatternEntriesDict[name].Shoot ();
		}
		
		public override void DoUpdate ()
		{
			if (GameManager.paused)
				return;
			HandleAttacking ();
			base.DoUpdate ();
		}

		public override void HandleRotating ()
		{
			trs.up = Level.instance.GetSmallestVectorToPoint(trs.position, Player.instance.trs.position, radius);
		}

		public override void HandleMoving ()
		{
			Vector2 toPlayer = Level.instance.GetSmallestVectorToPoint(trs.position, Player.instance.trs.position, radius + Player.instance.radius);
			float toPlayerDistanceSqr = toPlayer.LengthSquared();
			Vector2 velocity = Vector2.Zero;
			if (dodgeBulletDistance > 0)
			{
				float closestBulletDistanceSqr = dodgeBulletDistance * dodgeBulletDistance;
				Bullet closestBullet = null;
				for (int i = 0; i < Bullet.instances.Count; i ++)
				{
					Bullet bullet = Bullet.instances[i];
					if (bullet.gameObject.layer == int.NameToLayer("Player Bullet"))
					{
						float distanceSqr = Level.instance.GetSmallestVectorToPoint(trs.position, bullet.trs.position, radius + bullet.radius).LengthSquared();
						if (distanceSqr < closestBulletDistanceSqr)
						{
							closestBulletDistanceSqr = distanceSqr;
							closestBullet = bullet;
						}
					}
				}
				if (closestBullet != null)
				{
					velocity = Level.instance.GetSmallestVectorToPoint(trs.position, closestBullet.trs.position, radius + closestBullet.radius).Rotate90().Normalized() * moveSpeed;
					Vector2 toClosestBullet = closestBullet.trs.position - trs.position;
					if ((((Vector2) trs.position + velocity * Time.fixedDeltaTime) - toClosestBullet).LengthSquared() < (((Vector2) trs.position - velocity * Time.fixedDeltaTime) - toClosestBullet).LengthSquared())
						velocity *= -1;
					if (toPlayerDistanceSqr < targetRangeFromPlayer.min * targetRangeFromPlayer.min)
						velocity -= toPlayer.Normalized() * moveSpeed;
					velocity = velocity.Normalized() * moveSpeed;
				}
				else if (toPlayerDistanceSqr > targetRangeFromPlayer.max * targetRangeFromPlayer.max)
					velocity = toPlayer.Normalized() * moveSpeed;
				else if (toPlayerDistanceSqr < targetRangeFromPlayer.min * targetRangeFromPlayer.min)
					velocity = -toPlayer.Normalized() * moveSpeed;
			}
			else if (toPlayerDistanceSqr > targetRangeFromPlayer.max * targetRangeFromPlayer.max)
				velocity = toPlayer.Normalized() * moveSpeed;
			else if (toPlayerDistanceSqr < targetRangeFromPlayer.min * targetRangeFromPlayer.min)
				velocity = -toPlayer.Normalized() * moveSpeed;
			bool willCrashIntoHazard = true;
			for (int i = 0; i < 4; i ++)
			{
				int whatIDodge;
				if (dodgeBulletDistance == 0)
					whatIDodge = int.GetMask("Hazard");
				else
					whatIDodge = int.GetMask("Player Bullet", "Hazard");
				if (Physics2D.CircleCast(trs.position, radius, velocity, safeDistance, whatIDodge).collider == null)
				{
					willCrashIntoHazard = false;
					break;
				}
				velocity = velocity.Rotate90();
			}
			if (willCrashIntoHazard)
				velocity = Vector2.Zero;
			if (Level.instance.type.HasFlag(Level.Type.Wind))
				velocity += ((Vector2) (trs.position - Level.instance.trs.position)).Rotate270().Normalized() * Level.instance.windSpeed;
			rigid.velocity = velocity;
		}

		public virtual void HandleAttacking ()
		{
			bulletSpawnersParent.position = trs.position;
		}
	}
}
