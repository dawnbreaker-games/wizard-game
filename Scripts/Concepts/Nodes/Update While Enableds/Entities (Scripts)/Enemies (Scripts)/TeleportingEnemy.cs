using Godot;
using System;
using Extensions;
using System.Collections.Generic;

namespace FightRoom
{
	public partial class TeleportingEnemy : Enemy
	{
		public FloatRange teleportDistanceRange;
		public FloatRange teleportDelayRange;
		float teleportDelay;
		const uint TELEPORT_DISTANCE_SAMPLES = 17;

		public override void _Ready ()
		{
			teleportDelay = teleportDelayRange.Get(Random.value);
			base._Ready ();
		}

		public override void HandleMoving ()
		{
			base.HandleMoving ();
			HandleTeleporting ();
		}

		void HandleTeleporting ()
		{
			teleportDelay -= Time.deltaTime;
			if (teleportDelay < 0)
			{
				teleportDelay = teleportDelayRange.Get(Random.value);
				Vector2 destination;
				float teleportDistanceNormalized = 1;
				float teleportDistance = teleportDistanceRange.Get(1);
				for (uint i = 0; i < TELEPORT_DISTANCE_SAMPLES; i ++)
				{
					destination = (Vector2) trs.position + rigid.velocity.Normalized() * teleportDistance;
					if (Physics2D.OverlapCircle(destination, radius, int.GetMask("Player Bullet", "Hazard")) == null)
					{
						destination = (Vector2) trs.position + rigid.velocity.Normalized() * teleportDistance;
						Teleport (destination);
						return;
					}
					teleportDistanceNormalized -= 1f / (TELEPORT_DISTANCE_SAMPLES - 1);
					teleportDistance = teleportDistanceRange.Get(teleportDistanceNormalized);
				}
			}
		}

		void Teleport (Vector2 destination)
		{
			trs.position = destination;
			Physics2D.SyncTransforms();
		}
	}
}
