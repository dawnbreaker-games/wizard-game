namespace FightRoom
{
	public partial class Boss : Enemy
	{
		public static Boss instance;
		
		public override void _Ready ()
		{
			instance = this;
			base._Ready ();
		}

		public override void TakeDamage (float amount)
		{
			float previousHp = hp;
			base.TakeDamage (amount);
//			BossLevel bossLevel = (BossLevel) Level.instance;
//			bossLevel.totalDamage += previousHp - hp;
//			bossLevel.currentTimeText.Text = "Damage: " + string.Format("{0:0.#}", bossLevel.totalDamage);
//			if (bossLevel.totalDamage >= bossLevel.totalMaxHp)
//				bossLevel.End ();
		}
	}
}
