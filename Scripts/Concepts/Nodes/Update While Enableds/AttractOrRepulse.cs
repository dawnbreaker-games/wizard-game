using Godot;
using System;
using Extensions;
using System.Collections.Generic;

namespace FightRoom
{
	public partial class AttractOrRepulse : UpdateNode3D
	{
		public FloatRange attractAmountRange;
		public Curve multiplyAttractAmountCurve;
		public FloatRange multiplyTimeRange;
		public AttractEntry[] attractEntries = new AttractEntry[0];
#if !IS_BUILD
		public bool addNewAttractEntries;
		public bool updateAttractEntries;

		void OnValidate ()
		{
			if (updateAttractEntries)
			{
				updateAttractEntries = false;
				UpdateAttractEntries ();
			}
			else if (addNewAttractEntries)
			{
				addNewAttractEntries = false;
				List<AttractOrRepulse> instances = new List<AttractOrRepulse>(FindObjectsOfType<AttractOrRepulse>());
				for (int i = 0; i < this.attractEntries.Length; i ++)
				{
					AttractEntry attractEntry = this.attractEntries[i];
					instances.Remove(attractEntry.attractTo.GetComponent<AttractOrRepulse>());
				}
				instances.Remove(this);
				List<AttractEntry> attractEntries = new List<AttractEntry>(this.attractEntries);
				for (int i = 0; i < instances.Count; i ++)
				{
					AttractOrRepulse instance = instances[i];
					attractEntries.Add(new AttractEntry(instance.trs, attractAmountRange.Get(Random.value), multiplyTimeRange.Get(Random.value), Random.value * multiplyAttractAmountCurve.keys[multiplyAttractAmountCurve.keys.Length - 1].time));
				}
				this.attractEntries = attractEntries.ToArray();
			}
		}
#endif

		public override void _Ready ()
		{
			UpdateAttractEntries ();
			base._Ready ();
		}

		public override void DoUpdate ()
		{
			for (int i = 0; i < attractEntries.Length; i ++)
			{
				AttractEntry attractEntry = attractEntries[i];
				trs.position += (Vector3) ((Vector2) (attractEntry.attractTo.position - trs.position)).Normalized() * attractEntry.amount * multiplyAttractAmountCurve.Evaluate(GameManager.TimeSinceLevelLoad * attractEntry.multiplyTime + attractEntry.offsetTime) * Time.deltaTime;
			}
		}

		void UpdateAttractEntries ()
		{
			List<AttractOrRepulse> instances = new List<AttractOrRepulse>(FindObjectsOfType<AttractOrRepulse>());
			instances.Remove(this);
			attractEntries = new AttractEntry[instances.Count];
			for (int i = 0; i < instances.Count; i ++)
			{
				AttractOrRepulse instance = instances[i];
				attractEntries[i] = new AttractEntry(instance.trs, attractAmountRange.Get(Random.value), multiplyTimeRange.Get(Random.value), Random.value * multiplyAttractAmountCurve.keys[multiplyAttractAmountCurve.keys.Length - 1].time);
			}
		}

		[Serializable]
		public partial class AttractEntry
		{
			public Node3D attractTo;
			public float amount;
			public float multiplyTime;
			public float offsetTime;

			public AttractEntry (Node3D attractTo, float amount, float multiplyTime, float offsetTime)
			{
				this.attractTo = attractTo;
				this.amount = amount;
				this.multiplyTime = multiplyTime;
				this.offsetTime = offsetTime;
			}
		}
	}
}
