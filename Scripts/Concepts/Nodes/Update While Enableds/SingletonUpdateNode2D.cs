using Godot;

namespace FightRoom
{
	public partial class SingletonUpdateNode2D<T> : UpdateNode2D where T : Node2D
	{
		[Export]
		MultipleInstancesHandlingType handleMultipleInstances;
		[Export]
		bool setInstance;
		public static T instance;
		
		public override void _Ready ()
		{
			if (instance == null)
				instance = this;
			else if (instance != this && handleMultipleInstances != MultipleInstancesHandlingType.KeepAll)
			{
				if (handleMultipleInstances == MultipleInstancesHandlingType.DestroyNew)
				{
					Free();
					return;
				}
				else
					instance.Free();
			}
			if (setInstance)
				instance = this;
			base._Ready ();
		}

		public enum MultipleInstancesHandlingType
		{
			KeepAll,
			DestroyNew,
			DestroyOld
		}
	}
}
