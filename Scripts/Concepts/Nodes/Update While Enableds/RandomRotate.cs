using Godot;
using FightRoom;
using Extensions;

public partial class RandomRotate : UpdateNode3D
{
	[Export]
	float missileMapRadius;
	[Export]
	float missileTurnRate;
	[Export]
	float missileSpeed;
	[Export]
	float maxAngleToInitFacing;
	Vector2 missilePosition;
	Vector2 missileDestination;
	Vector2 missileVelocity;
	float missileDistToDestSqr;
	bool missileHasReducedDistToDest;
	Vector3 initFacing;
	
	public override void _Ready ()
	{
		initFacing = ToGlobal(Vector3.Forward);
		missileVelocity = VectorExtensions.RandomDirection() * missileSpeed;
		missileDestination = VectorExtensions.RandomVector() * missileMapRadius;
		base._Ready ();
	}

	public override void DoUpdate ()
	{
		float prevMissileDistToDestSqr = (missilePosition - missileDestination).LengthSquared();
		float missileIdealTurnAmount = missileVelocity.AngleTo(missileDestination - missilePosition);
		float missileTurnAmount = MathfExtensions.Clamp(missileIdealTurnAmount, -missileTurnRate * GameManager.deltaTime, missileTurnRate * GameManager.deltaTime);
		missileVelocity = missileVelocity.Rotate(missileTurnAmount);
		missilePosition += missileVelocity * GameManager.deltaTime;
		float missileDistToDestSqr = (missilePosition - missileDestination).LengthSquared();
		if (missileHasReducedDistToDest && missileDistToDestSqr > prevMissileDistToDestSqr)
		{
			missileDestination = VectorExtensions.RandomVector() * missileMapRadius;
			missileHasReducedDistToDest = false;
		}
		else if (missileDistToDestSqr < prevMissileDistToDestSqr)
			missileHasReducedDistToDest = true;
		RotateObjectLocal(Vector3.Up, missileVelocity.X * GameManager.deltaTime);
		RotateObjectLocal(Vector3.Right, missileVelocity.Y * GameManager.deltaTime);
		float angleToInitFacing = ToGlobal(Vector3.Forward).AngleTo(initFacing);
		float angleOvershoot = angleToInitFacing - maxAngleToInitFacing;
//		if (angleOvershoot > 0)
//			LookAt(Vector3.RotateTowards(ToGlobal(Vector3.Forward), initFacing, angleOvershoot * MathfExtensions.DEG_2_RAD, 0), ToGlobal(Vector3.Up));
	}
}
