using Godot;
using Extensions;
using System.Collections.Generic;

namespace FightRoom
{
	public partial class Entity : UpdateNode3D, IDestructable, ISpawnable
	{
		public string ScenePath
		{
			get
			{
				return "res://Scenes/Enttity.scn";
			}
		}
		public float hp;
		public float Hp
		{
			get
			{
				return hp;
			}
			set
			{
				hp = value;
			}
		}
		public uint maxHp;
		public uint MaxHp
		{
			get
			{
				return maxHp;
			}
			set
			{
				maxHp = value;
			}
		}
		public CharacterBody3D characterBody;
		public MeshInstance3D meshInstance;
		public CollisionObject3D collisionObject;
		public float moveSpeed;
		public float radius;
		public Node bloodTrsPrefab;
		public AnimationPlayer animationPlayer;
		// public AnimationEntry[] animationEntries = new AnimationEntry[0];
		// public Dictionary<string, AnimationEntry> animationEntriesDict = new Dictionary<string, AnimationEntry>();
		// public AudioClip[] deathAudioClips = new AudioClip[0];
		protected bool dead;

		public override void _Ready ()
		{
			base._Ready ();
			hp = maxHp;
			// for (int i = 0; i < animationEntries.Length; i ++)
			// {
			// 	AnimationEntry animationEntry = animationEntries[i];
			// 	animationEntriesDict.Add(animationEntry.animatorStateName, animationEntry);
			// }
		}
		
		public override void DoUpdate ()
		{
			if (GameManager.paused)
				return;
			HandleRotating ();
			HandleMoving ();
		}

		public virtual void HandleRotating ()
		{
		}
		
		public virtual void HandleMoving ()
		{
		}

		public virtual void TakeDamage (float amount)
		{
			if (dead || maxHp < 0 || amount == 0)
				return;
			hp = MathfExtensions.Clamp(hp - amount, 0, MaxHp);
			if (hp == 0)
			{
				dead = true;
				Death ();
			}
		}

		public virtual void Death ()
		{
			Destroy(gameObject);
			// PlayAnimationEntry ("Death");
			// AudioManager.instance.MakeSoundEffect (deathAudioClips[Random.Range(0, deathAudioClips.Length)], trs.position);
		}

		// public void PlayAnimationEntry (string name)
		// {
		// 	animationEntriesDict[name].Play ();
		// }

		public static Entity[] GetInstances ()
		{
			Entity[] output = new Entity[Enemy.instances.Count + 1];
			output[0] = Player.instance;
			for (int i = 0; i < Enemy.instances.Count; i ++)
			{
				Enemy enemy = Enemy.instances[i];
				output[i + 1] = enemy;
			}
			return output;
		}

		public struct Snapshot
		{
			public string scenePath;
			public Entity entity;
			public Vector3 position;
			public Vector3 rotation;
			public AnimationEntry animationEntry;
			public float normalizedAnimationEntryTime;

			public Snapshot (string scenePath, Entity entity, Vector3 position, Vector3 rotation, AnimationEntry animationEntry, float normalizedAnimationEntryTime)
			{
				this.scenePath = scenePath;
				this.entity = entity;
				this.position = position;
				this.rotation = rotation;
				this.animationEntry = animationEntry;
				this.normalizedAnimationEntryTime = normalizedAnimationEntryTime;
			}

			public Snapshot (Entity entity) : this (entity.ScenePath, entity, entity.GlobalPosition, entity.GlobalRotationDegrees, new AnimationEntry(), float.MaxValue)
			{
				normalizedAnimationEntryTime = entity.animator.GetCurrentAnimatorStateInfo(0).normalizedTime;
			}

			public Entity Apply ()
			{
				if (entity == null)
					entity = ObjectPool.instance.Spawn<Entity>(prefabIndex, position, Quaternion.Euler(Vector3.Forward * rotation));
				else
				{
					entity.GlobalPosition = position;
					entity.GlobalRotationDegrees = rotation;
				}
				return entity;
			}
		}
	}
}
