//using Godot;
//using Extensions;
//using System.Collections;
//using System.Collections.Generic;
//
//namespace FightRoom
//{
//	public partial class Patrol : UpdateNode3D
//	{
//		public bool isFlying;
//		public float patrolRange;
//		public float stopRange;
//		public int whatIPatrolOn;
//		public CharacterBody3D characterBody;
//		public float moveSpeed;
//		float yVel;
//		Vector3 move;
//		Vector3 initPosition;
//		Vector3 destination;
//		float stopRangeSqr;
//		Vector3 toDestination;
//
//		public override void _Ready ()
//		{
//			initPosition = trs.position;
//			stopRangeSqr = stopRange * stopRange;
//			SetDestination ();
//			base._Ready ();
//		}
//
//		public override void DoUpdate ()
//		{
//			toDestination = destination - trs.position;
//			if (!isFlying)
//				toDestination = toDestination.SetY(0);
//			move = Vector3.ClampMagnitude(toDestination, 1);
//			move *= moveSpeed;
//			trs.forward = move;
//			HandleGravity ();
//			if (characterBody != null && characterBody.enabled)
//				characterBody.Move(move * Time.deltaTime);
//			else
//				rigid.velocity = move;
//			if (toDestination.LengthSquared() <= stopRangeSqr || (characterBody != null && characterBody.collisionFlags.ToString().Contains("Sides")))
//				SetDestination ();
//		}
//
//		void SetDestination ()
//		{
//			if (isFlying)
//			{
//				destination = initPosition + Random.onUnitSphere * Random.value * patrolRange;
//				return;
//			}
//			do
//			{
//				destination = initPosition + (Random.insideUnitCircle * patrolRange).XYToXZ();
//				RaycastHit hit;
//				if (Physics.Raycast(destination.SetY(trs.position.y + patrolRange), Vector3.down, out hit, float.MaxValue, whatIPatrolOn))
//					return;
//			} while (true);
//		}
//
//		void HandleGravity ()
//		{
//			if (characterBody != null && characterBody.enabled && !characterBody.isGrounded)
//			{
//				yVel += Physics.gravity.y * Time.deltaTime;
//				move += Vector3.up * yVel;
//			}
//			else
//				yVel = 0;
//		}
//	}
//}
