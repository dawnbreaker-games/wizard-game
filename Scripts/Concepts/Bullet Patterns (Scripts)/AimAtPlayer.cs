using Godot;
using System.Collections;
using System.Collections.Generic;

namespace FightRoom
{
	public partial class AimAtPlayer : BulletPattern
	{
		public override Vector3 GetShootDirection (Node3D spawner)
		{
			return Player.instance.GlobalPosition - spawner.GlobalPosition;
//			return Level.instance.GetSmallestVectorToPoint(spawner.position, Player.instance.trs.position, 0);
		}
	}
}
