using Godot;
using Extensions;
using System.Collections;
using System.Collections.Generic;

namespace FightRoom
{
	public partial class ShootInDirectionThenRedirectToDirection : AimInDirection
	{
		public float redirectTime;
		public Vector2 redirectDirection;
		
		public override Bullet[] Shoot (Node3D spawner, Bullet bulletPrefab)
		{
			Bullet[] output = base.Shoot(spawner, bulletPrefab);
			for (int i = 0; i < output.Length; i ++)
			{
				Bullet bullet = output[i];
				RedirectAfterDelay (bullet, redirectDirection, redirectTime);
			}
			return output;
		}
	}
}
