using Godot;
using System.Collections;
using System.Collections.Generic;

namespace FightRoom
{
	public partial class AimWhereFacingThenAimAtPlayer : AimWhereFacing
	{
		// [MakeConfigurable]
		public float aimAtPlayerTime;
		
		public override Bullet[] Shoot (Node3D spawner, Bullet bulletPrefab)
		{
			Bullet[] output = base.Shoot(spawner, bulletPrefab);
			for (int i = 0; i < output.Length; i ++)
			{
				Bullet bullet = output[i];
				RedirectAfterDelay (bullet, aimAtPlayerTime);
			}
			return output;
		}
		
		public override Vector3 GetRedirectDirection (Bullet bullet)
		{
			return Player.instance.trs.position - bullet.trs.position;
		}
	}
}
