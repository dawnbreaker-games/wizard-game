using Godot;

namespace FightRoom
{
	public partial class AimInDirection : BulletPattern
	{
		public Vector3 shootDirection;

		public override Vector3 GetShootDirection (Node3D spawner)
		{
			return shootDirection;
		}
	}
}
