using Godot;
using Extensions;
using System.Collections;
using System.Collections.Generic;

namespace FightRoom
{
	public partial class TargetNearPlayerThenMoveToTargetThenStopAndDespawnWithDelay : BulletPattern
	{
		public FloatRange offsetRange;
		public int whatICantTarget;
		public float destroyDelay;

		public override Bullet[] Shoot (Node3D spawner, Bullet bulletPrefab)
		{
			Vector2 targetPosition;
			do
			{
				targetPosition = (Vector2) Player.instance.trs.position + Random.insideUnitCircle.Normalized() * offsetRange.Get(Random.value);
			} while (Physics2D.OverlapPoint(targetPosition, whatICantTarget) != null);
			targetPosition = (Vector2) spawner.position + Level.instance.GetSmallestVectorToPoint(spawner.position, targetPosition, bulletPrefab.radius);
			Vector2 toTargetPosition = targetPosition - (Vector2) spawner.position;
			Bullet bullet = ObjectPool.instance.Spawn<Bullet>(bulletPrefab.prefabIndex, spawner.position, Quaternion.LookRotation(Vector3.Forward, toTargetPosition));
			float timeUntilReachTarget = toTargetPosition.Length() / bullet.moveSpeed;
			bullet.AddEvent ((object obj) => { StopMovingAndDespawnWithDelay ((Bullet) obj); }, timeUntilReachTarget);
			return new Bullet[] { bullet };
		}

		void StopMovingAndDespawnWithDelay (Bullet bullet)
		{
			bullet.velocity = Vector2.Zero;
			bullet.rigid.velocity = Vector2.Zero;
			ObjectPool.instance.DelayDespawn (bullet.prefabIndex, bullet.gameObject, bullet.trs, destroyDelay);
		}
	}
}
