using Godot;
using Extensions;
using System.Collections;
using System.Collections.Generic;

namespace FightRoom
{
	public partial class AimWhereFacingWithOffsetThenAimAtPlayer : AimWhereFacingThenAimAtPlayer
	{
		// [MakeConfigurable]
		public float shootOffset;
		
		public override Vector3 GetShootDirection (Node3D spawner)
		{
			return VectorExtensions.Rotate(GetShootDirection(spawner), shootOffset);
		}
	}
}
