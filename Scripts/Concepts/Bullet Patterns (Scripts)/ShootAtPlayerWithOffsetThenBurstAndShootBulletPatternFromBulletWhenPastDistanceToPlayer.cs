using Godot;
using Extensions;
using System.Collections;
using System.Collections.Generic;

namespace FightRoom
{
	public partial class ShootAtPlayerWithOffsetThenBurstAndShootBulletPatternFromBulletWhenPastDistanceToPlayer : AimAtPlayer
	{
		public Vector3 shootOffset;
		public Bullet burstBulletPrefab;
		public BulletPattern bulletPattern;
		
		public override Bullet[] Shoot (Node3D spawner, Bullet bulletPrefab)
		{
			spawner.forward = Player.instance.trs.position - spawner.position;
			spawner.Rotate(shootOffset);
			Vector3 spawnPosition = spawner.position;
			Bullet bullet = ObjectPool.instance.Spawn<Bullet>(bulletPrefab.prefabIndex, spawnPosition, spawner.rotation);
			bullet.AddEvent ((object obj) => { BurstRoutine ((Bullet) obj, spawnPosition); });
			return new Bullet[] { bullet };
		}

		void BurstRoutine (Bullet bullet, Vector3 initPosition)
		{
			if ((bullet.trs.position - initPosition).LengthSquared() >= (Player.instance.trs.position - bullet.trs.position).LengthSquared())
			{
				ObjectPool.instance.Despawn (bullet.prefabIndex, bullet.gameObject, bullet.trs);
				bulletPattern.Shoot (bullet.trs, burstBulletPrefab);
			}
			else
				bullet.AddEvent ((object obj) => { BurstRoutine ((Bullet) obj, initPosition); });
		}
	}
}
