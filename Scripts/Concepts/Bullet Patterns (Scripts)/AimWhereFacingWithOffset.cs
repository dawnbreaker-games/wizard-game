using Godot;
using Extensions;
using System.Collections.Generic;

namespace FightRoom
{
	public partial class AimWhereFacingWithOffset : AimWhereFacing
	{
		public Vector3 offset;

		public override Vector3 GetShootDirection (Node3D spawner)
		{
			return base.GetShootDirection(spawner).Rotate(Quaternion.Euler(offset));
		}
	}
}
