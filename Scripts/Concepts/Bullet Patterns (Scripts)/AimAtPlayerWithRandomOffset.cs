using Godot;
using Extensions;
using System.Collections;
using System.Collections.Generic;

namespace FightRoom
{
	public partial class AimAtPlayerWithRandomOffset : AimAtPlayer
	{
		// [MakeConfigurable]
		public FloatRange randomShootOffsetRange;
		
		public override Vector3 GetShootDirection (Node3D spawner)
		{
			return base.GetShootDirection(spawner).Rotate(randomShootOffsetRange.Get(Random.value));
		}
	}
}
