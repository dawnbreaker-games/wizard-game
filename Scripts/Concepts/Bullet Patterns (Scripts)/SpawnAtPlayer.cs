using Godot;
using System.Collections;
using System.Collections.Generic;

namespace FightRoom
{
	public partial class SpawnAtPlayer : BulletPattern
	{
		public override Bullet[] Shoot (Node3D spawner, Bullet bulletPrefab)
		{
			return new Bullet[] { Instantiate(bulletPrefab, Player.instance.trs.position, spawner.rotation) };
		}
	}
}
