using Godot;
using Extensions;
using System.Collections;
using System.Collections.Generic;

namespace FightRoom
{
	public partial class ChooseFromLocalRotationsThenShootBulletPattern : BulletPattern
	{
		public BulletPattern bulletPattern;
		public ChooseMethod chooseMethod;
		public Vector3[] localRotations = new Vector3[0];
		public float accuracy = 1;
		
		public override Bullet[] Shoot (Node3D spawner, Bullet bulletPrefab)
		{
			int indexToUse = -1;
			if (chooseMethod == ChooseMethod.Random)
				indexToUse = Random.Range(0, localRotations.Length);
			else if (chooseMethod == ChooseMethod.LoopForwards)
			{
				indexToUse = IndexOfCurrentRotationInArray(spawner) + 1;
				if (indexToUse == localRotations.Length)
					indexToUse = 0;
			}
			else if (chooseMethod == ChooseMethod.LoopBackwards)
			{
				indexToUse = IndexOfCurrentRotationInArray(spawner) - 1;
				if (indexToUse == -1)
					indexToUse = localRotations.Length - 1;
			}
			spawner.localEulerAngles = localRotations[indexToUse];
			return bulletPattern.Shoot(spawner, bulletPrefab);
		}

		public virtual int IndexOfCurrentRotationInArray (Node3D spawner)
		{
			for (int i = 0; i < localRotations.Length; i ++)
			{
				if (Quaternion.Angle(Quaternion.Euler(localRotations[i]), Quaternion.Euler(spawner.localEulerAngles)) <= accuracy)
					return i;
			}
			return -1;
		}

		public enum ChooseMethod
		{
			Random,
			LoopForwards,
			LoopBackwards
		}
	}
}
