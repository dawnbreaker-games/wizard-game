using Godot;

namespace FightRoom
{
	public partial class AimInLocalDirection : BulletPattern
	{
		public Vector3 shootDirection;

		public override Vector3 GetShootDirection (Node3D spawner)
		{
			return spawner.rotation * shootDirection;
		}
	}
}
