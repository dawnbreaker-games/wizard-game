using Godot;
using System;
using System.Collections.Generic;

namespace FightRoom
{
	public partial class ShootBulletPatternEveryXTimes : BulletPattern
	{
		public bool shootAtFirstTime;
		public uint xTimes;
		public BulletPattern bulletPattern;
		Dictionary<Node3D, uint> timesRemainingDict = new Dictionary<Node3D, uint>();

		public override void Init (Node3D spawner)
		{
			if (shootAtFirstTime)
				timesRemainingDict[spawner] = 1;
			else
				timesRemainingDict[spawner] = xTimes;
		}

		public override Bullet[] Shoot (Node3D spawner, Bullet bulletPrefab)
		{
			timesRemainingDict[spawner] --;
			if (timesRemainingDict[spawner] == 0)
			{
				timesRemainingDict[spawner] = xTimes;
				return bulletPattern.Shoot(spawner, bulletPrefab);
			}
			return new Bullet[0];
		}
	}
}
