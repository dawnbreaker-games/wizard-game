using Godot;
using Extensions;
using System.Collections;
using System.Collections.Generic;

namespace FightRoom
{
	public partial class ShootBulletPatternAtOtherBulletPatternAiming : BulletPattern
	{
		public BulletPattern bulletPatternToGetAim;
		public BulletPattern shootBulletPattern;

		public override Bullet[] Shoot (Node3D spawner, Bullet bulletPrefab)
		{
			return shootBulletPattern.Shoot(spawner.position, bulletPatternToGetAim.GetShootDirection(spawner), bulletPrefab);
		}
	}
}
