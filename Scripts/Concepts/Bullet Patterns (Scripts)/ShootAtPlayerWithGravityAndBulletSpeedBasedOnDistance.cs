using Godot;
using Extensions;
using System.Collections;
using System.Collections.Generic;

namespace FightRoom
{
	public partial class ShootAtPlayerWithGravityAndBulletSpeedBasedOnDistance : BulletPattern
	{
		public Curve bulletSpeedMultiplierOverDistance;

		public override Bullet[] Shoot (Node3D spawner, Bullet bulletPrefab)
		{
			Vector3 toPlayer = Player.instance.trs.position - spawner.position;
			float x = toPlayer.SetY(0).Length();
			float y = -toPlayer.y;
			float speed = MathfExtensions.Clamp(bulletSpeedMultiplierOverDistance.Evaluate(toPlayer.Length()), 0, bulletPrefab.moveSpeed);
			float speedSqr = speed * speed;
			float gravity = Physics.gravity.y;
			float parentheses = gravity * x * x + 2 * y * speedSqr;
			float sqrt = Mathf.Sqrt(speedSqr * speedSqr - gravity * parentheses);
			float denominator = gravity * x;
			float angle1 = Mathf.Atan((speedSqr + sqrt) / denominator) * Mathf.Rad2Deg;
			float angle2 = Mathf.Atan((speedSqr - sqrt) / denominator) * Mathf.Rad2Deg;
			spawner.localEulerAngles = spawner.localEulerAngles.SetX(Mathf.Max(angle1, angle2));
			Bullet bullet = ObjectPool.instance.Spawn<Bullet>(bulletPrefab.prefabIndex, spawner.position, spawner.rotation);
			bullet.moveSpeed = speed;
			bullet.rigid.velocity = bullet.trs.up * speed;
			return new Bullet[] { bullet };
		}
	}
}
