using Godot;
using Extensions;
using System.Collections;
using System.Collections.Generic;

namespace FightRoom
{
	public partial class ShootBulletPatternThenRotate : BulletPattern
	{
		public BulletPattern bulletPattern;
		public Vector3 rotation;
		
		public override Bullet[] Shoot (Node3D spawner, Bullet bulletPrefab)
		{
			Bullet[] output = new Bullet[0];
			if (bulletPattern != null)
				output = bulletPattern.Shoot(spawner, bulletPrefab);
			spawner.Rotate(rotation);
			return output;
		}
	}
}
