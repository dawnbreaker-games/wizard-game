using Godot;
using Extensions;
using System.Collections;
using System.Collections.Generic;

namespace FightRoom
{
	public partial class AimAtPlayerWithOffset : AimAtPlayer
	{
		// [MakeConfigurable]
		public float offset;
		
		public override Vector3 GetShootDirection (Node3D spawner)
		{
			return base.GetShootDirection(spawner).Rotate(offset);
		}
	}
}
