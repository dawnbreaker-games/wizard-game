using Godot;
using Extensions;
using System.Collections;
using System.Collections.Generic;

namespace FightRoom
{
	public partial class AimWhereFacingWithRandomOffset : AimWhereFacing
	{
		public FloatRange randomShootOffsetRange;
		
		public override Vector3 GetShootDirection (Node3D spawner)
		{
			return base.GetShootDirection(spawner).Rotate(Quaternion.Euler(Random.onUnitSphere * randomShootOffsetRange.Get(Random.value)));
		}
	}
}
