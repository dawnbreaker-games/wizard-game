//using Godot;
//using FightRoom;
//using Extensions;
//
//public static class Line2DUtilities
//{
//	public static void SetLine2DsToAABBSides (AABB aabb, Line2D[] lines)
//	{
//		LineSegment3D[] sides = aabb.GetSides();
//		for (int i = 0; i < 12; i ++)
//		{
//			LineSegment3D side = sides[i];
//			lines[i].SetPositions(new Vector3[2] { side.start, side.end });
//		}
//	}
//
//	public static Line2D AddLine2DToNode2DOrMakeNew (Node2D node)
//	{
//		if (node == null)
//			node = new Node2D();
//		else if (node.GetComponent<Line2D>() != null)
//		{
//			Transform trs = node.GetComponent<Transform>();
//			node = new Node2D();
//			node.GetComponent<Transform>().SetParent(trs);
//		}
//		return node.AddComponent<Line2D>();
//	}
//
//	public static Line2D AddLine2DToNode2DOrMakeNew (Node2D node, FollowWaypoints.WaypointPath waypointPath)
//	{
//		Line2D line = AddLine2DToNode2DOrMakeNew(node);
//		SetGraphicsStyle (line, waypointPath);
//		return line;
//	}
//
//	public static Line2D AddLine2DToNode2D (Node2D node, FollowWaypoints.WaypointPath waypointPath)
//	{
//		Line2D line = node.AddComponent<Line2D>();
//		SetGraphicsStyle (line, waypointPath);
//		return line;
//	}
//
//	public static void SetGraphicsStyle (this Line2D line, FollowWaypoints.WaypointPath waypointPath)
//	{
//		line.sharedMaterial = waypointPath.material;
//		line.startColor = waypointPath.color;
//		line.endColor = waypointPath.color;
//		line.startWidth = waypointPath.width;
//		line.endWidth = waypointPath.width;
//		line.sortingLayerName = waypointPath.sortingLayerName;
//		line.sortingOrder = MathfExtensions.Clamp(waypointPath.sortingOrder, -32768, 32767);
//	}
//
//	public static void SetUseWorldSpace (this Line2D line, bool useWorldSpace)
//	{
//		line.useWorldSpace = useWorldSpace;
//		Vector3[] positions = new Vector3[line.positionCount];
//		line.GetPositions(positions);
//		for (int i = 0; i < line.positionCount; i ++)
//		{
//			if (useWorldSpace)
//				positions[i] = trs.TransformPoint(positions[i]);
//			else
//				positions[i] = trs.InverseTransformPoint(positions[i]);
//		}
//		line.SetPositions(positions);
//	}
//}
