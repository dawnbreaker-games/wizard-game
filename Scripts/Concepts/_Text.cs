using Godot;
using System;
using Extensions;
using System.Collections;
using System.Collections.Generic;

namespace FightRoom
{
	public partial class _Text : UpdateNode3D, ISpawnable
	{
		public string ScenePath
		{
			get
			{
				throw new NotImplementedException();
			}
		}
		public string Text
		{
			get
			{
				return text;
			}
			set
			{
				if (text != value)
				{
					text = value;
					UpdateText ();
				}
			}
		}
		[Export]
		public Node3D scalerNode;
		[Export]
		public Node3D textCharactersParent;
		[Export]
		public Sprite3D backgroundSprite;
		[Export]
		public TextCharacter textCharacterPrefab;
		[Export]
		public Font font;
		public HorizontalAlignment horizontalAlignment;
		public VerticalAlignment verticalAlignment;
		[Export]
		public float minGlyphWidth;
		[Export]
		public float maxGlyphWidth;
		[Export]
		public float minGlyphHeight;
		[Export]
		public float maxGlyphHeight;
		[Export]
		public Material material;
		[Export]
		public bool overrideSorting;
		[Export]
		public string sortingLayerName;
		[Export]
		public int sortingOrder;
		[Export]
		public DisplayDelayMode displayDelayMode;
		[Export]
		public float displayDelayPerCharacter;
		[Export]
		public float addToTotalDisplayDelay;
#if !IS_BUILD
		[Export]
		public bool updateText;
		[Export]
		public Color debugBoxColor;
#endif
		[Export]
		string text;
		float displayDelayRemaining;
		int currentChracterIndex;
		int currentSentenceIndex;
		List<TextCharacter[]> sentences = new List<TextCharacter[]>();
		TextCharacter[] currentSentence = new TextCharacter[0];
		TextCharacter[] textCharacters = new TextCharacter[0];

#if !IS_BUILD
		void OnValidate ()
		{
			if (updateText)
			{
				updateText = false;
				minGlyphWidth = float.MaxValue;
				maxGlyphWidth = -float.MaxValue;
				minGlyphHeight = float.MaxValue;
				maxGlyphHeight = -float.MaxValue;
				for (int i = 0; i < font.glyphTable.Count; i ++)
				{
					Glyph glyph = font.glyphTable[i];
					GlyphMetrics glyphMetrics = glyph.metrics;
					minGlyphWidth = Mathf.Min(glyphMetrics.horizontalBearingX, minGlyphWidth);
					maxGlyphWidth = Mathf.Max(glyphMetrics.width * glyph.scale + glyphMetrics.horizontalBearingX + glyphMetrics.horizontalAdvance, maxGlyphWidth);
					minGlyphHeight = Mathf.Min(glyphMetrics.horizontalBearingY, minGlyphHeight);
					maxGlyphHeight = Mathf.Max(glyphMetrics.height * glyph.scale + glyphMetrics.horizontalBearingY, maxGlyphHeight);
				}
				UpdateText ();
			}
		}

		void OnDrawGizmos ()
		{
			Vector3[] corners = new Vector3[4];
			rectTrs.GetLocalCorners(corners);
			Vector3 aabbMin = rectTrs.TransformPoint(corners[0]);
			Vector3 aabbMax = rectTrs.TransformPoint(corners[2]);
			AABB aabb = new AABB((aabbMax + aabbMin) / 2, Quaternion.Inverse(rectTrs.rotation) * (aabbMax - aabbMin));
			DebugExtensions.DrawAABB (aabb, debugBoxColor, 0, rectTrs.rotation);
		}
#endif

		public virtual IEnumerator Start ()
		{
#if !IS_BUILD
			if (!Application.isPlaying)
				yield break;
#endif
			yield return new WaitForEndOfFrame();
			if (gameObject.activeInHierarchy)
				UpdateText ();
		}

		public virtual void OnDisable ()
		{
			GameManager.updatables = GameManager.updatables.Remove(this);
		}

		public void UpdateText ()
		{
			GameManager.updatables = GameManager.updatables.Remove(this);
			for (int i = 0; i < textCharactersParent.childCount; i ++)
			{
				Transform child = textCharactersParent.GetChild(i);
#if !IS_BUILD
				if (!Application.isPlaying)
				{
					GameManager.DestroyOnNextEditorUpdate (child.gameObject);
					return;
				}
#endif
				TextCharacter textCharacter2 = child.GetComponent<TextCharacter>();
				ObjectPool.instance.Despawn (textCharacter2.prefabIndex, textCharacter2.gameObject, textCharacter2.trs);
				i --;
			}
			if (string.IsNullOrEmpty(text))
			{
				gameObject.SetActive(false);
				return;
			}
			scalerNode.Scale = Vector3.one;
			textCharactersParent.Scale = Vector3.one;
			textCharactersParent.Position = Vector3.Zero;
			Vector2 drawingPosition = Vector2.Zero;
			Vector3[] corners = new Vector3[4];
			rectTrs.GetLocalCorners(corners);
			Vector3 aabbMin = corners[0];
			Vector3 aabbMax = corners[2];
			AABB aabb = new AABB((aabbMax + aabbMin) / 2, aabbMax - aabbMin);
			string[] lines = text.Split("\n");
			int maxLineLength = 0;
			for (int i = 0; i < lines.Length; i ++)
			{
				string line = lines[i];
				maxLineLength = Mathf.Max(line.Length, maxLineLength);
			}
			float maxGlyphSizeX = maxGlyphWidth - minGlyphWidth;
			float targetMaxLineWidth = maxGlyphSizeX * text.Length;
			float aabbAspectRatio = aabb.size.x / aabb.size.y;
			float aspectRatiosDistance = float.MaxValue;
			float lineHeight = maxGlyphHeight - minGlyphHeight;
			int _lineCount = lines.Length;
			_lineCount += (int) ((float) maxLineLength * maxGlyphWidth / targetMaxLineWidth);
			float _textWidth = targetMaxLineWidth;
			float _textHeight = _lineCount * lineHeight;
			bool sizesAreBothThickerInSameAxis = (_textWidth >= _textHeight) == (aabb.size.x >= aabb.size.y);
			while (targetMaxLineWidth > maxGlyphSizeX)
			{
				int lineCount = lines.Length;
				lineCount += (int) ((float) maxLineLength * maxGlyphWidth / targetMaxLineWidth);
				float textWidth = targetMaxLineWidth;
				float textHeight = lineCount * lineHeight;
				bool newSizesAreBothThickerInSameAxis = (textWidth >= textHeight) == (aabb.size.x >= aabb.size.y);
				if (sizesAreBothThickerInSameAxis != newSizesAreBothThickerInSameAxis)
					break;
				targetMaxLineWidth -= maxGlyphSizeX;
			}
			targetMaxLineWidth += maxGlyphSizeX;
			Vector2 min = VectorExtensions.INFINITE2;
			Vector2 max = -VectorExtensions.INFINITE2;
			bool overTargetMaxLineWidth = false;
			List<TextCharacter> _currentSentence = new List<TextCharacter>();
			List<TextCharacter> _textCharacters = new List<TextCharacter>();
			for (int i = 0; i < text.Length; i ++)
			{
				char c = text[i];
				if (c == '\n')
				{
					drawingPosition.x = 0;
					drawingPosition.y -= lineHeight;
					overTargetMaxLineWidth = false;
				}
				else
				{
					TMP_Character character;
					if (font.characterLookupTable.TryGetValue(c, out character))
					{
						Glyph glyph = character.glyph;
						GlyphRect glyphRect = glyph.glyphRect;
						GlyphMetrics glyphMetrics = glyph.metrics;
						if (!overTargetMaxLineWidth && drawingPosition.x != 0 && drawingPosition.x + glyphMetrics.horizontalBearingX + glyphMetrics.width * glyph.scale > targetMaxLineWidth)
							overTargetMaxLineWidth = true;
						if (overTargetMaxLineWidth && c == ' ')
						{
							drawingPosition.x = 0;
							drawingPosition.y -= lineHeight;
							overTargetMaxLineWidth = false;
						}
						else
						{
							TextCharacter textCharacter = ObjectPool.instance.Spawn<TextCharacter>(textCharacterPrefab, rotation:rectTrs.rotation, parent:textCharactersParent);
							textCharacter.gameObject.layer = gameObject.layer;
							Material material = new Material(this.material);
							material.SetVector("_glyphPositionInAtlas", new Vector2(glyphRect.x, glyphRect.y));
							material.SetVector("_glyphSizeInAtlas", new Vector2(glyphRect.width, glyphRect.height));
							textCharacter.spriteRenderer.sharedMaterial = material;
							if (overrideSorting)
							{
								textCharacter.spriteRenderer.sortingLayerName = sortingLayerName;
								textCharacter.spriteRenderer.sortingOrder = sortingOrder;
							}
							textCharacter.trs.localScale = new Vector3(glyphMetrics.width, glyphMetrics.height, textCharacter.trs.localScale.z) * glyph.scale;
							Vector2 glyphOffset = new Vector2(glyphMetrics.horizontalBearingX, glyphMetrics.horizontalBearingY);
							Vector2 textCharacterMin = drawingPosition + glyphOffset;
							textCharacter.trs.localPosition = textCharacterMin + (Vector2) textCharacter.trs.localScale / 2;
							min = Vector2.Min(textCharacterMin, min);
							Vector2 textCharacterMax = textCharacterMin + (Vector2) textCharacter.trs.localScale;
							max = Vector2.Max(textCharacterMax, max);
							drawingPosition.x += textCharacter.trs.localScale.x + glyphMetrics.horizontalAdvance;
							if (displayDelayMode == DisplayDelayMode.NoDelay || (_textCharacters.Count == 0 && displayDelayMode == DisplayDelayMode.DelayEachCharacter) || (sentences.Count == 0 && displayDelayMode == DisplayDelayMode.DelayEachSentence))
								textCharacter.gameObject.SetActive(true);
							else
								textCharacter.gameObject.SetActive(false);
							_currentSentence.Add(textCharacter);
							_textCharacters.Add(textCharacter);
						}
					}
					if ( c == '.' || c == '?' || c == '!')
					{
						sentences.Add(_currentSentence.ToArray());
						_currentSentence.Clear();
					}
				}
			}
			if (min.x == float.MaxValue)
			{
				gameObject.SetActive(false);
				return;
			}
			textCharacters = _textCharacters.ToArray();
			Vector2 size = max - min;
			gameObject.SetActive(true);
			Vector2 newLocalScale = aabb.size.Divide(size);
			float minLocalScaleComponent = Mathf.Min(newLocalScale.x, newLocalScale.y);
			scalerNode.localScale = new Vector3(minLocalScaleComponent, minLocalScaleComponent, 1);
			string alignmentOptionsString = alignmentOptions.ToString();
			bool alignmentOptionsContainsLeft = alignmentOptionsString.Contains("Left");
			bool alignmentOptionsContainsRight = alignmentOptionsString.Contains("Right");
			bool alignmentOptionsContainsDown = alignmentOptionsString.Contains("Bottom");
			bool alignmentOptionsContainsUp = alignmentOptionsString.Contains("Top");
			if (alignmentOptionsContainsLeft)
				textCharactersParent.localPosition = textCharactersParent.localPosition.SetX(aabb.min.x / minLocalScaleComponent);
			else if (alignmentOptionsContainsRight)
				textCharactersParent.localPosition = textCharactersParent.localPosition.SetX(aabb.max.x / minLocalScaleComponent - size.x);
			else
				textCharactersParent.localPosition = textCharactersParent.localPosition.SetX(aabb.center.x / minLocalScaleComponent - size.x / 2);
			if (alignmentOptionsContainsDown)
				textCharactersParent.localPosition = textCharactersParent.localPosition.SetY(aabb.min.y / minLocalScaleComponent);
			else if (alignmentOptionsContainsUp)
				textCharactersParent.localPosition = textCharactersParent.localPosition.SetY(aabb.max.y / minLocalScaleComponent - size.y);
			else
				textCharactersParent.localPosition = textCharactersParent.localPosition.SetY(aabb.center.y / minLocalScaleComponent - size.y / 2);
			if (overrideSorting)
			{
				backgroundSprite.sortingLayerName = sortingLayerName;
				backgroundSprite.sortingOrder = sortingOrder - 1;
			}
//			backgroundSprite.gameObject.layer = gameObject.layer;
			backgroundSprite.Position = (Vector2) textCharactersParent.Position + size / 2;
			textCharactersParent.Position = textCharactersParent.localPosition - (Vector3) min;
			backgroundSprite.Scale = size.SetZ(backgroundSprite.Scale.z);
			if (displayDelayMode != DisplayDelayMode.NoDelay)
			{
				if (displayDelayMode == DisplayDelayMode.DelayEachCharacter)
				{
					if (textCharacters.Length > 1)
					{
						currentChracterIndex = 1;
						displayDelayRemaining = displayDelayPerCharacter + addToTotalDisplayDelay / textCharacters.Length;
						GameManager.updatables = GameManager.updatables.Add(this);
					}
				}
				else if (sentences.Count > 1)
				{
					currentSentence = sentences[0];
					currentSentenceIndex = 1;
					currentChracterIndex = currentSentence.Length;
					displayDelayRemaining = displayDelayPerCharacter * currentSentence.Length + addToTotalDisplayDelay / sentences.Count;
					currentSentence = sentences[currentSentenceIndex];
					GameManager.updatables = GameManager.updatables.Add(this);
				}
			}
		}

		public override void DoUpdate ()
		{
			displayDelayRemaining -= Time.unscaledDeltaTime;
			if (displayDelayRemaining <= 0)
			{
				if (displayDelayMode == DisplayDelayMode.DelayEachCharacter)
				{
					DisplayNextCharacter ();
					displayDelayRemaining += displayDelayPerCharacter + addToTotalDisplayDelay / textCharacters.Length;
				}
				else
				{
					for (int i = 0; i < currentSentence.Length; i ++)
						DisplayNextCharacter ();
					currentSentenceIndex ++;
					if (currentSentenceIndex < sentences.Count)
					{
						displayDelayRemaining += displayDelayPerCharacter * currentSentence.Length + addToTotalDisplayDelay / sentences.Count;
						currentSentence = sentences[currentSentenceIndex];
					}
				}
			}
		}

		void DisplayNextCharacter ()
		{
			if (currentChracterIndex >= textCharacters.Length)
			{
				GameManager.updatables = GameManager.updatables.Remove(this);
				throw new Exception("currentCharacterIndex is " + currentChracterIndex + " and textCharacters.Length is " + textCharacters.Length + " when running DisplayNextCharacter");
			}
			TextCharacter currentChracter = textCharacters[currentChracterIndex];
			currentChracter.gameObject.SetActive(true);
			currentChracterIndex ++;
			if (currentChracterIndex >= textCharacters.Length)
				GameManager.updatables = GameManager.updatables.Remove(this);
		}

		public void SetAlpha (float alpha)
		{
			Material material = new Material(backgroundSprite.sharedMaterial);
			material.SetColor("_tint", material.GetColor("_tint").SetAlpha(alpha));
			backgroundSprite.sharedMaterial = material;
			for (int i = 0; i < textCharacters.Length; i ++)
			{
				TextCharacter textCharacter = textCharacters[i];
				material = new Material(textCharacter.spriteRenderer.sharedMaterial);
				material.SetColor("_tint", material.GetColor("_tint").SetAlpha(alpha));
				textCharacter.spriteRenderer.sharedMaterial = material;
			}
		}

		public enum DisplayDelayMode
		{
			NoDelay,
			DelayEachCharacter,
			DelayEachSentence
		}
	}
}
