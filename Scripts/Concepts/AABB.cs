using Godot;
using System;

[Serializable]
public struct AABB
{
	public Vector3 min;
	public Vector3 max;

	public AABB (Vector3 min, Vector3 max)
	{
		this.min = min;
		this.max = max;
	}

	public AABB (AABB aabb)
	{
		min = aabb.min;
		max = aabb.max;
	}
	
	public Vector3 GetCenter ()
	{
		return (min + max) / 2;
	}
}
