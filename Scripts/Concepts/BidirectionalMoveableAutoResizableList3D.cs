using Godot;
using System;
using Extensions;
using System.Collections.Generic;

[Serializable]
public partial class BidirectionalMoveableAutoResizableList3D<T> : BidirectionalMoveableAutoResizableList<BidirectionalMoveableAutoResizableList2D<T>>
{
	public T this[int xIndex, int yIndex, int zIndex]
	{
		get
		{
			return this[xIndex][yIndex, zIndex];
		}
		set
		{
			if (autoResizeWhenGetAndSetElements)
			{
				if (xIndex - indexRange.min >= values.Count)
				{
					indexRange = new IntRange(Mathf.Min(indexRange.min, xIndex), Mathf.Max(indexRange.max, xIndex));
					Add (new BidirectionalMoveableAutoResizableList2D<T>(), true);
				}
				else if (xIndex - indexRange.min < 0)
				{
					indexRange = new IntRange(Mathf.Min(indexRange.min, xIndex), Mathf.Max(indexRange.max, xIndex));
					Add (new BidirectionalMoveableAutoResizableList2D<T>(), false);
				}
			}
			this[xIndex][yIndex, zIndex] = value;
		}
	}

	public BidirectionalMoveableAutoResizableList3D () : base ()
	{
	}

	public BidirectionalMoveableAutoResizableList3D (Vector3I minIndex, Vector3I maxIndex, bool autoResizeWhenGetAndSetElements = true)
	{
		indexRange = new IntRange(minIndex.X, maxIndex.X);
		this.autoResizeWhenGetAndSetElements = autoResizeWhenGetAndSetElements;
		for (int x = minIndex.X; x <= maxIndex.X; x ++)
		{
			for (int y = minIndex.Y; y <= maxIndex.Y; y ++)
			{
				for (int z = minIndex.Z; z <= maxIndex.Z; z ++)
					this[x] = new BidirectionalMoveableAutoResizableList2D<T>(new Vector2I(minIndex.Y, maxIndex.Y), new Vector2I(minIndex.Z, maxIndex.Z));
					// Insert (new Vector3I(x, y, z), default(T), x > maxIndex.X, y > maxIndex.Y, z > maxIndex.Z);
			}
		}
	}

	public void Insert (Vector3I index, T element, bool toXEnd, bool toYEnd, bool toZEnd)
	{
		BidirectionalMoveableAutoResizableList2D<T> list = this[index.X];
		list.Insert(new Vector2I(index.Y, index.Z), element, toYEnd, toZEnd);
		if (Count > 1)
		{
			if (toXEnd)
				indexRange.max ++;
			else
				indexRange.min --;
		}
	}

	public bool Remove (T element, bool toXEnd, bool toYEnd, bool toZEnd)
	{
		for (int i = indexRange.min; i <= indexRange.max; i ++)
		{
			BidirectionalMoveableAutoResizableList2D<T> list = this[i];
			if (list.Remove(element, toYEnd, toZEnd))
			{
				if (toXEnd)
					indexRange.max --;
				else
					indexRange.min ++;
				return true;
			}
		}
		return false;
	}

	public void RemoveAt (Vector3I index, bool toXEnd, bool toYEnd)
	{
		BidirectionalMoveableAutoResizableList2D<T> list = this[index.X];
		list.RemoveAt(index.X, toYEnd);
		if (toXEnd)
			indexRange.max --;
		else
			indexRange.min ++;
	}

	public void Add (T element, bool toXEnd, bool toYEnd, bool toZEnd)
	{
		if (toXEnd)
		{
			BidirectionalMoveableAutoResizableList2D<T> list = this[Count - 1];
			Add(list);
			if (Count > 1)
				indexRange.max ++;
		}
		else
		{
			BidirectionalMoveableAutoResizableList2D<T> list = this[0];
			list.Insert(Vector2I.Zero, element, toYEnd, toZEnd);
			Insert(0, list);
			if (Count > 1)
				indexRange.min --;
		}
	}
}
