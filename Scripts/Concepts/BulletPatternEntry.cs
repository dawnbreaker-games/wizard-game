using Godot;
using System;

namespace FightRoom
{
	[Serializable]
	public partial class BulletPatternEntry
	{
		public string name;
		public BulletPattern bulletPattern;
		public Bullet bulletPrefab;
		public Node3D spawner;
		
		public virtual Bullet[] Shoot ()
		{
			return bulletPattern.Shoot(spawner, bulletPrefab);
		}
	}
}
