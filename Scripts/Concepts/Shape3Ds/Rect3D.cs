using Godot;
using System;
using Extensions;

[Serializable]
public partial class Rect3D : Shape3D
{
	public Vector3 center;
	public Vector2 size;
	public Vector3 rotation;

	public Rect3D (Vector3 center, Vector2 size, Vector3 rotation)
	{
		this.center = center;
		this.size = size;
		this.rotation = rotation;
	}
}
