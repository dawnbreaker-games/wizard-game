using Godot;
using System;

namespace FightRoom
{
	[Serializable]
	public struct AnimationEntry
	{
		public string animationName;
		public AnimationPlayer animationPlayer;

		public void Play ()
		{
			animationPlayer.Play(animationName);
		}

		public bool IsPlaying ()
		{
			return animationPlayer.CurrentAnimation == animationName;
		}

		public float GetLength ()
		{
			return animationPlayer.GetAnimation(animationName).Length;
		}
	}
}
