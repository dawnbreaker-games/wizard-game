using System;

public partial class Range<T>
{
	public T min;
	public T max;

	public Range ()
	{
	}

	public Range (T min, T max)
	{
		this.min = min;
		this.max = max;
	}

	public virtual T Get (float normalizedValue)
	{
		if (normalizedValue < 0 || normalizedValue > 1)
			throw new ArgumentOutOfRangeException("normalizedValue");
		if (normalizedValue <= .5f)
			return min;
		else
			return max;
	}

	public virtual float InverseGet (T value)
	{
		if (value.Equals(min))
			return 0;
		else if (value.Equals(max))
			return 1;
		else
			throw new NotImplementedException("Range<T>.InverseGet is not defined for the given input");
	}

	public virtual bool Contains (T value, bool includeMin = true, bool includeMax = true)
	{
		if ((includeMin && value.Equals(min)) || (includeMax && value.Equals(max)))
			return true;
		throw new NotImplementedException("Range<T>.Contains is not defined for the given inputs");
	}

	public virtual bool Contains (T value, bool includeMinAndMax = true)
	{
		return Contains(value, includeMinAndMax, includeMinAndMax);
	}
	
	public T[] ToArray ()
	{
		return new T[] { min, max };
	}
	
	public static Range<T> FromArray (T[] components)
	{
		return new Range<T>(components[0], components[1]);
	}
}
