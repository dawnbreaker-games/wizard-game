using Godot;
using System.Collections;
using System.Collections.Generic;

namespace FightRoom
{
	public partial class BulletPattern// : ScriptableObject//, IConfigurable
	{
		public bool canSpawn = true;
		public bool setSpawnerAimingOnShoot;

		public virtual void Init (Node3D spawner)
		{
		}

		public virtual Vector3 GetShootDirection (Node3D spawner)
		{
			return spawner.up;
		}
		
		public virtual Bullet[] Shoot (Node3D spawner, Bullet bulletPrefab)
		{
			if (spawner == null)
				return new Bullet[0];
			Vector3 direction = GetShootDirection(spawner);
			if (setSpawnerAimingOnShoot)
				spawner.up = direction;
			if (!canSpawn)
				return new Bullet[0];
			Bullet bullet = ObjectPool.instance.Spawn<Bullet>(bulletPrefab.prefabIndex, spawner.position, Quaternion.LookRotation(Vector3.Forward, direction));
			return new Bullet[] { bullet };
		}
		
		public virtual Bullet[] Shoot (Vector3 spawnPosition, Vector3 direction, Bullet bulletPrefab)
		{
			if (!canSpawn)
				return new Bullet[0];
			Bullet bullet = ObjectPool.instance.Spawn<Bullet>(bulletPrefab.prefabIndex, spawnPosition, Quaternion.LookRotation(Vector3.Forward, direction));
			return new Bullet[] { bullet };
		}
		
		public virtual EventManager.Event RedirectAfterDelay (Bullet bullet, float delay)
		{
			return bullet.AddEvent((object obj) => { Redirect ((Bullet) obj); }, delay);
		}
		
		public virtual EventManager.Event RedirectAfterDelay (Bullet bullet, Vector3 direction, float delay)
		{
			return bullet.AddEvent((object obj) => { Redirect ((Bullet) obj, direction); }, delay);
		}

		public virtual Bullet Redirect (Bullet bullet)
		{
			bullet.velocity = GetRedirectDirection(bullet) * bullet.moveSpeed;
			bullet.rigid.velocity = bullet.velocity + bullet.extraVelocity;
			if (bullet.rigid.velocity != Vector2.Zero)
				bullet.trs.up = bullet.rigid.velocity;
			return bullet;
		}
		
		public virtual Bullet Redirect (Bullet bullet, Vector3 direction)
		{
			bullet.velocity = direction * bullet.moveSpeed;
			bullet.rigid.velocity = bullet.velocity + bullet.extraVelocity;
			if (bullet.rigid.velocity != Vector2.Zero)
				bullet.trs.up = bullet.rigid.velocity;
			return bullet;
		}
		
		public virtual Vector3 GetRedirectDirection (Bullet bullet)
		{
			return bullet.trs.up;
		}
		
		public virtual EventManager.Event SplitAfterDelay (Bullet bullet, Bullet splitBulletPrefab, float delay)
		{
			return bullet.AddEvent((object obj) => { Split ((Bullet) obj, splitBulletPrefab); }, delay);
		}
		
		public virtual EventManager.Event SplitAfterDelay (Bullet bullet, Vector3 direction, Bullet splitBulletPrefab, float delay)
		{
			return bullet.AddEvent((object obj) => { Split ((Bullet) obj, direction, splitBulletPrefab); }, delay);
		}

		public virtual Bullet[] Split (Bullet bullet, Bullet splitBulletPrefab)
		{
			return Shoot(bullet.trs.position, GetSplitDirection(bullet), splitBulletPrefab);
		}

		public virtual Bullet[] Split (Bullet bullet, Vector3 direction, Bullet splitBulletPrefab)
		{
			return Shoot(bullet.trs.position, direction, splitBulletPrefab);
		}
		
		public virtual Vector3 GetSplitDirection (Bullet bullet)
		{
			return bullet.trs.up;
		}
	}
}
