using Godot;
using System;

[Serializable]
public struct ColorOffset
{
	public ComponentOffset redOffset;
	public ComponentOffset greenOffset;
	public ComponentOffset blueOffset;
	public ComponentOffset alphaOffset;

	public Color Apply (Color color)
	{
		return new Color(redOffset.Apply(color.R), greenOffset.Apply(color.G), blueOffset.Apply(color.B));
	}

	public Color ApplyWithTransparency (Color color)
	{
		Color output = Apply(color);
		output.A = alphaOffset.Apply(color.A);
		return output;
	}

	public Color ApplyInverse (Color color)
	{
		return new Color(redOffset.ApplyInverse(color.R), greenOffset.ApplyInverse(color.G), blueOffset.ApplyInverse(color.B));
	}

	public Color ApplyInverseWithTransparency (Color color)
	{
		Color output = ApplyInverse(color);
		output.A = alphaOffset.ApplyInverse(color.A);
		return output;
	}

	[Serializable]
	public struct ComponentOffset
	{
		public Operation operation;
		public float value;

		public float Apply (float component)
		{
			if (operation == Operation.Add)
				return component + value;
			else if (operation == Operation.Subtract)
				return component - value;
			else if (operation == Operation.Multiply)
				return component * value;
			else// if (operation == Operation.Divide)
				return component / value;
		}

		public float ApplyInverse (float component)
		{
			if (operation == Operation.Add)
				return component - value;
			else if (operation == Operation.Subtract)
				return component + value;
			else if (operation == Operation.Multiply)
				return component / value;
			else// if (operation == Operation.Divide)
				return component * value;
		}
	}

	public enum Operation
	{
		Add,
		Subtract,
		Multiply,
		Divide
	}
}
