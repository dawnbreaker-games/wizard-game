namespace FightRoom
{
	public static class LocalUserInfo
	{
		public static string username;
		public static float totalTime;
		public static uint tasksDone;
		public static bool isPublic;
	}
}