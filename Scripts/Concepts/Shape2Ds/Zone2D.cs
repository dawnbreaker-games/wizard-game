//using Godot;
//using System;
//using Extensions;
//
//[Serializable]
//public partial class Zone2D : Shape2D
//{
//	public Collider2D collider;
//	public Node2D[] transforms = new Node2D[0];
//	public Type type;
//
//	public Zone2D (Node2D[] transforms)
//	{
//		type = Type.Transforms;
//		this.transforms = transforms;
//		corners = new Vector2[transforms.Length];
//		for (int i = 0; i < transforms.Length; i ++)
//		{
//			Node2D trs = transforms[i];
//			corners[i] = trs.position;
//		}
//		SetEdgesOfPolygon ();
//	}
//
//	public Zone2D (Collider2D collider)
//	{
//		type = Type.ColliderCorners;
//		this.collider = collider;
//		EdgeCollider2D edgeCollider = collider as EdgeCollider2D;
//		if (edgeCollider != null)
//			corners = edgeCollider.points;
//		else
//		{
//			PolygonCollider2D polygonCollider = collider as PolygonCollider2D;
//			if (polygonCollider != null)
//				corners = polygonCollider.points;
//			else
//			{
//				BoxCollider2D boxCollider = collider as BoxCollider2D;
//				if (boxCollider != null)
//					corners = boxCollider.bounds.ToRect().GetCorners();
//			}
//		}
//		SetEdgesOfPolygon ();
//	}
//
//	public enum Type
//	{
//		ColliderCorners,
//		Transforms
//	}
//}
