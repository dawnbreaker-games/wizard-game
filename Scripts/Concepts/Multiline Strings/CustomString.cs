using System;

using System.Collections;
using System.Collections.Generic;

namespace FightRoom
{
	[Serializable]
	public partial class CustomString : MultilineString
	{
		public SerializableDictionary<InputManager.InputDevice, MultilineString> valuesForInputDevices = new SerializableDictionary<InputManager.InputDevice, MultilineString>();
		public override string Value
		{
			get
			{
				valuesForInputDevices.Init ();
				value = valuesForInputDevices[InputManager.instance.inputDevice].value;
				return value;
			}
			set
			{
				valuesForInputDevices.Init ();
				this.value = value;
				valuesForInputDevices[InputManager.instance.inputDevice].value = value;
			}
		}
	}
}