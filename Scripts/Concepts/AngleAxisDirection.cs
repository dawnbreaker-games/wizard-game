using Godot;
using System;
using Extensions;
using System.Collections;
using System.Collections.Generic;

[Serializable]
public class AngleAxisDirection
{
	public float xyAngle;
	public float xzAngle;
	public float yzAngle;
	public Vector3 direction;

	public AngleAxisDirection (float xyAngle, float xzAngle, float yzAngle)
	{
		this.xyAngle = xyAngle;
		this.xzAngle = xzAngle;
		this.yzAngle = yzAngle;
		UpdateValue ();
	}

	public AngleAxisDirection (AngleAxisDirection angleAxisDirection) : this (angleAxisDirection.xyAngle, angleAxisDirection.xzAngle, angleAxisDirection.yzAngle)
	{
	}
	
	public Vector3 UpdateValue ()
	{
		Vector2 xyVector = new Vector2();
		Vector2 xzVector = new Vector2();
		Vector2 yzVector = new Vector2();
		if (xyAngle != 0)
			xyVector = VectorExtensions.FromFacingAngle(xyAngle);
		if (xzAngle != 0)
			xzVector = VectorExtensions.FromFacingAngle(xzAngle);
		if (yzAngle != 0)
			yzVector = VectorExtensions.FromFacingAngle(yzAngle);
		direction = new Vector3(xyVector.X + xzVector.X, xyVector.Y + yzVector.X, xzVector.Y + yzVector.Y).Normalized();
		return direction;
	}
	
	public static AngleAxisDirection operator+ (AngleAxisDirection angleAxisDirection, AngleAxisDirection angleAxisDirection2)
	{
		AngleAxisDirection output = new AngleAxisDirection(angleAxisDirection);
		output.xyAngle += angleAxisDirection2.xyAngle;
		output.xzAngle += angleAxisDirection2.xzAngle;
		output.yzAngle += angleAxisDirection2.yzAngle;
		output.UpdateValue ();
		return output;
	}
	
	public static AngleAxisDirection operator* (AngleAxisDirection angleAxisDirection, AngleAxisDirection angleAxisDirection2)
	{
		AngleAxisDirection output = new AngleAxisDirection(angleAxisDirection);
		output.xyAngle *= angleAxisDirection2.xyAngle;
		output.xzAngle *= angleAxisDirection2.xzAngle;
		output.yzAngle *= angleAxisDirection2.yzAngle;
		output.UpdateValue ();
		return output;
	}
}
