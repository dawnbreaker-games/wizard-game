using Godot;
using System;

namespace FightRoom
{
	[Serializable]
	public partial class MultilineString
	{
		[Export]
		string value;
		public virtual string Value
		{
			get
			{
				return value;
			}
			set
			{
				this.value = value;
			}
		}
	}
}
